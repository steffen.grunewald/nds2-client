function value = slope( self ) % -*- mode: octave; -*-
% DATATYPE retrieve the slope of the channel
%
%   Retrieves the slope of the channel
%
%   INPUTS:
%     SELF   : ndsm.channel
%
%   OUTPUTS:
%     VALUE
  value = self.slope( )
end
