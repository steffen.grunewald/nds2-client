% -*- mode: octave -*-
%------------------------------------------------------------------------
%------------------------------------------------------------------------
global unit_test;

unit_test = unittest( );

hostname = unit_test.hostname( );
port = unit_test.port( );
protocol = nds2.connection.PROTOCOL_TWO;
use_gap_handler = 'true';
gap_handler = 'STATIC_HANDLER_NEG_INF';

%------------------------------------------------------------------------
% Establish the connection
%------------------------------------------------------------------------

conn = nds2.connection(hostname, port, protocol );
conn.setParameter( 'ITERATE_USE_GAP_HANDLERS', use_gap_handler );
conn.setParameter( 'GAP_HANDLER', gap_handler );

%------------------------------------------------------------------------
% Run the test
%------------------------------------------------------------------------
START = 1219524930;
STOP = START + 4;
CHANNELS = { 'X1:PEM-1' };

bufs = conn.fetch(START, STOP, CHANNELS);

expected = -2147483648;
actual = bufs( 1 ).getData( );

for i = 1:size(actual, 1)
    unit_test.check( expected, actual( i ), 'Verify data');
end

%------------------------------------------------------------------------
% Finish
%------------------------------------------------------------------------
conn.close( );
unit_test.exit( );