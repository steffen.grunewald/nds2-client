/* -*- tab-width:8 c-basic-offset:4  indent-tabs-mode:nil -*- */
/* set vi: ts=8:softtabstop=4,shiftwidth=4,expandtab */

#if HAVE_CONFIG_H
#include "daq_config.h"
#endif /*HAVE_CONFIG_H */

#include <stdarg.h>
#include <stdio.h>

#include "nds_log.h"

void
nds_flush( )
{
    fflush( stderr );
}

void
nds_log( const char* Message )
{

    fprintf( stderr, "%s", Message );
}
