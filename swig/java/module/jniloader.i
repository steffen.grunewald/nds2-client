/*
 * Static initialization code to automatically load JNI library
 *
 * Copyright (C) 2014  Leo Singer <leo.singer@ligo.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */


/* Imports for JNI loader (see '%pragma(java) jniclasscode' block below). */
%pragma(java) jniclassimports = %{
	import java.io.File;
	import java.io.FileOutputStream;
	import java.io.InputStream;
	import java.io.IOException;
	import java.net.URL;
	import java.net.URI;
	import java.net.URISyntaxException;
%}

%pragma(java) jniclasscode = %{

	/* Load JNI library an arbitrary URL.
	 * Inspired by http://tlrobinson.net/blog/2009/03/09/embedding-and-loading-a-jni-library-from-a-jar/
	 */
	private static void loadLibraryFromURL(URL url) throws IOException
	{
		// Get an input stream for the shared object.
		InputStream in = url.openStream();

		try {
			// Create a temp file and an input stream for it.
			String os = System.getProperty( "os.name" );
			String ext = ".jnilib";
			if ( os.startsWith( "Windows" ) )
			{
				ext = ".dll";
			}
			File f = File.createTempFile("$moduleJNI-", ext);
			try {
				FileOutputStream out = new FileOutputStream(f);

				try {
					// Copy the lib to the temp file.
					byte[] buf = new byte[1024];
					int len;
					while ((len = in.read(buf)) > 0)
						out.write(buf, 0, len);
				} finally {
					out.close();
				}

				// load the lib specified by it's absolute path and delete it
				System.load(f.getAbsolutePath());
			} finally {
				f.delete();
			}
		} finally {
			in.close();
		}
	}

	static {
		String libraryPrefix;
		String librarySuffix;
		// Find the name of the shared object on this platform.
		// The shared object has the same name as this class.
		if ( System.getProperty("os.name").startsWith("Windows") )
		{
			libraryPrefix = "";
			librarySuffix = ".dll";
		}
		else
		{
			libraryPrefix = "lib";
			librarySuffix = ".jnilib";
		}	
		final String libraryName = libraryPrefix + "$moduleJNI" + librarySuffix;

		// Find the URL of the shared object.
		final URL libraryURL = $moduleJNI.class.getResource(libraryName);
		if (libraryURL == null)
			throw new RuntimeException("The native module '" + libraryName + "' could not be found.");

		if ("file".equals(libraryURL.getProtocol()))
		{
			// Convert the URL to a URI. URL.getPath() returns a URL-encoded
			// path, which is not suitable as a path on the filesystem.
			// Fortunately, URI.getPath() returns an unencoded path.
			//
			// The URISyntaxException is probably impossible in this situation,
			// so if it happens, we just convert wrap it in an unchecked
			// RuntimeException.
			URI libraryURI;
			try {
				libraryURI = libraryURL.toURI();
			} catch (URISyntaxException e) {
				throw new RuntimeException(e);
			}
			System.load(libraryURI.getPath());
		} else {
			try {
				loadLibraryFromURL(libraryURL);
			} catch (IOException e) {
				throw new RuntimeException("The native module '" + libraryURL + "' could not be loaded.", e);
			}
		}

	}
%}
