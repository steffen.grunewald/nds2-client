% -*- mode: octave -*-
host = getenv( 'NDS_TEST_HOST' )
if ( isempty( host ) );
  host = 'localhost'
end
port = getenv( 'NDS_TEST_PORT' )
if ( isempty( port ) )
  port = 31200
else
  port = str2num( port )
end

protocol = 1;

c = nds2.connection( host, port, protocol )

try;
  lst = c.findChannels('*')
catch except;
  disp( 'Caught Runtime error, should be from a timeout, but we currently cannot get a good error message' );
  exit(0);
end;
disp( 'Did not test timeouts!!!!' )
exit(1);
