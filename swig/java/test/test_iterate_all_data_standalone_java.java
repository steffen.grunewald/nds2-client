/* ------------------------------------------------------------------- *
 * ------------------------------------------------------------------- */

import nds2.buffer;
import nds2.connection;
import nds2.nds2;
import nds2.parameters;
import nds2.request_period;

public final class test_iterate_all_data_standalone_java {

    private static unittest unit_test;

    public static void main( String[] args )
	throws java.io.UnsupportedEncodingException
    {
	unit_test = new unittest( args );

	String hostname = unit_test.hostname( );
	String use_gap_handler = "true";
	boolean explicit_args = false;
	int port = unit_test.port( );
	/* ----------------------------------------------------------- *
	   Need to adjust according to parameter
	 * ----------------------------------------------------------- */
	int protocol = connection.PROTOCOL_TWO;
	if ( unit_test.hasOption( "-proto-1" ) )
	{
	    protocol = connection.PROTOCOL_ONE;
	}
	if ( unit_test.hasOption( "-proto-2" ) )
	{
	    protocol = connection.PROTOCOL_TWO;
	}
	if ( unit_test.hasOption( "-no-gap" ) )
	{
	    use_gap_handler = "false";
	}

	/* ------------------------------------------------------------- *
	   Establish the connection
	 * ------------------------------------------------------------- */

    parameters params;
    params = new parameters( hostname, port, protocol );
    params.set( "ITERATE_USE_GAP_HANDLERS", use_gap_handler );

	/* ------------------------------------------------------------- *
	   Run the test
	 * ------------------------------------------------------------- */

	int start = 1770000000;
	int end = start + 20;
	int stride = 10;
	request_period period = new request_period( start, end, stride );
	String[] channels = new String[] {
	    "X1:PEM-1",
	    "X1:PEM-2",
	};

	long last_start = 0;
	int cur_gps = start;
	double[] expected = new double[]{ 1.5, 2.75 };

	for( buffer[] bufs : nds2.iterate( params, period, channels ) )
	{
	    /* ------------------------------------------------------- */
	    unit_test.check( bufs[0].getGpsSeconds( ),
			     cur_gps,
			     "Verifying GPS second of buffer 0" );
	    unit_test.check( bufs[0].samples( ),
			     256 * stride,
			     "Verifying samples of buffer 0" );
	    unit_test.check( true,
			     checkData( bufs[0], expected[0] ),
			     "Verifying data of buffer 0" );
	    /* ------------------------------------------------------- */
	    unit_test.check( bufs[1].getGpsSeconds( ),
			     cur_gps,
			     "Verifying GPS second of buffer 0" );
	    unit_test.check( bufs[1].samples( ),
			     512 * stride,
			     "Verifying samples of buffer 0" );
	    unit_test.check( true,
			     checkData( bufs[1], expected[1] ),
			     "Verifying data of buffer 0" );
	    /* ------------------------------------------------------- */
	    cur_gps = bufs[1].stop( );
	    expected = new double[]{ 3.5, 4.75 };
	}

	unit_test.exit( );
    }

    static boolean isGapSecond( int GPS )
    {
	return( false );
    }

    static boolean checkData( buffer Buf, double Value )
    {
	int samples_per_sec = (int)Buf.getSampleRate( );
	final int CHANNEL_TYPE = Buf.getDataType( );

	unit_test.msgInfo( "samples_per_sec: %d", samples_per_sec );
	int offset = 0;
	for( int cur_gps = Buf.start( ); cur_gps != Buf.stop( ); ++cur_gps )
	{
	    double expected = Value;
	    if ( isGapSecond( cur_gps ) )
	    {
		expected = 0.0;
	    }
	    for( int i = 0; i < samples_per_sec; ++i )
	    {
		if ( CHANNEL_TYPE == buffer.DATA_TYPE_FLOAT32 )
		{
		    if ( ((float[])Buf.getData( ))[offset] != (float)expected )
		    {
			return( false );
		    }
		}
		else if ( CHANNEL_TYPE == buffer.DATA_TYPE_FLOAT64 )
		{
		    if ( ((double[])Buf.getData( ))[offset] != expected )
		    {
			return( false );
		    }
		}
		offset++;
	    }
	}
	return( true );
    }
}