% -*- mode: octave -*-
classdef UnitTestClass
   properties
     VERBOSE_LEVEL
     STATUS
   end

   methods( Static )
     function VALUE = hostname( )
       VALUE = getenv( 'NDS_TEST_HOST' );
       if ( isempty( VALUE ) );
	 host = 'localhost';
       end
     end
     function VALUE = port( )
       VALUE = getenv( 'NDS_TEST_PORT' );
       if ( isempty( VALUE ) )
	 VALUE = 31200;
       else
	 VALUE = str2num( VALUE );
       end
     end
   end% methods( Static )

   methods
     function OBJ = check( self, condition, frmt, varargin )
       if ( condition )
	 lead = '-- PASS: ';
       else
	 lead = '-- FAIL: ';
	 self.STATUS = 1;
       end
       if( length( varargin ) > 0 )
	 frmt = sprintf( frmt, varargin{:} );
       end
       frmt = sprintf( '%s%s', lead, frmt  );
       self.message( frmt );
       OBJ = self;
     end

     function exit( obj )
       % ----------------------------------------------------------------
       % Leave according to how testing completed
       % ----------------------------------------------------------------
       exit( obj.STATUS );
     end% function - exit

     function VALUE = initialize( obj, varargin )
       obj.VERBOSE_LEVEL = int16( 0 );
       obj.STATUS = int16( 0 );
       VALUE = obj;
     end% function initialize

     function message( obj, text )
       text = string( text );
       disp( text );
     end

     function msg_debug( obj, level, frmt, varargin )
       if ( obj.VERBOSE_LEVEL >= level )
         if( length( varargin ) > 0 )
           frmt = sprintf( frmt, varargin{:} );
         end
         frmt = sprintf( '-- DBUG: %s', frmt );
         obj.message( frmt );
       end
     end%

     function msg_info( obj, frmt, varargin )
       if( length( varargin ) > 0 )
         frmt = sprintf( frmt, varargin{:} );
       end
       frmt = sprintf( '-- INFO: %s', frmt );
       obj.message( frmt );
     end%

   end% methods
end
