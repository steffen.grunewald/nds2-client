% -*- mode: octave -*-
%------------------------------------------------------------------------
global unit_test;

unit_test = UnitTest( );

hostname = unit_test.hostname( );
port = unit_test.port( );
protocol = ndsm.Connection.PROTOCOL_TWO;

%------------------------------------------------------------------------
% Establish the connection
%------------------------------------------------------------------------
	   
conn = ndsm.Connection(hostname, port, protocol );

%------------------------------------------------------------------------
% Run test
%------------------------------------------------------------------------
data = letoriginaldatagooutofscope( conn );
unit_test.check( data(1), ...
		 data(2), ...
		 'Verify first data pair on returned data' );
unit_test.check( data(2), ...
		 data(3), ...
		 'Verify second data pair on returned data' );

%------------------------------------------------------------------------
% Finish
%------------------------------------------------------------------------
conn.close( );
unit_test.exit( );

