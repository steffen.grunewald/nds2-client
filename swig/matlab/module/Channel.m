classdef Channel < handle % -*- mode: matlab; -*-
% Channel metadata information
    properties (Access = private)
        % property attributes: http://www.mathworks.com/help/matlab/matlab_oop/property-attributes.html
        handle = 0;
        array_handle = 0;
    end
    
    properties (Constant, GetAccess = public)
        CHANNEL_TYPE_UNKNOWN = nds2.channel.CHANNEL_TYPE_UNKNOWN;
        CHANNEL_TYPE_ONLINE = nds2.channel.CHANNEL_TYPE_ONLINE;
        CHANNEL_TYPE_RAW =nds2.channel.CHANNEL_TYPE_RAW;
        CHANNEL_TYPE_RDS = nds2.channel.CHANNEL_TYPE_RDS;
        CHANNEL_TYPE_STREND = nds2.channel.CHANNEL_TYPE_STREND;
        CHANNEL_TYPE_MTREND = nds2.channel.CHANNEL_TYPE_MTREND;
        CHANNEL_TYPE_TEST_POINT = nds2.channel.CHANNEL_TYPE_TEST_POINT;
        CHANNEL_TYPE_STATIC = nds2.channel.CHANNEL_TYPE_STATIC;

        DATA_TYPE_UNKNOWN = nds2.channel.DATA_TYPE_UNKNOWN;
        DATA_TYPE_INT16 = nds2.channel.DATA_TYPE_INT16;
        DATA_TYPE_INT32 = nds2.channel.DATA_TYPE_INT32;
        DATA_TYPE_INT64 = nds2.channel.DATA_TYPE_INT64;
        DATA_TYPE_FLOAT32 = nds2.channel.DATA_TYPE_FLOAT32;
        DATA_TYPE_FLOAT64 = nds2.channel.DATA_TYPE_FLOAT64;
        DATA_TYPE_COMPLEX32 = nds2.channel.DATA_TYPE_COMPLEX32;
        DATA_TYPE_UINT32 = nds2.channel.DATA_TYPE_UINT32;
    end
    
    methods (Hidden, Access = protected)
        function HANDLE = gethandle(self)
            if isa(self.handle,'numeric')
                throw(MException('nds.connection:notopen','No valid channel for self operation'));
            end
            HANDLE = self.handle;
        end
    end
    
    methods (Static, Access = public )
        function VALUE = getDEFAULT_CHANNEL_MASK( )
        % Retrieve the value of DEFAULT_CHANNEL_MASK
        %
        %   OUTPUTS:
        %     The value of DEFAULT_CHANNEL_MASK
            VALUE = nds2.channel.getDEFAULT_CHANNEL_MASK( );
        end

        function VALUE = getDEFAULT_DATA_MASK( )
        % Retrieve the value of DEFAULT_DATA_MASK
        %
        %   OUTPUTS:
        %     The value of DEFAULT_DATA_MASK
            VALUE = nds2.channel.getDEFAULT_DATA_MASK( );
        end

        function VALUE = issecondtrend( NAME )
        % Check the channel name to see if it is a second trend.
        %
        % INPUTS:
        %       NAME : A channel name, as a string.
        %<
        %  OUTPUTS:
        %       VALUE : true if Name ends in ",s-trend", else false.
        %
            VALUE = nds2.channel.isSecondTrend( NAME );
        end

        function VALUE = isminutetrend( NAME )
        % Check the channel name to see if it is a minute trend.
        %
        % INPUTS:
        %       NAME : A channel name, as a string.
        %
        %  OUTPUTS:
        %       VALUE : true if Name ends in ",m-trend", else false.
        %
            VALUE = nds2.channel.isMinuteTrend( NAME );
        end
    end

    methods (Access = public)
        
        function CHANNEL = Channel( varargin )
        % CHANNEL constructor for the ndsm.channel class
        %
        %   OUTPUTS:
        %     CHAN : ndsm.Channel instance
            switch( nargin )
              case 0
                h = nds2.channel( );
              case 1
                h = varargin{1};
                if ( ( isa( varargin{1}, 'nds2.buffer' ) ) ...
                     || ( isa( varargin{1}, 'nds2.channel' ) ) )
                    h = varargin{ 1 };
                elseif ( isa( varargin{1}, 'nds2.buffer[]' ) )
                    for x = 1:size( varargin{1} )
                        buf = varargin{1};
                        buf = buf(x);
                        CHANNEL(x) = ndsm.Buffer( buf );
                    end
                    return;
                elseif ( isa( varargin{1}, 'nds2.channel[]' ) )
                    for x = 1:size( varargin{1} )
                        buf = varargin{1};
                        buf = buf(x);
                        CHANNEL(x) = ndsm.Channel( buf );
                    end
                    return;
                else % if
                    h = varargin{1};
                end % if ()
              case 2
                if ( ( isa( varargin{1}, 'nds2.channel' ) && ...
                       isa( varargin{2}, 'nds2.channel[]' ) ) || ...
                     ( isa( varargin{1}, 'nds2.buffer' ) && ...
                       isa( varargin{2}, 'nds2.buffer[]' ) ) )
                    h = varargin{1};
                    OBJ.array_handle = varargin{2};
                end
            end
            CHANNEL.handle = h;
        end

        function NAME = name( self )
        % NAME retrieve the name of the channel
        %
        %   Retrieves the name of the channel
        %
        %   INPUTS:
        %     SELF : ndsm.Channel instance
        %
        %   OUTPUTS:
        %     NAME : name of the channel
            NAME = char(self.gethandle( ).name( ));
        end

        function TYPE = chantype( self )
        % TYPE retrieve the type of the channel.
        %
        % Retrieves the channel_type representing the channel's type.
        %
        %   INPUTS:
        %     SELF : ndsm.Channel
        %
        %   OUTPUTS:
        %     TYPE : Type of the channel
            TYPE = self.gethandle( ).type( );
        end

        function DATA_TYPE =datatype( self )
        % DATA_TYPE retrieve the data type of the channel.
        %
        % Retrieves the data_type representing the data type of the channel.
        %
        %   INPUTS:
        %     SELF      : ndsm.Channel
        %
        %   OUTPUTS:
        %     DATA_TYPE : Data type of channel
            DATA_TYPE = self.gethandle( ).dataType( );
        end

        function DATA_TYPE_SIZE = datatypesize( self )
        % DATA_TYPE_SIZE retrieve the size in bytes of an individual
        %    sample from the channel.
        %
        % Retrieves the size_type value giving the size of an
        %    individual sample of self channel.
        %
        %   INPUTS:
        %     SELF           : ndsm.Channel
        %
        %   OUTPUTS:
        %     DATA_TYPE_SIZE : Size of the data type
            DATA_TYPE_SIZE = self.gethandle( ).dataTypeSize( );
        end

        function SAMPLE_RATE = samplerate( self )
        % SAMPLE_RATE retrieve the sample rate of the channel
        %
        % Retrieves the sample rate of the channel.
        %
        %   INPUTS:
        %     SELF        : ndsm.Channel
        %
        %   OUTPUTS:
        %     SAMPLE_RATE : Sample rate of the channel
            SAMPLE_RATE = self.gethandle( ).sampleRate( );
        end

        function GAIN = gain( self )
        % GAIN retrieve the gain of the channel
        %
        % Retrieves the gain of the channel.
        %
        %   INPUTS:
        %     SELF : ndsm.Channel
        %
        %   OUTPUTS:
        %     GAIN : Gain of the channel
            GAIN = self.gethandle( ).gain( );
        end

        function SLOPE = slope( self )
        % SLOPE retrieve the slope of the channel
        %
        % Retrieves the slope of the channel.
        %
        %   INPUTS:
        %     SELF  : ndsm.Channel
        %
        %   OUTPUTS:
        %     SLOPE : Slope of the channel
            SLOPE = self.gethandle( ).slope( );
        end

        function OFFSET = offset( self )
        % OFFSET retrieve the offset of the channel
        %
        % Retrieves the offset of the channel.
        %
        %   INPUTS:
        %     SELF   : ndsm.Channel
        %
        %   OUTPUTS:
        %     OFFSET : Offset of the channel
            OFFSET = self.gethandle( ).offset( );
        end

        function UNITS = units( self )
        % UNITS retrieve the units of the channel
        %
        % Retrieves the units of the channel.
        %
        %   INPUTS:
        %     SELF  : ndsm.Channel
        %
        %   OUTPUTS:
        %     UNITS : Units of the channel
            UNITS = char(self.gethandle( ).units( ));
        end
        
        function NAME = tostring( self )
        % NAME retrieve the name of the channel
        %
        %   Retrieves the name of the channel
        %
        %   INPUTS:
        %     SELF : ndsm.Channel
        %
        %   OUTPUTS:
        %     NAME : name of the channel
            NAME = char( self.gethandle( ).toString( ) );
        end

        % ----------------------------------------------------------------

        function SAMPLE_RATE = getsamplerate( self )
        % SAMPLE_RATE retrieve the sample rate of the channel
        %
        % Retrieves the sample rate of the channel.
        %
        %   INPUTS:
        %     SELF        : ndsm.Channel
        %
        %   OUTPUTS:
        %     SAMPLE_RATE : Sample rate of the channel
            ndsm.Depricated( 'ndsm.channel.getsamplerate', 'ndsm.channel.samplerate' )
            SAMPLE_RATE = self.samplerate( );
        end

    end
end
