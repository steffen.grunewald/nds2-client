#include <iostream>
#include <fstream>
#include <cstdlib>
#include <sstream>

#include "debug_stream.hh"

namespace
{
    std::ofstream nullStream;

    std::ostream* dstream = &nullStream;

    bool initialized = false;
}

namespace NDS
{
    namespace detail
    {

        std::ostream&
        dout( )
        {
            if ( !initialized )
            {
                const char* var = std::getenv( "NDS_CLIENT_DEBUG" );
                if ( var != (const char*)nullptr )
                {
                    int value = 1;

                    if ( *var != '\0' )
                    {
                        std::istringstream i( var );

                        i >> value;
                    }
                    if ( value > 0 )
                    {
                        dstream = &std::cerr;
                    }
                }
                initialized = true;
            }
            return *dstream;
        }
    }
}
