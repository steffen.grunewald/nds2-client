import nds2
import os

# -----------------------------------------------------------------------
#   Simple test to verify version function
# -----------------------------------------------------------------------

# -----------------------------------------------------------------------

def create_mTrends( ):
    return [ 'A,m-trend',
             'X1:ABC,m-trend',
             'X1:ABC.mean,m-trend',
             'A:,m-trend' ]

# -----------------------------------------------------------------------

def create_sTrends( ):
    return [ 'A,s-trend',
             'X1:ABC,s-trend',
             'X1:ABC.mean,s-trend',
             'A:,s-trend' ]

# -----------------------------------------------------------------------

def create_junk( ):
    return [ '',
             's-trend',
             ',s-trend',
             'm-trend',
             ',m-trend',
             'S-TREND',
             ',S-TREND',
             'M-TREND',
             ',M-TREND',
             ',mtrend',
             'STREND',
             ',strend',
             'MTREND',
             'X1:ABCCDEFGs-trend',
             'X1:ABCDEFGS-TREND',
             'X1:ABCCDEFGm-trend',
             'X1:ABCDEFGM-TREND',
             'X1:ABCDEFGM' ]

# -----------------------------------------------------------------------
#   Minute Trend
# -----------------------------------------------------------------------
trends = create_mTrends( )
  
for i in trends:
    assert( nds2.channel.IsMinuteTrend( i ) == True )
    assert( nds2.channel.IsSecondTrend( i ) == False )

# -----------------------------------------------------------------------
#   Second Trend
# -----------------------------------------------------------------------
trends = create_sTrends( )
  
for i in trends:
    assert( nds2.channel.IsMinuteTrend( i ) == False )
    assert( nds2.channel.IsSecondTrend( i ) == True )

# -----------------------------------------------------------------------
#   Junk Trend
# -----------------------------------------------------------------------
trends = create_junk( )
  
for i in trends:
    assert( nds2.channel.IsMinuteTrend( i ) == False );
    assert( nds2.channel.IsSecondTrend( i ) == False );
