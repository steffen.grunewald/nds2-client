/* ------------------------------------------------------------------- *
 * ------------------------------------------------------------------- */

import nds2.connection;
import nds2.channel;

public final class test_count_channels_java {

    public static void main( String[] args )
	throws java.io.UnsupportedEncodingException
    {
	unittest unit_test = new unittest( args );

	String hostname = unit_test.hostname( );
	int port = unit_test.port( );
	int protocol = connection.PROTOCOL_TWO;

	if ( args.length == 1 )
	{
	    if ( args[0].equals( "-proto-1" ) )
	    {
		protocol = connection.PROTOCOL_ONE;
	    }
	    else if ( args[0].equals( "-proto-2" ) )
	    {
		protocol = connection.PROTOCOL_TWO;
	    }
	}

	/* ------------------------------------------------------------- *
	 * Establish the connection
	 * ------------------------------------------------------------- */
	   
	connection conn = new connection(hostname, port, protocol );

	/* ------------------------------------------------------------- *
	   Run the test
	 * ------------------------------------------------------------- */
	if(  conn.getProtocol() == connection.PROTOCOL_ONE )
	{
	    unit_test.check( conn.countChannels("*"),
			     4*11,
			     "PROTOCOL 1 Full count" );
	    unit_test.check( conn.countChannels("*4*"),
			     11,
			     "PROTOCOL 1 Channels containing the number 4" );
	    unit_test.check( conn.countChannels("*4*",
						channel.getDEFAULT_CHANNEL_MASK( ),
						nds2.channel.getDEFAULT_DATA_MASK( ),
						0.9,
						1.1),
			     5,
			     "PROTOCOL 1 Channels containing the number 4 - DEFAULT_CHANNEL_MASK and DEFAULT_DATA_MASK" );
	    unit_test.check( conn.countChannels("*",
						nds2.channel.CHANNEL_TYPE_ONLINE),
			     4,
			     "PROTOCOL 1 All online channels" );
	    unit_test.check( conn.countChannels("*",
						channel.CHANNEL_TYPE_ONLINE,
						channel.getDEFAULT_DATA_MASK( ),
						10.0,
						300.0),
			     4,
			     "PROTOCOL 1 All online channels with DEFAULT_DATA_MASK" );
	}
	else
	{
	    unit_test.check( conn.countChannels("*"),
			     12,
			     "PROTOCOL 2 Full count" );
	    unit_test.check( conn.countChannels("*4*"),
			     3,
			     "PROTOCOL 2 Channels containing the number 4" );
	    unit_test.check( conn.countChannels("*4*",
						channel.getDEFAULT_CHANNEL_MASK( ),
						channel.getDEFAULT_DATA_MASK( ),
						0.9,
						1.1),
			     1,
			     "PROTOCOL 2 Channels containing the number 4 - DEFAULT_CHANNEL_MASK and DEFAULT_DATA_MASK" );
	    unit_test.check( conn.countChannels("*",
						channel.CHANNEL_TYPE_RAW),
			     4,
			     "PROTOCOL 2 All raw channel types" );
	    unit_test.check( conn.countChannels("*",
						channel.CHANNEL_TYPE_RAW,
						channel.getDEFAULT_DATA_MASK( ),
						10.0,
						300.0),
			     4,
			     "PROTOCOL 2 all raw channesl with DEFAULT_DATA_MASK" );
	}

	/* ------------------------------------------------------------- *
	 * Finish
	 * ------------------------------------------------------------- */
	conn.close( );

	unit_test.exit( );
    }
}