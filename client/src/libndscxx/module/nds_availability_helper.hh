#ifndef NDS2_AVAILABILITY_HELPER_HH
#define NDS2_AVAILABILITY_HELPER_HH

namespace NDS
{
    namespace detail
    {
        bool gaps_equivalent( const simple_segment_list_type& l1,
                              const simple_segment_list_type& l2 );
    }
}

#endif // NDS2_AVAILABILITY_HELPER_HH
