import nds2
import os
import sys


def _unicode(inp):
    if sys.version_info.major == 2:
        return unicode(inp)
    try:
        return inp.decode()
    except AttributeError:
        return inp

# test the python client using unicode strings for string input
# in these tests we are really only concerned about testing the
# string inputs.
# However we add in extra parameters at times to make sure
# the code that handles string arguments (which may need to
# coerse a string type) does not change another input


host = u'localhost'
if 'NDS_TEST_HOST' in os.environ:
    host = _unicode(os.environ['NDS_TEST_HOST'])
port = 31200
if 'NDS_TEST_PORT' in os.environ:
    port = int(os.environ['NDS_TEST_PORT'])
# the protocol doesn't matter, but we will pick nds2
proto = nds2.connection.PROTOCOL_TWO

conn = nds2.connection(host, port, proto)

assert ( conn.count_channels(u"*PEM*") == conn.count_channels("*PEM*") )

chans = conn.find_channels( u"*PEM*" )
assert ( len(chans) == 3 )
assert ( chans[0].name == "X1:PEM-1" )

conn.set_epoch( u"ALL" )

conn.check( 1000000000, 1000000001, [ "X1:PEM-1", u"X1:PEM-2"] )
conn.check( 1000000000, 1000000001, [ u"X1:PEM-1", "X1:PEM-2"] )
conn.check( 1000000000, 1000000001, [ u"X1:PEM-1", u"X1:PEM-2"] )
conn.check( 1000000000, 1000000001, [ u"X1:PEM-1", ] )

try:
    conn.fetch( 1000000000, 1000000001, [ "X1:PEM-1", u"X1:PEM-2"] )
except Exception as e:
    assert ( 'No data was found' in str(e) )
try:
    conn.fetch( 1000000000, 1000000001, [ u"X1:PEM-1", "X1:PEM-2"] )
except Exception as e:
    assert ( 'No data was found' in str(e) )
try:
    conn.fetch( 1000000000, 1000000001, [ u"X1:PEM-1", u"X1:PEM-2"] )
except Exception as e:
    assert ( 'No data was found' in str(e) )
try:
    conn.fetch( 1000000000, 1000000001, [ u"X1:PEM-1",] )
except Exception as e:
    assert ( 'No data was found' in str(e) )

conn.set_parameter(u"ALLOW_DATA_ON_TAPE", u"true")
assert ( conn.get_parameter("ALLOW_DATA_ON_TAPE") == "true" )
assert ( conn.get_parameter(u"ALLOW_DATA_ON_TAPE") == "true" )

# get_avialability
conn.get_availability([ "X1:PEM-1", u"X1:PEM-2"])
conn.get_availability([ u"X1:PEM-1", "X1:PEM-2"])
conn.get_availability([ u"X1:PEM-1", u"X1:PEM-2"])
conn.get_availability([ u"X1:PEM-1", ])

# iterate
try:
    conn.iterate([b"X1:PEM-1", u"X1:PEM-2"])
except Exception as e:
    assert ( 'Requested data were not found.' in str(e) )
try:
    conn.iterate([u"X1:PEM-1", b"X1:PEM-2"])
except Exception as e:
    assert ( 'Requested data were not found.' in str(e) )
try:
    conn.iterate([u"X1:PEM-1", u"X1:PEM-2"])
except Exception as e:
    assert ( 'Requested data were not found.' in str(e) )
try:
    conn.iterate([u"X1:PEM-1",])
except Exception as e:
    assert ( 'Requested data were not found.' in str(e) )