% -*- mode: octave -*-
classdef UnitTest
   properties
     VERBOSE_LEVEL = 0;
     STATUS = 0;
   end

   methods( Static )
     function RETVAL = hasoption( Option )
       v = UnitTest.setgetvar( );
       try
	 RETVAL = validatestring( Option, v );
	 if ( ( ischar( RETVAL ) ) && ( strcmp( RETVAL, Option ) ) )
	   RETVAL = true;
	 else
	   RETVAL = false;
	 end
       catch ME
	 RETVAL = false;
       end
     end
     function VALUE = hostname( )
       VALUE = getenv( 'NDS_TEST_HOST' );
       if ( isempty( VALUE ) );
	 VALUE = 'localhost';
       end
     end
     function VALUE = port( )
       VALUE = getenv( 'NDS_TEST_PORT' );
       if ( isempty( VALUE ) )
	 VALUE = 31200;
       else
	 VALUE = str2num( VALUE );
       end
     end
     function OBJ = checkstring( s )
         if ( exist('isstring'))
             r1 = isstring(s);
         else
             r1 = ischar(s);
         end
         OBJ = r1;
     end
     function VALUE = logicaltostring( l )
         if ( l )
           v1 = '1';
         else
           v1 = '0';
         end
         VALUE = v1;
     end
     function RETVAL = setgetvar( ArgV )
       persistent ARGV;
       if ( nargin )
	ARGV = ArgV;
      end
      RETVAL = ARGV;
    end
   end% methods( Static )

   methods
     function OBJ = UnitTest( ArgV )
       if ( nargin == 1 )
	 UnitTest.setgetvar( ArgV );
       end
     end

     function OBJ = check( self, varargin )
       switch ( length( varargin ) )
	 case 3
	   if ( ( UnitTest.checkstring( varargin{1} ) ...
		 || ischar( varargin{1} ) ...
		 || ( isa( varargin{1}, 'java.lang.String' ) ) ) ...
		 && ...
	        ( UnitTest.checkstring( varargin{2} ) ...
		  || ischar( varargin{2} ) ...
		  || ( isa( varargin{2}, 'java.lang.String' ) ) ) )
	     condition = strcmp( varargin{1}, varargin{2} );
	     s1 = char( varargin{1} );
	     s2 = char( varargin{2} );
	   elseif ( ( islogical( varargin{1} ) && ( islogical( varargin{2} ) ) ) )
	     condition = ( varargin{1} == varargin{2} );
	     s1 = UnitTest.logicaltostring( varargin{1} );
	     s2 = UnitTest.logicaltostring( varargin{2} );
	   elseif ( ( isnumeric( varargin{1} ) && ( isnumeric( varargin{2} ) ) ) )
	     condition = ( varargin{1} == varargin{2} );
	     s1 = char( varargin{1} );
	     s2 = char( varargin{2} );
	   end
	   fmt = sprintf( '%s [%s =?= %s]', varargin{3}, s1, s2 );
       end
       if ( condition )
	 lead = '-- PASS: ';
       else
	 lead = '-- FAIL: ';
	 self.STATUS = 1;
       end
       fmt = sprintf( '%s%s', lead, fmt  );
       self.message( fmt );
       OBJ = self;
     end

     function exit( obj )
       % ----------------------------------------------------------------
       % Leave according to how testing completed
       % ----------------------------------------------------------------
       exit( obj.STATUS );
     end% function - exit

     function VALUE = exitcode( self )
       VALUE = self.STATUS;
     end

     function VALUE = initialize( obj, varargin )
       obj.VERBOSE_LEVEL = int16( 0 );
       obj.STATUS = int16( 0 );
       VALUE = obj;
     end% function initialize

     function message( obj, text )
       text = char( text );
       disp( text );
     end

     function msgdebug( obj, level, fmt, varargin )
       if ( obj.VERBOSE_LEVEL >= level )
         if( length( varargin ) > 0 )
           fmt = sprintf( fmt, varargin{:} );
         end
         fmt = sprintf( '-- DBUG: %s', fmt );
         obj.message( fmt );
       end
     end%

     function msginfo( obj, fmt, varargin )
       if( length( varargin ) > 0 )
         fmt = sprintf( fmt, varargin{:} );
       end
       fmt = sprintf( '-- INFO: %s', fmt );
       obj.message( fmt );
     end%

   end% methods
end
