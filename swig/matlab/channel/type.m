function value = type( self ) % -*- mode: octave; -*-
% TYPE retrieve the type of the channel
%
%   Retrieves the type of the channel
%
%   INPUTS:
%     SELF   : ndsm.channel
%
%   OUTPUTS:
%     VALUE
  value = self.type( )
end
