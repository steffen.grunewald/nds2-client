import nds2
import os
import sys

host = 'localhost'
if 'NDS_TEST_HOST' in os.environ:
    host = os.environ['NDS_TEST_HOST']
port = 31200
if 'NDS_TEST_PORT' in os.environ:
    port = int(os.environ['NDS_TEST_PORT'])

keys = ["NDS_TEST_PORT", "NDS_TEST_HOST", "NDSSERVER", "NDS2_CLIENT_HOSTNAME", "NDS2_CLIENT_PORT", "NDS2_CLIENT_PROTOCOL_VERSION"]
for key in keys:
    try:
        del os.environ[key]
    except:
        pass


proto = nds2.connection.PROTOCOL_TRY
if '-option1' in sys.argv:
    val = "{0}:{1}".format(host, port)
    os.environ['NDSSERVER'] = val
    os.putenv('NDSSERVER', val)
elif '-option2' in sys.argv:
    os.environ['NDS2_CLIENT_HOSTNAME'] = host
    os.putenv('NDS2_CLIENT_HOSTNAME', host)
    os.environ['NDS2_CLIENT_PORT'] = "{0}".format(port)
    os.putenv('NDS2_CLIENT_PORT', "{0}".format(port))

# assert that a parameters object gets the settings
# we expect
p = nds2.parameters()
assert(p.get('HOSTNAME') == host)
assert(p.get('PORT') == "{0}".format(port))

if 'params' in sys.argv:
    conn = nds2.connection(nds2.parameters())
else:
    conn = nds2.connection()
