% -*- mode: octave -*-
%
% Unit tests against mock-up NDS 1 server
%
% Copyright (C) 2014  Leo Singer <leo.singer@ligo.org>
%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License along
% with this program; if not, write to the Free Software Foundation, Inc.,
% 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
%

nds2;
UnitTest;

function validate_buffer( buf, i, lead, cross_check,
			 start, samples, mult,
			 bytepattern )
  unit_test_check(cross_check.data_type,
		  buf.data_type,
		  sprintf('%sValidating buffer{%d} data_type field',
			  lead, i))
  unit_test_check(buf.gps_seconds,
		  start,
		  sprintf('%sValidating bufferl{%d} gps_seconds field',
			  lead, i))
  unit_test_check(buf.gps_nanoseconds,
		  0,
		  sprintf('%sValidating bufferl{%d} gps_nanoseconds',
			  lead, i))
  unit_test_check(length(buf.data),
		  samples * (buf.sample_rate * mult),
		  sprintf('%sValidating bufferl{%d} sample_rate',
			  lead, i))
  unit_test_check(typecast(buf.data, "uint8"),
		  repmat(bytepattern, length(buf.data) , 1),
		  sprintf('%sValidating bufferl{%d} data field',
			  lead, i))
endfunction # validate_buffer

raw_bytepatterns = {
    cast([0xB0; 0x00], "uint8"),
    cast([0xDE; 0xAD; 0xBE; 0xEF], "uint8"),
    cast([0x00; 0x00; 0x00; 0x00; 0xFE; 0xED; 0xFA; 0xCE], "uint8"),
    cast([0xFE; 0xED; 0xFA; 0xCE], "uint8")
};

min_bytepatterns = {
    cast([0x01; 0x01], "uint8"),
    cast([0x02; 0x02; 0x02; 0x02], "uint8"),
    cast([0x03; 0x03; 0x03; 0x03; 0x03; 0x03; 0x03; 0x03], "uint8"),
};

max_bytepatterns = {
    cast([0x11; 0x11], "uint8"),
    cast([0x12; 0x12; 0x12; 0x12], "uint8"),
    cast([0x13; 0x13; 0x13; 0x13; 0x13; 0x13; 0x13; 0x13], "uint8"),
};

mean_bytepatterns = {
    cast([0x1A; 0x1A; 0x1A; 0x1A; 0x1A; 0x1A; 0x1A; 0x1A], "uint8"),
    cast([0x2A; 0x2A; 0x2A; 0x2A; 0x2A; 0x2A; 0x2A; 0x2A], "uint8"),
    cast([0x3A; 0x3A; 0x3A; 0x3A; 0x3A; 0x3A; 0x3A; 0x3A], "uint8")
};

rms_bytepatterns = {
    cast([0x1B; 0x1B; 0x1B; 0x1B; 0x1B; 0x1B; 0x1B; 0x1B], "uint8"),
    cast([0x2B; 0x2B; 0x2B; 0x2B; 0x2B; 0x2B; 0x2B; 0x2B], "uint8"),
    cast([0x3B; 0x3B; 0x3B; 0x3B; 0x3B; 0x3B; 0x3B; 0x3B], "uint8")
};

n_bytepatterns = {
    cast([0x1C; 0x1C; 0x1C; 0x1C], "uint8"),
    cast([0x2C; 0x2C; 0x2C; 0x2C], "uint8"),
    cast([0x3C; 0x3C; 0x3C; 0x3C], "uint8")
};

try
  #----------------------------------------------------------------------
  # Establish connection
  #----------------------------------------------------------------------

  hostname = unit_test_hostname( );
  port = unit_test_port( );

  conn = nds2.connection(hostname, port, nds2.connection.PROTOCOL_ONE);

  unit_test_check( conn.get_host(),
		   hostname,
		   'Hostname verification' );
  unit_test_check( conn.get_port( ),
		   port,
		   "Port number verification" );
  unit_test_check( conn.get_protocol( ),
		   nds2.connection.PROTOCOL_ONE,
		   'Protocol verification' );

  #----------------------------------------------------------------------
  # This test does a simple count of channels
  #
  # There is channels A - I (9)
  # Each channel has:
  #     Raw Data (+1)
  #     Second Trend Data (+5)
  #     Minute Trend Data (+5)
  #
  # NOTE:
  #     update this check when the channel count is
  #     changed on the mock server
  #------------------------------------------------------------------------

  channels = conn.find_channels("*");
  base_channel_count = 9;
  unit_test_check( length( channels ),
		   ( base_channel_count * (5*2 + 1) ),
		   'Verification of findchannels for query "*"' );

  channels = conn.find_channels("X*:{A,B,C,G,H,I}");
  unit_test_check( length(channels),
		   6,
		   'Validating number of channels matching pattern "X*:{A,B,C,G,H,I}"' )
  unit_test_check( sprintf(channels{1}),
		   "<X1:A (16Hz, ONLINE, INT16)>",
		   'Validating channel name for channel 1')
  unit_test_check( sprintf(channels{2}),
		   "<X1:B (16Hz, ONLINE, INT32)>",
		   'Validating channel name for channel 2')
  unit_test_check( sprintf(channels{3}),
		   "<X1:C (16Hz, ONLINE, INT64)>",
		   'Validating channel name for channel 3')
  unit_test_check( sprintf(channels{4}),
		   "<X1:G (16Hz, ONLINE, UINT32)>",
		   'Validating channel name for channel 4')
  unit_test_check( sprintf(channels{5}),
		   "<X1:H (65536Hz, ONLINE, FLOAT32)>",
		   'Validating channel name for channel 5')
  unit_test_check( sprintf(channels{6}),
		   "<X1:I (16384Hz, ONLINE, UINT32)>",
		   'Validating channel name for channel 6')

  channels = conn.find_channels("X1:G*");
  expectedChannels = sort({"<X1:G (16Hz, ONLINE, UINT32)>",
			   "<X1:G.max,m-trend (0.0166667Hz, MTREND, UINT32)>",
			   "<X1:G.max,s-trend (1Hz, STREND, UINT32)>",
			   "<X1:G.mean,m-trend (0.0166667Hz, MTREND, FLOAT64)>",
			   "<X1:G.mean,s-trend (1Hz, STREND, FLOAT64)>",
			   "<X1:G.min,m-trend (0.0166667Hz, MTREND, UINT32)>",
			   "<X1:G.min,s-trend (1Hz, STREND, UINT32)>",
			   "<X1:G.rms,m-trend (0.0166667Hz, MTREND, FLOAT64)>",
			   "<X1:G.rms,s-trend (1Hz, STREND, FLOAT64)>",
			   "<X1:G.n,m-trend (0.0166667Hz, MTREND, INT32)>",
			   "<X1:G.n,s-trend (1Hz, STREND, INT32)>"});
  unit_test_check( length(channels),
		   length(expectedChannels),
		   'Verification of findchannels for query "X1:G*"' )
  actualChannels = cell(length(channels), 1);
  for i = 1:length(channels)
    actualChannels{i} = sprintf(channels{i});
  end
  actualChannels = sort(actualChannels);
  for i = 1:length(channels)
    msg = sprintf( 'Validating channel name for channel %d', i );
		   unit_test_check( expectedChannels{i},
				    actualChannels{i},
				    msg );
  end

  bufs = conn.fetch(1000000000, 1000000004, {"X1:A", "X1:B", "X1:C", "X1:G"});
  unit_test_check( length(bufs), 4,
		   'Verification of fetch for query "{X1:A, X1:B, X1:C, X1:G}"' );
  for i = 1:length(bufs)
    buf = bufs{i};
    bytepattern = raw_bytepatterns{i};
    unit_test_check( buf.gps_seconds,
		     1000000000,
		     'Validating buffer gps_seconds field' )
    unit_test_check( buf.gps_nanoseconds,
		     0,
		     'Validating buffer gps_nanoseconds field' )
    unit_test_check( length(buf.data),
		     4 * buf.channel.sample_rate,
		     'Validating buffer sample_rate field' )
    unit_test_check( typecast(buf.data, "uint8"),
		     repmat(bytepattern, length(buf.data), 1),
		     'Validating buffer data field' )
  end

  for tselect = 1:2
    if (tselect == 1)
        start = 1000000000;
        end_time = 1000000020;
        mult = 1;
        samples = 20;
        trend = "s-trend";
    else
        start = 1000000020;
        end_time = 1000000140;
        mult = 60;
        samples = 2;
        trend = "m-trend";
    end

    # -------------------------------------------------------------------
    #  M I N
    # -------------------------------------------------------------------
    lead = sprintf('%s: %s: ', trend, 'MIN')
    channel_list = {["X1:A.min," trend], ["X1:B.min," trend], ["X1:C.min," trend]};
    bufs = conn.fetch(start, end_time, channel_list);
    unit_test_check( length(bufs),
		     3,
		     sprintf('%sValidating number of elements returned from fetch',
			     lead ) )
    for i = 1:length(bufs)
        buf = bufs{i};
        cross_check = conn.find_channels([buf.name "," trend]){1};
	validate_buffer( buf, i, lead, cross_check,
			start, samples, mult,
			min_bytepatterns{i});
    end

    # -------------------------------------------------------------------
    #  M A X
    # -------------------------------------------------------------------
    lead = sprintf('%s: %s: ', trend, 'MAX')
    channel_list = {["X1:A.max," trend], ["X1:B.max," trend], ["X1:C.max," trend]};
    bufs = conn.fetch(start, end_time, channel_list);
    unit_test_check(length(bufs),
		    3,
		    'MAX: Validating number of elements returned from fetch')
    for i = 1:length(bufs)
        buf = bufs{i};
        cross_check = conn.find_channels([buf.name "," trend]){1};
	validate_buffer( buf, i, lead, cross_check,
			start, samples, mult,
			max_bytepatterns{i});
    end

    # -------------------------------------------------------------------
    #   M E A N
    # -------------------------------------------------------------------
    lead = sprintf('%s: %s: ', trend, 'MEAN')
    channel_list = {["X1:A.mean," trend], ["X1:B.mean," trend], ["X1:C.mean," trend]};
    bufs = conn.fetch(start, end_time, channel_list);
    unit_test_check(length(bufs),
		    3,
		    'MEAN: Validating number of elements returned from fetch')
    for i = 1:length(bufs)
        buf = bufs{i};
        cross_check = conn.find_channels([buf.name "," trend]){1};
	validate_buffer( buf, i, lead, cross_check,
			start, samples, mult,
			mean_bytepatterns{i});
    end

    # -------------------------------------------------------------------
    #  R M S
    # -------------------------------------------------------------------
    lead = sprintf('%s: %s: ', trend, 'RMS')
    channel_list = {["X1:A.rms," trend], ["X1:B.rms," trend], ["X1:C.rms," trend]};
    bufs = conn.fetch(start, end_time, channel_list);
    unit_test_check(length(bufs),
		    3,
		    'RMS: Validating number of elements returned from fetch')
    for i = 1:length(bufs)
        buf = bufs{i};
        cross_check = conn.find_channels([buf.name "," trend]){1};
	validate_buffer( buf, i, lead, cross_check,
			start, samples, mult,
			rms_bytepatterns{i});
    end

    # -------------------------------------------------------------------
    #  N
    # -------------------------------------------------------------------
    lead = sprintf('%s: %s: ', trend, 'N')
    channel_list = {["X1:A.n," trend], ["X1:B.n," trend], ["X1:C.n," trend]};
    bufs = conn.fetch(start, end_time, channel_list);
    unit_test_check(length(bufs),
		    3,
		    'N: Validating number of elements returned from fetch')
    for i = 1:length(bufs)
        buf = bufs{i};
        cross_check = conn.find_channels([buf.name "," trend]){1};
	validate_buffer( buf, i, lead, cross_check,
			start, samples, mult,
			n_bytepatterns{i});
    end

  end

  conn.iterate({"X1:A", "X1:B", "X1:C"})
  bufs = conn.next();
  unit_test_check(length(bufs),
		  3,
		  'Validating number of elements returned from next')
  for i = 1:length(bufs)
    buf = bufs{i};
    bytepattern = raw_bytepatterns{i};
    #unit_test_check(buf.gps_seconds, 1000000000)
    #unit_test_check(buf.gps_nanoseconds, 0)
    #unit_test_check(length(buf.data), buf.sample_rate)
    #unit_test_check(typecast(buf.data, "uint8"), repmat(bytepattern, length(buf.data), 1))
  end

  % connection automatically closed when garbage collected,
  % but test explicit close anyway
  conn.close()
catch err
  s = unit_test_dump_stack( err.stack );
  unit_test_msginfo( s );
  unit_test_check( false, ...
		   true, ...
		   sprintf( "Caught unexpected excption: %s\n%s", ...
			   err.message, ...
			   s ) );
end
