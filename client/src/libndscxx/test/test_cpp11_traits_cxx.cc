//
// Created by jonathan.hanks on 4/28/18.
//

#include <type_traits>

#include "nds.hh"

int
main( int argc, char* argv[] )
{
    // This test will fail to compile if these come up false.

    static_assert( std::is_move_assignable< NDS::epoch >::value,
                   "Checking for move assignment" );
    static_assert( std::is_move_constructible< NDS::epoch >::value,
                   "Checking for move consturction" );

    static_assert( std::is_move_assignable< NDS::channel >::value,
                   "Checking for move assignment" );
    static_assert( std::is_move_constructible< NDS::channel >::value,
                   "Checking for move consturction" );

    static_assert( std::is_move_assignable< NDS::channels_type >::value,
                   "Checking for move assignment" );
    static_assert( std::is_move_constructible< NDS::channels_type >::value,
                   "Checking for move consturction" );

    static_assert( std::is_move_assignable< NDS::buffer >::value,
                   "Checking for move assignment" );
    static_assert( std::is_move_constructible< NDS::buffer >::value,
                   "Checking for move consturction" );

    static_assert( std::is_move_assignable< NDS::buffers_type >::value,
                   "Checking for move assignment" );
    static_assert( std::is_move_constructible< NDS::buffers_type >::value,
                   "Checking for move consturction" );

    return 0;
}
