#!/usr/bin/env python
from __future__ import print_function
import os
import sys

import nds2 as nds

host = 'localhost'
if 'NDS_TEST_HOST' in os.environ:
    host = os.environ['NDS_TEST_HOST']
port = 31200
if 'NDS_TEST_PORT' in os.environ:
    port = int(os.environ['NDS_TEST_PORT'])

c = nds.connection(host, port)
lst = c.find_channels('*')
assert len(lst) == 5

for item in lst:
    print( item )

chans = ["<X1:PEM-1 (256Hz, RAW, FLOAT32)>",
    "<X1:PEM-1 (1Hz, STREND, FLOAT32)>",
    "<X1:PEM-2 (256Hz, RAW, FLOAT32)>",
    "<X1:PEM-2 (1Hz, STREND, FLOAT32)>",
    "<X1:PEM-3 (256Hz, RAW, FLOAT32)>",
]

for i in range(len(chans)):
    print("testing %s == %s" % (chans[i], str(lst[i])))
    assert chans[i] == str(lst[i])

lst = c.find_channels('X1*1')
assert len(lst) == 2

for item in lst:
    print(item)

chans = ["<X1:PEM-1 (256Hz, RAW, FLOAT32)>",
    "<X1:PEM-1 (1Hz, STREND, FLOAT32)>",
]

for i in range(len(chans)):
    print("testing %s == %s" % (chans[i], str(lst[i])))
    assert chans[i] == str(lst[i])

lst = c.find_channels('X1*', nds.channel.CHANNEL_TYPE_STREND)
assert len(lst) == 2

for item in lst:
    print(item)

chans = ["<X1:PEM-1 (1Hz, STREND, FLOAT32)>",
    "<X1:PEM-2 (1Hz, STREND, FLOAT32)>",
]

for i in range(len(chans)):
    print("testing %s == %s" % (chans[i], str(lst[i])))
    assert chans[i] == str(lst[i])


c.close()
