#include "nds.hh"
#include "nds_testing.hh"

#include <iostream>
#include <sstream>
#include <cstdlib>
#include <string>

using namespace NDS;

int
main( int argc, char* argv[] )
{

    // connection::port_type port = 31200;
    connection::port_type port = 53197;
    if (::getenv( "NDS_TEST_PORT" ) )
    {
        std::istringstream ps(::getenv( "NDS_TEST_PORT" ) );
        ps >> port;
    }
    std::string hostname( "localhost" );
    if (::getenv( "NDS_TEST_HOST" ) )
    {
        hostname = std::string(::getenv( "NDS_TEST_HOST" ) );
    }
    std::cerr << "connecting to " << hostname << ":" << port << std::endl;
    pointer< connection > conn( make_unique_ptr< connection >(
        hostname, port, connection::PROTOCOL_TWO ) );

    connection::channel_names_type twochan;
    twochan.push_back( "X1:PEM-1" );
    twochan.push_back( "X1:PEM-2" );

    connection::channel_names_type onechan;
    onechan.push_back( "X1:PEM-1" );

    try
    {
        conn->fetch( 1000000000, 1000000001, twochan );
    }
    catch ( connection::daq_error& e )
    {
        std::cout << "We caught an exception: " << e.what( ) << std::endl;
    }
    return 0;
}
