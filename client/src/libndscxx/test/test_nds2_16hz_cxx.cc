//
// Created by jonathan.hanks on 1/12/21.
//
#include <cstdlib>
#include <cstdint>
#include <memory>
#include <iostream>

#include <nds.hh>

#include "test_macros.hh"
#include "nds_testing.hh"

int
main( int argc, char* argv[] )
{

    std::vector< std::string > args = convert_args( argc, argv );

    //---------------------------------------------------------------------
    // Obtain the port of the server
    //---------------------------------------------------------------------
    NDS::connection::port_type port = 31200;
    if (::getenv( "NDS_TEST_PORT" ) )
    {
        std::istringstream ps(::getenv( "NDS_TEST_PORT" ) );

        ps >> port;
    }

    std::string hostname( "localhost" );
    if (::getenv( "NDS_TEST_HOST" ) )
    {
        hostname = ::getenv( "NDS_TEST_HOST" );
    }

    std::cout << "Connecting to " << hostname << ":" << port << "\n";
    auto                             abort_at = 17;
    NDS::buffer::gps_nanosecond_type expected_nano = 0;
    NDS::buffer::gps_nanosecond_type delta = 1000000000 / 16;

    auto params = NDS::parameters( hostname, port );

    auto iter =
        NDS::iterate( params,
                      NDS::request_period( NDS::request_period::FAST_STRIDE ),
                      { "X1:FEC-117_CPU_METER,online" } );
    for ( auto& bufs : iter )
    {
        NDS::buffer& cur_buf = ( *bufs ).front( );
        //        std::cout << cur_buf.NameLong() << " " << cur_buf.Start() <<
        //        ":" << cur_buf.StartNano();
        //        std::cout << "-" << cur_buf.StopNano() << "\n";
        //        std::cout << "\t" << cur_buf.Samples() << "\n";
        NDS_ASSERT( ( *bufs )[ 0 ].Samples( ) == 1 );
        NDS_ASSERT( ( *bufs )[ 0 ].StartNano( ) == expected_nano );

        expected_nano += delta;
        if ( expected_nano == 1000000000 )
        {
            expected_nano = 0;
        }

        NDS_ASSERT( ( *bufs )[ 0 ].StopNano( ) == expected_nano );
        --abort_at;
        if ( abort_at == 0 )
        {
            break;
        }
    }
    return 0;
}