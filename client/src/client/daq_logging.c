/* -*- tab-width:8 c-basic-offset:4  indent-tabs-mode:nil -*- */
/* set vi: ts=8:softtabstop=4,shiftwidth=4,expandtab */

#include "daq_config.h"

#include <stdio.h>
#include <stdarg.h>

#include "nds_logging.h"

struct nds_log_
{
    int s_group_mask;
    int s_group_debug_level[ NDS_LOG_GROUP_SIZE_MAX ];
};

typedef struct nds_log_ nds_log_t;

static nds_log_t log_info = {
    /* --------------------------------------------------------------------
     * Set bit mask of things that should be debugged by
     * default.
     * ----------------------------------------------------------------- */
    ( 1 << NDS_LOG_GROUP_CONNECTION ) | ( 1 << NDS_LOG_GROUP_VERBOSE_ERRORS ),
    /* --------------------------------------------------------------------
     * Setting of the default logging levels
     * ----------------------------------------------------------------- */
    {
        30, /* NDS_LOG_GROUP_CONNECTION */
        30, /* NDS_LOG_GROUP_VERBOSE_ERRORS */
    }

};

int
nds_logging_check( int Group, int Level )
{
    int retval = 0;

    if ( ( ( 1 << Group ) & log_info.s_group_mask ) &&
         ( log_info.s_group_debug_level[ Group ] >= Level ) )
    {
        retval = 1;
    }

    return retval;
}

void
nds_logging_message( const char* MessageFormat, ... )
{
    char    buf[ 2048 ];
    va_list ap;

    va_start( ap, MessageFormat );

#if HAVE_VSNPRINTF_S
    vsnprintf_s
#else /* HAVE_VSNPRINTF_S */
    vsnprintf
#endif /* HAVE_VSNPRINTF_S */
        ( buf,
          sizeof( buf ),
#if HAVE_VSNPRINTF_S
          sizeof( buf ) - 1,
#endif /* HAVE_VSNPRINTF_S */
          MessageFormat,
          ap );

    fprintf( stderr, "%s", buf );

    va_end( ap );
}
