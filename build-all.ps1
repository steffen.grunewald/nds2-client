# -*- mode: powershell -*-
# -----------------------------------------------------------------------
#  This script will build all of the nds2 client for Windows
#
#  NOTES:
#    * PowerShell Documentation - https://ss64.com/ps
#    * During development on a virtual system
#      PS> Set-ExecutionPolicy -ExecutionPolicy Unrestricted
#
# -----------------------------------------------------------------------
Param( 
    [string]$KerberosDir = "C:\Program Files\MIT\Kerberos",
    [string]$CMakeConfig = "Release",
    [string]$CMakeGenerator = "Visual Studio 14 2015 Win64",
    [switch]$EnableTesting = $true,
    [switch]$EnableBuildCore = $true,
    [switch]$EnableBuildSWIG = $true
)

$SourceDir = Split-Path -Parent -Path $MyInvocation.MyCommand.Definition
$BasenameSourceDir = Split-Path -Leaf -Path $SourceDir
$DataDir = Join-Path -path (Split-Path -Parent -Path $SourceDir) -childPath "nds2-test-blobs"
$BuildDir = Join-Path -path (get-item env:USERPROFILE).Value -childPath "build"
$BuildDir = Join-Path -path  $BuildDir -childPath "nds"
$BuildDir = Join-Path -path  $BuildDir -childPath $BasenameSourceDir

$SWIGSourceDir = "$SourceDir\swig"
$BasenameSWIGSourceDir = Split-Path -Leaf -Path $SWIGSourceDir
$SWIGBuildDir = Join-Path -path (get-item env:USERPROFILE).Value -childPath "build"
$SWIGBuildDir = Join-Path -path  $SWIGBuildDir -childPath "nds"
$SWIGBuildDir = Join-Path -path  $SWIGBuildDir -childPath "$BasenameSourceDir-$BasenameSWIGSourceDir"

function eval {
    param( $expression )

    $cmd = ""
    ForEach( $e in $expression ) {
	$cmd += " $e"
    }

    Invoke-Expression -Command $cmd
    if ( $LASTEXITCODE -ne 0 )
    {
        throw [ System.Management.Automation.RuntimeException ]::new( )
    }
}

function Build {
    param( $SourceDir,
           $BuildDir,
	   $DataDir,
	   $PackageName )

    #--------------------------------------------------------------------
    # Setup the space in which to build the system
    #--------------------------------------------------------------------
    write-output $BuildDir
    try
    {
	# Purge previous builds
	Remove-Item -recurse -force $BuildDir
    }
    catch
    {
    }
    try
    {
	# Create space for new build
	New-Item -path $BuildDir -type directory
	Set-Location -path $BuildDir
  
	# Configure the software
	$configureCmd = New-Object System.Collections.Generic.List[System.object]
	$configureCmd.Add( "cmake" )
	$configureCmd.Add( "-G" )
	$configureCmd.Add( "'$CMakeGenerator'" )
	$configureCmd.Add( "'-DREPLAY_BLOB_CACHE_DIR=$DataDir'" )
	$configureCmd.Add( "'-DWITH_GSSAPI=$KerberosDir'" )
	$configureCmd.Add( "'-DENABLE_SWIG_JAVA=YES'" )
	$configureCmd.Add( "'-DENABLE_SWIG_MATLAB=YES'" )
	$configureCmd.Add( "'-DENABLE_SWIG_PYTHON2=NO'" )
	$configureCmd.Add( "'-DENABLE_SWIG_PYTHON3=NO'" )
	$configureCmd.Add( "'-DENABLE_SWIG_OCTAVE=NO'" )
	$configureCmd.Add( "'$SourceDir'" )
	# write-output "cmake $configureArgs"
	eval $configureCmd

	$buildCmd = New-Object System.Collections.Generic.List[System.object]
	$buildCmd.Add( "cmake" )
	$buildCmd.Add( "--build" )
	$buildCmd.Add( "." )
	$buildCmd.Add( "--config" )
	$buildCmd.Add( $CMakeConfig )
	eval $buildCmd

	if ( $EnableTesting )
	{
	    $testCmd = New-Object System.Collections.Generic.List[System.object]
	    $testCmd.Add( "ctest" )
	    $testCmd.Add( "--build-config" )
	    $testCmd.Add( $CMakeConfig )
	    eval $testCmd
	}

	$packagingCmd = New-Object System.Collections.Generic.List[System.object]
	$packagingCmd.Add( "cpack" )
	eval $packagingCmd

	$installer = Get-ChildItem -Path $PackageName-*-win64.exe
        $installCmd  = New-Object System.Collections.Generic.List[System.object]
        $installCmd.Add( "$installer" )
        $installCmd.Add( "/S" )  # Silent
        eval $installCmd
    }
    catch
    {
	write-host "$($_.Exception.GetType().FullName)" -ForegroundColor Red
	write-host "$($_.Exception).Message( )" -ForegroundColor Red
    }
}

# write-output $BasenameSourceDir
# write-output (get-item env:USERPROFILE).Value

if ( $EnableBuildCore )
{
    Build $SourceDir $BuildDir $DataDir "nds2-client"
}
if ( $EnableBuildSWIG )
{
    Build $SWIGSourceDir $SWIGBuildDir $DataDir "nds2-client-swig"
}