/* -*- mode: C++ ; c-basic-offset: 2; indent-tabs-mode: nil -*- */

%include "std_string.i"
%include "std_vector.i"
%include "std_pair.i"

%include "nds_buffer.i"

%shared_ptr(availability)
%shared_ptr(availability_list_type)
%shared_ptr(simple_availability_list_type)

%shared_ptr(segment)
%shared_ptr(segment_list_type)
%shared_ptr(simple_segment)
%shared_ptr(simple_segment_list_type)

%{
#include <sstream>

#include "nds_availability.hh"

using NDS::epoch;
using NDS::segment;
using NDS::simple_segment;
using NDS_swig::simple_segment_list_type;
using NDS_swig::simple_availability_list_type;
using NDS_swig::segment_list_type;
using NDS_swig::availability;
using NDS_swig::availability_list_type;

%}

%nds_doc_class_seperator( )

%nds_doc_class_begin(segment)
%nds_doc_brief("An availability segment defines when data is available.")
%nds_doc_details("LIGO data is not always avaialble.  Channels are added, removed,"%nds_doc_nl( )
                 "the data collection services or the instrument have downtime.  This"%nds_doc_nl( )
	 			 "structure holds information about a time span when data is available."
                 %nds_doc_nl( )
                 "An individual segment holds data on when a channel (not specified in the"%nds_doc_nl( )
                 "structure) was available in a  specific frame type in a given [start,stop) time frame."%nds_doc_nl( )
                 %nds_doc_nl( )
				 "This information is only available for NDS2")
%nds_doc_class_end( )
struct segment
{
	std::string frame_type;
	buffer::gps_second_type gps_start;
	buffer::gps_second_type gps_stop;

	segment();
	segment(buffer::gps_second_type gps_start, buffer::gps_second_type gps_stop);
	segment(std::string frame_type, buffer::gps_second_type gps_start, buffer::gps_second_type gps_stop);
};

%nds_doc_class_seperator( )

%nds_doc_class_begin(simple_segment)
%nds_doc_brief("A simple [start,stop) range for denoting segments")
%nds_doc_class_end( )
struct simple_segment {
	typedef buffer::gps_second_type gps_second_type;

	simple_segment();
	simple_segment(const simple_segment &other);
	~simple_segment();
	simple_segment(gps_second_type start, gps_second_type stop);


	// this is ignored by SWIG
	//simple_segment &operator=(const simple_segment &other);

	gps_second_type gps_start;
	gps_second_type gps_stop;
};

%nds_doc_class_seperator( )

%shared_ptr( std::vector< std::shared_ptr< simple_segment> > );
%template(vectorSimpleSegment) std::vector< std::shared_ptr< simple_segment> >;

%nds_doc_class_begin(simpleSegmentListType)
%nds_doc_brief("A list of simple_segment objects.  This does not contain the channel name.")
%nds_doc_class_end( )
class simple_segment_list_type
  : public std::vector< std::shared_ptr< simple_segment > > {
public:
};

%nds_doc_class_seperator( )

%shared_ptr( std::vector< std::shared_ptr< simple_segment_list_type > > );
%template(vectorSimpleSegmentListType) std::vector< std::shared_ptr< simple_segment_list_type > >;
%nds_doc_class_begin(simpleAvailabilityListType)
%nds_doc_brief("A list of lists containing simple_segment objects")
%nds_doc_class_end( )
class simple_availability_list_type
  : public std::vector< std::shared_ptr< simple_segment_list_type> > {
public:
};

%extend simple_availability_list_type {
  std::string ToString( )
  {
    std::ostringstream msg;

    msg << ( *self );

    return msg.str( );
  }
}

%nds_doc_class_seperator( )

%shared_ptr( std::vector< std::shared_ptr< segment > > );
%template(vectorSegment) std::vector< std::shared_ptr< segment > >;
%nds_doc_class_begin(segment_list_type)
%nds_doc_brief("A list of detailed segments.  This does not contain the channel name.")
%nds_doc_class_end( )
class segment_list_type
  : public std::vector< std::shared_ptr< segment > >
{
public:
};

%nds_doc_class_seperator( )

%nds_doc_class_begin(availability)
%nds_doc_brief("Availability information for a single channel.")
%nds_doc_details("This contains a channel name, and a list of segments in which"%nds_doc_nl( )
				 "the data is available.")
%nds_doc_class_end( )
struct availability
{
	std::string name;
	segment_list_type data;

	%nds_doc_method_begin(availability,simpleList,)
	%nds_doc_body_begin( )
	%nds_doc_returns("A list of simple segments representing the channels avialability")
	%nds_doc_remark("Use this if you only care about availability time spans and do not need frame types.")
	%nds_doc_body_end( )
	%nds_doc_method_end( )
	std::shared_ptr< simple_segment_list_type > simple_list();
};

%nds_doc_class_seperator( )

%shared_ptr( std::vector< std::shared_ptr< availability > > )
%template(availabilityListType) std::vector< std::shared_ptr< availability > >;
%nds_doc_class_begin(availabilityListType)
%nds_doc_brief("A list of availabilities (channels, availablity segments)")
%nds_doc_details("This contians a list of detailed availaiblity lists for several channels")
%nds_doc_class_end( )
class availability_list_type
  : public std::vector< std::shared_ptr< availability > >
{
public:
	~availability_list_type();

	%nds_doc_method_begin(availabilityListType,simpleList,)
	%nds_doc_body_begin( )
	%nds_doc_returns("A list of lists of simple segments representing the channels avialability")
	%nds_doc_remark("Use this if you only care about availability time spans and do not need frame types.")
	%nds_doc_body_end( )
	%nds_doc_method_end( )
	simple_availability_list_type simple_list() const;
};

%extend availability {
  std::string ToString( )
  {
    std::ostringstream msg;

    msg << ( *self );

    return msg.str( );
  }
}

%extend availability_list_type {
  std::string ToString( )
  {
    std::ostringstream msg;

    msg << ( *self );

    return msg.str( );
  }
}

%nds_doc_class_seperator( )
