/* -*- mode: C++ ; c-basic-offset: 2; indent-tabs-mode: nil -*- */

#ifndef SWIG__COMMON__NDS_VERSION_HH
#define SWIG__COMMON__NDS_VERSION_HH

#include <string>

#include "nds_export.hh"

namespace NDS
{
    inline namespace abi_0
    {
        DLL_EXPORT std::string version( );
    }
};

#endif // SWIG__COMMON__NDS_VERSION_HH
