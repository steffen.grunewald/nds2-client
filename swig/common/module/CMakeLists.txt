# -*- mode: cmake; coding: utf-8; cmake-tab-width: 4; indent-tabs-mode: nil; -*- vim:fenc=utf-8:ft=cmake:et:sw=4:ts=4:sts=4
include(UseCopyIfDifferent)

configure_file(nds_swig_version.hh.in ${CMAKE_CURRENT_BINARY_DIR}/nds_swig_version.hh @ONLY)

set( nds_hdrs
    nds_swig.hh
    ${CMAKE_CURRENT_BINARY_DIR}/nds_swig_version.hh
    )
set( SWIG_SOURCES
    nds.i
    nds_availability.i
    nds_buffer.i
    nds_channel.i
    nds_connection.i
    nds_epoch.i
    nds_version.i
    )
install(
    FILES ${SWIG_SOURCES} ${nds_hdrs}
    DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/${PROJECT_BASENAME}
    COMPONENT Development
    )
