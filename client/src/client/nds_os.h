/* -*- tab-width:8 c-basic-offset:4  indent-tabs-mode:nil -*- */
/* set vi: ts=8:softtabstop=4,shiftwidth=4,expandtab */

#ifndef NDS_OS_H
#define NDS_OS_H

#include <stdio.h>

#include "daqc.h"
#include "daqc_private.h"
#ifdef __cplusplus
#include <stdint.h>
#include <limits>
#include <stdexcept>
#ifndef INT32_MAX
#define INT32_MAX ( std::numeric_limits< int32_t >::max( ) )
#endif /* INT32_MAX */
#endif /* __cplusplus */

/*
 * If you desire not to use the Windows _s functions, then add the following
 * lines to the source file:
 *
 * #if _WIN32
 * #define _CRT_SECURE_NO_WARNINGS 1
 * #endif
 *
 */
void socket_io_non_blocking( nds_socket_type Socket );
void socket_io_default( nds_socket_type Socket );

void pstrerror_r( char* StrErrBuf, size_t StrErrBufLen );

#ifdef __cplusplus
template < typename DEST, typename SOURCE >
inline DEST
check_int_overflow( SOURCE Source )
{
    if ( sizeof( DEST ) < sizeof( SOURCE ) )
    {
        switch ( sizeof( DEST ) )
        {
        case 4:
            if ( Source > INT32_MAX )
            {
                throw std::overflow_error(
                    "Rounding error resulting from integer truncation" );
            }
        }
    }
    return static_cast< DEST >( Source );
}
#endif /* __cplusplus */

#if WIN32 || WIN64
/* ----------------------------------------------------------------------
 * Windows System
 * ----------------------------------------------------------------------
 */

#define DLL_EXPORT __declspec( dllexport )

#include <io.h>

typedef size_t ssize_t;
typedef int    socklen_t;

#if __cplusplus

/* modes for access are not defined by io.h */
#define F_OK ( (int)0 )
#define W_OK ( (int)2 )

static inline int
close( nds_socket_type s )
{
    return closesocket( s );
}
static inline int
dup( int fd )
{
    return _dup( fd );
}
static inline FILE*
fdopen( int fd, const char* mode )
{
    return _fdopen( fd, mode );
}
static inline ssize_t
read( nds_socket_type s, void* buf, size_t len )
{
    return recv(
        s, reinterpret_cast< char* >( buf ), static_cast< int >( len ), 0 );
}
static inline void
sleep( int seconds )
{
    Sleep( seconds * 1000 );
}
static inline void
usleep( int microseconds )
{
    Sleep( microseconds / 1000 );
}
static inline ssize_t
write( nds_socket_type s, const void* buf, size_t count )
{
    return send( s,
                 reinterpret_cast< const char* >( buf ),
                 static_cast< int >( count ),
                 0 );
}

static inline errno_t
strerror_r( int errnum, char* buf, size_t buflen )
{
    return strerror_s( buf, buflen, errnum );
}

#define perror( message ) nds_perror( message )

#else /* __cplusplus */

#define close( fd ) closesocket( fd )
#define dup( oldfd ) _dup( oldfd )
#define fdopen( path, mode ) _fdopen( path, mode )
#define read( fd, buf, len ) recv( fd, buf, len, 0 )
#define sleep( Seconds ) Sleep( Seconds * 1000 )
#define usleep( MicroSeconds ) Sleep( MicroSeconds / 1000 )
#define write( fd, buf, count ) send( fd, buf, count, 0 )
#define perror( message ) nds_perror( message )
#define unlink( filename ) _unlink( filename )

#define strerror_r( a, b, c ) strerror_s( b, c, a )
#endif /* __cplusplus */

#define access( path, mode ) _access( path, mode )
#define strdup( str ) _strdup( str )

#else /* _WIN32 || _WIN64 */
/* ----------------------------------------------------------------------
 * Non-Windows System
 * ----------------------------------------------------------------------
 */
#define DLL_EXPORT
#define strncpy_s( dest, dest_len, src, count ) strncpy( dest, src, count )

#define strcpy_s( dest, dest_len, src ) strcpy( dest, src )
#endif /* _WIN32 || _WIN64 */

#endif /* NDS_OS_H */
