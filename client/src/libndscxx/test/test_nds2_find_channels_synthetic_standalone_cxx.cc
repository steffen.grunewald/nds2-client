#include "nds.hh"
#include "nds_testing.hh"
#include "test_macros.hh"

#include <algorithm>
#include <cmath>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>

int
main( int argc, char** argv )
{
    std::vector< std::string > args = convert_args( argc, argv );

    NDS::connection::port_type port = 31200;
    if (::getenv( "NDS_TEST_PORT" ) )
    {
        std::istringstream ps(::getenv( "NDS_TEST_PORT" ) );
        ps >> port;
    }

    std::string hostname( "localhost" );
    if (::getenv( "NDS_TEST_HOST" ) )
        hostname = ::getenv( "NDS_TEST_HOST" );

    // This test only runs against NDS2
    NDS::connection::protocol_type proto = NDS::connection::PROTOCOL_TWO;

    NDS::parameters params( hostname, port, proto );

    NDS::channels_type lst = NDS::find_channels(
        params,
        NDS::channel_predicate( "*", NDS::epoch( 1200000000, 1210000000 ) ) );
    NDS_ASSERT( lst.size( ) == 5 );

    std::vector< std::string > ref_str1;
    ref_str1.emplace_back( "<X1:PEM-1 (256Hz, RAW, FLOAT32)>" );
    ref_str1.emplace_back( "<X1:PEM-1 (1Hz, STREND, FLOAT32)>" );
    ref_str1.emplace_back( "<X1:PEM-2 (256Hz, RAW, FLOAT32)>" );
    ref_str1.emplace_back( "<X1:PEM-2 (1Hz, STREND, FLOAT32)>" );
    ref_str1.emplace_back( "<X1:PEM-3 (256Hz, RAW, FLOAT32)>" );

    for ( int i = 0; i < lst.size( ); ++i )
    {
        std::cout << lst[ i ] << std::endl;
        NDS_ASSERT( lst[ i ].NameLong( ).compare( ref_str1[ i ] ) == 0 );
    }
}
