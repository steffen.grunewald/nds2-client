#ifndef __NDS2_CLIENT_DEBUG_STREAM_HH__
#define __NDS2_CLIENT_DEBUG_STREAM_HH__

#include <ostream>
#include "nds_export.hh"

namespace NDS
{
    namespace detail
    {
        DLL_EXPORT
        std::ostream& dout( );
    }
}

#endif