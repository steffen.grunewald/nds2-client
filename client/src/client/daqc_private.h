/* -*- tab-width:8 c-basic-offset:4  indent-tabs-mode:nil -*- */
/* set vi: ts=8:softtabstop=4,shiftwidth=4,expandtab */

#ifndef DAQC_PRIVATE_H
#define DAQC_PRIVATE_H

#include "daqc_net.h"

#if defined( WIN32 ) || defined( WIN64 )
typedef SOCKET nds_socket_type;
#else /* WIN32 || WIN64 */
typedef int nds_socket_type;
#endif /* WIN32 || WIN64 */

#if __cplusplus
extern "C" {
#endif /* __cplusplus */

typedef struct sockaddr_in socket_addr_t;

enum socket_states
{
    NDS_SOCKET_OK,
    NDS_SOCKET_FAILURE,
    NDS_SOCKET_TRANSIENT_FAILURE
};

/** Encapsulated internal status information.
  */
struct daq_private_
{
    /**  Socket to which the connection request is made. By default this
     *  is 8088.
     *  \brief DAQD server socket
     */
    nds_socket_type sockfd;

    /**  DAQD (NDS1) server  IP address
     */
    socket_addr_t* srvr_addr;

    /** Data connection socket
     */
    nds_socket_type datafd;

    size_t max_command_count;
    size_t cur_command_count;

    int last_command_timeout;
};

typedef struct daq_private_ daq_private_t;

/**  Allocate a private structure AND the socket_addr_t element. Call
  *  daq_private_init to initialize the data elements.
  *  \param Private pointer to receive new private structure address.
  */
void daq_private_create( daq_private_t** Private );

/**  Close the data socket. daq_private_data_close is protected against
  *  invalid \a Privte pointer of invalid (unopened) socket id.
 */
int daq_private_data_close( daq_private_t* Private );

/**  Free the private data structure. Call daq_private_srvr_disconnect
  *  to free the socket address and then free the Private structure. The
  *  Private structure pointer is set to NULL. daq_private_delete is
  *  protected against a NULL Private structure pointer address or NULL
  *  structure pointer.
  *  \param Private Address of private structure pointer.
  */
void daq_private_delete( daq_private_t** Private );

/**  Initialize all fields of a pre-allocated private data structure.
  *  The socket numbers are initialized to INVALID_SOCKET and if the
  *  server address field is allocated (srvr_addr not NULL), it is zeroed.
  *  \param Private is a pointer to the private structure to be initialized.
  */
void daq_private_init( daq_private_t* Private );

/**  Test whether the server port is connected.
  *  \param Private is a pointer to the private structure to be tested.
  *  \return Non-zero if the server port is connected.
  */
int daq_private_srvr_is_open( daq_private_t* Private );

/**  Set the server socket for blocking/non-blocking I/O. If the
  *  \a NonBlocking argument is non-zero, the socket is set for
  *  non-blocking I/O. If it is zero, the socket is set to block.
  *  \note Not protected against invalid Private pointer (NULL) or socket.
  *  \brief Select non-blocking or blocking server socket I/O.
  *  \param Private Private structure pointer.
  *  \param NonBlocking Select Blocking/Non-Blocking I/O.
  *  \return Zero if previously blocking I/O.
  */
int daq_private_srvr_nonblocking( daq_private_t* Private, int NonBlocking );

/**  Shut down server socket connection and close the socket. Invlidate the
  *  socket number. daq_private_srvr_close IS protected against both invalid
  *  (NULL) \a Private pointer and socket number.
  *  \brief Close server socket.
  *  \param Private Pointer to private structure
  */
void daq_private_srvr_close( daq_private_t* Private );

/**  Open a connection to the pre-specified server. The socket specified in
  *  sockfd is connected to the server whose address is specified in
  *  srvr_addr. No check is made for valid (non-NULL) private structure
  *  pointer or valid socket number.
  *  \brief Connect to a server
  *  \param Private Pointer to the private structure.
  *  \return Error if non-zero.
  */
int daq_private_srvr_connect( daq_private_t* Private );

/**  Free the socket_addr_t storage and set pointer (srvr_addr) to NULL.
  *  daq_private_srvr_disconnect is protected against NULL pointers to
  *  the private structure and to the socket addr structure.
  *  \param Private is a pointer to the Private structure to be disconnected.
  */
void daq_private_srvr_disconnect( daq_private_t* Private );

/**  Allocate an internet stream (tcp) socket to communicate with a server.
  *  Set socket to allow reuse.
  */
int daq_private_srvr_open( daq_private_t* Private );

/** Record that the last command timed out.  We want to close off the session.
 *  However we need to record that it happened so a get_last_message call can
 *  be given a good message.
 */
void daq_private_timeout_session( daq_private_t* Private );

/** Return true if the session has timed out
 */
int daq_private_is_session_in_timeout( daq_private_t* Private );

#if __cplusplus
}
#endif /* __cplusplus */

#endif /* DAQC_PRIVATE_H */
