#------------------------------------------------------------------------
# -*- mode: cmake -*-
#------------------------------------------------------------------------
# cx_target_manual( )
#
# TARGET - Intended audience for the document
#    * Developer - Developer of the library being described
#    * Admin - Person responsible for installation of the software
#        and the services which may be part of the package
#    * User - Application developer who uses the library or tools
# TEMPLATE - Doxygen configuration file which has place holders
#    (ex: DiskCacheManual.cfg.in)
#------------------------------------------------------------------------
include( Autotools/cm_msg_error )
include( CMakeParseArguments )

macro(cp_set_boolean VAR )
  if ( ${ARG_${VAR}} )
    set( ${VAR} YES )
  else( )
    set( ${VAR} NO )
  endif( )
endmacro()

macro(cp_set_one_value VAR )
  set(options
          )
  set(oneValueArgs
          DEFAULT
          )
  set(multiValueArgs
          )

  cmake_parse_arguments(ARG "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )
  if ( ARG_${VAR} )
    set( ${VAR} ${ARG_${VAR}} )
  else( )
    if ( ARG_DEFAULT )
      set( ${VAR} ${ARG_DEFAULT} )
    else( )
      set( ${VAR} )
    endif( )
  endif( )
  string(REPLACE ";" " " ${VAR} "${${VAR}}")
endmacro()

macro(cp_set_multi_value VAR )
  set(options
          )
  set(oneValueArgs
          )
  set(multiValueArgs
          DEFAULT
          )

  cmake_parse_arguments(ARG "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )
  if ( ARG_${VAR} )
    set( ${VAR} ${ARG_${VAR}} )
  else( )
    if ( ARG_DEFAULT )
      set( ${VAR} ${ARG_DEFAULT} )
    else( )
      set( ${VAR} )
    endif( )
  endif( )
  string(REPLACE ";" " " ${VAR} "${${VAR}}")

endmacro()

function(cx_target_manual)

  find_package(Doxygen)

  if (NOT DOXYGEN_FOUND)
    #--------------------------------------------------------------------
    # Nothing to do so leave now
    #--------------------------------------------------------------------
    return( )
  endif()
  #----------------------------------------------------------------------
  # With doxygen available, a manual can be generated
  #----------------------------------------------------------------------
  set(options
          EXTRACT_ALL
          EXTRACT_PRIVATE
          EXTRACT_STATIC
          EXTRACT_LOCAL_CLASSES
          EXTRACT_LOCAL_METHODS
          EXTRACT_ANON_NSPACES
          HIDE_UNDOC_MEMBERS
          HIDE_UNDOC_CLASSES
          )
  set(oneValueArgs
          MAIN_PAGE
          OUTPUT_NAME
          TARGET
          TEMPLATE
          ENABLE_SECTIONS
          )
  set(multiValueArgs
          EXCLUDE
          EXCLUDE_PATTERNS
          )

  cmake_parse_arguments(ARG "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )
  if ( NOT ARG_TARGET )
    cm_msg_error( "cx_target_manual called without TARGET argument" )
  endif( )
  if ( NOT ARG_OUTPUT_NAME )
    set ( ARG_OUTPUT_NAME "${ARG_TARGET}" )
  endif ()
  if ( NOT ARG_TEMPLATE )
    cm_msg_error( "cx_target_manual called without TEMPLATE argument" )
  endif( )
  get_filename_component( output_ ${ARG_TEMPLATE} NAME )
  string( REGEX REPLACE "[.]in$" "" output_ ${output_} )
  set( cfg_ ${CMAKE_CURRENT_BINARY_DIR}/${ARG_TARGET}/${output_} )
  file( MAKE_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/${ARG_OUTPUT_NAME} )

  set( INPUT ${CMAKE_SOURCE_DIR}/src )
  set( OUTPUT_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/${ARG_OUTPUT_NAME} )
  set( EXAMPLE_PATH ${INPUT} )
  set( STRIP_FROM_PATH ${INPUT})

  set( ENABLE_SECTIONS ${ARG_ENABLE_SECTIONS} )
  cp_set_boolean( EXTRACT_ALL )
  cp_set_boolean( EXTRACT_ALL )
  cp_set_boolean( EXTRACT_PRIVATE )
  cp_set_boolean( EXTRACT_STATIC )
  cp_set_boolean( EXTRACT_LOCAL_CLASSES )
  cp_set_boolean( EXTRACT_LOCAL_METHODS )
  cp_set_boolean( EXTRACT_ANON_NSPACES )
  cp_set_boolean( HIDE_UNDOC_MEMBERS )
  cp_set_boolean( HIDE_UNDOC_CLASSES )

  cp_set_one_value( MAIN_PAGE )

  cp_set_multi_value( EXCLUDE DEFAULT CMakeLists.txt )
  cp_set_multi_value( EXCLUDE_PATTERNS DEFAULT */sqlite3.* )

  configure_file( ${ARG_TEMPLATE} ${cfg_} @ONLY )

  add_custom_target( ${ARG_TARGET}
          COMMAND ${DOXYGEN_EXECUTABLE} ${cfg_}
          WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
          DEPENDS ${cfg_}
          COMMENT "Generating ${ARG_TARGET} documentation"
          VERBATIM)


  if ( NOT TARGET doc )
    add_custom_target( doc )
  endif( )
  add_dependencies( doc ${ARG_TARGET} )
  install(CODE
          "execute_process(COMMAND \"${CMAKE_COMMAND}\" --build \"${CMAKE_CURRENT_BINARY_DIR}\" --target ${ARG_TARGET})")
  install(
          DIRECTORY ${OUTPUT_DIRECTORY}/html/
          DESTINATION ${CMAKE_INSTALL_DOCDIR}/${ARG_OUTPUT_NAME})
endfunction()
