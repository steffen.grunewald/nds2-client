#include "nds.hh"

#include <algorithm>
#include <memory>
#include <limits>
#include <cstdlib>
#include <iostream>
#include <sstream>
#include <string>

#include "test_macros.hh"
#include "nds_testing.hh"

int
main( int argc, char* argv[] )
{
    auto args = convert_args( argc, argv );

    //---------------------------------------------------------------------
    // Obtain the port of the server
    //---------------------------------------------------------------------
    NDS::connection::port_type port = 31200;
    if (::getenv( "NDS_TEST_PORT" ) )
    {
        std::istringstream ps(::getenv( "NDS_TEST_PORT" ) );

        ps >> port;
    }

    std::string hostname( "localhost" );
    if (::getenv( "NDS_TEST_HOST" ) )
    {
        hostname = ::getenv( "NDS_TEST_HOST" );
    }

    NDS::connection::protocol_type proto = NDS::connection::PROTOCOL_TWO;

    NDS::connection conn( hostname, port );
    NDS_ASSERT(
        conn.parameters( ).set( "GAP_HANDLER", "STATIC_HANDLER_NEG_INF" ) );

    auto channels = NDS::connection::channel_names_type{ "X1:PEM-1" };
    auto buffers = conn.fetch( 1219524930, 1219524930 + 4, channels );
    auto buf = buffers[ 0 ];

    std::for_each( buf.cbegin< std::int32_t >( ),
                   buf.cend< std::int32_t >( ),
                   []( std::int32_t val ) -> void {
                       NDS_ASSERT(
                           val == std::numeric_limits< std::int32_t >::min( ) );
                   } );
    return 0;
}