/* -*- mode: C++ ; c-basic-offset: 2; indent-tabs-mode: nil -*- */
/*
 * This file allows for SWIG specialization for Java
 */
#if defined(SWIGJAVA)

/* Make all functions and non-constant fields camel-case. */
%rename("%(lowercamelcase)s", %$ismember, %$isvariable, %$not %$hasconsttype) "";
%rename("%(lowercamelcase)s", %$isfunction) "";

%typemap(javapackage) nds2;

%javaconst(0);

#------------------------------------------------------------------------

%define %nds_doc_nl()"<br>
"
%enddef /* %nds_doc_nl */

%define %nds_doc_par()"<p>
"
%enddef /* %nds_doc_nl */

%define %nds_doc_brief(TEXT)
%nds_doc_par()TEXT%nds_doc_nl( )
%enddef

%define %nds_doc_details(TEXT)
%nds_doc_par()TEXT %nds_doc_nl( )
%enddef

%define %nds_doc_remark(TEXT)
"REMARK: " TEXT%nds_doc_nl( )
%enddef
%define %nds_doc_returns(TEXT)
"Returns " TEXT%nds_doc_nl( )%nds_doc_nl( )
%enddef

%define %nds_doc_param(CALL,TYPE,DESC)
%enddef

#------------------------------------------------------------------------

%define %nds_doc_class_begin(__CLASS__)
%typemap(javaimports) __CLASS__ "
/** "
%enddef

%define %nds_doc_class_end()
" */"
%enddef

%define %nds_doc_method_begin(__CLASS__,__METHOD__,__CALL__)
%javamethodmodifiers __CLASS__::__METHOD__(__CALL__) "
/** "
%enddef

%define %nds_doc_method_end()
" */
public";
%enddef

%define %nds_doc_body_begin()
""
%enddef

%define %nds_doc_body_end()
""
%enddef

%define %nds_doc_body_none()
""
%enddef

#------------------------------------------------------------------------

%define %nds_namespace() "nds2"
%enddef

#------------------------------------------------------------------------

%inline %{
  typedef std::string MATLABSTRINGTYPE;
%}

%typemap(jni) MATLABSTRINGTYPE "jobjectArray";
%typemap(jtype) MATLABSTRINGTYPE "char[][]";
%typemap(jstype) MATLABSTRINGTYPE "char[][]";

%typemap(javaout) MATLABSTRINGTYPE {
	return $jnicall;
}

%typemap(out) MATLABSTRINGTYPE {
	jclass clazz;
	jsize len;
	jcharArray innerArray;
	jchar *innerArrayChars;
	int i;

	/* Look up the class for an array of chars (char []). */
	clazz = JCALL1(FindClass, jenv, "[C");
	if (!clazz)
	{
		$cleanup
		return $null;
	}

	/* Create a new inner array -- which will store the chars in the string. */
	len = (jsize)( $1.size( ) );
	innerArray = JCALL1(NewCharArray, jenv, len);
	if (!innerArray)
	{
		$cleanup
		return $null;
	}

	/* Since a Java char is 16 bits wide, and in most C compilers a char is
	 * 8 bits wide, it's safest to copy the chars over byte by byte. */

	/* Get a pointer to the underlying array data. */
	innerArrayChars = JCALL2(GetCharArrayElements, jenv, innerArray, NULL);
	if (!innerArrayChars)
	{
		$cleanup
		return $null;
	}

	/* Copy character-for-character. */
	for (i = 0; i < len; i ++)
		innerArrayChars[i] = $1[i];

	/* Release the array data, causing a copy if necessary. */
	JCALL3(ReleaseCharArrayElements, jenv, innerArray, innerArrayChars, 0);

	/* Create a new array of char[] (a char[][]). */
	$result = JCALL3(NewObjectArray, jenv, 1, clazz, (jobject) innerArray);
	if (!$result)
	{
		$cleanup
		return $null;
	}

	/* Let go of innerArray. (GC will do this for us, but let's be tidy.) */
	JCALL1(DeleteLocalRef, jenv, (jobject) innerArray);
}



#------------------------------------------------------------------------

%include "nds_java_availability.i"
%include "nds_java_buffer.i"
%include "nds_java_channel.i"
%include "nds_java_channel_names_type.i"
%include "nds_java_connection.i"

%include "enumsimple.swg"
%include "std_string.i"

%include "nds.i"
//%include "nds_java_connection_iterator.i"
%include "nds_java_standalone.i"
%include "jniloader.i"

#endif /* defined(SWIGJAVA) */
