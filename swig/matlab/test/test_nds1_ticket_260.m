% -*- mode: octave -*-
%------------------------------------------------------------------------
%------------------------------------------------------------------------

unit_test = UnitTest;

hostname = unit_test.hostname( );
port = unit_test.port( );
protocol = ndsm.Connection.PROTOCOL_ONE;

%------------------------------------------------------------------------
% Establish the connection
%------------------------------------------------------------------------
conn = ndsm.Connection(hostname, port, protocol );


%------------------------------------------------------------------------
% Test parameters
%------------------------------------------------------------------------
tbeg_O2 = 1164556817;
tbeg_60= 60*round(tbeg_O2/60);
tdur_test = 6000;
tend_test=tbeg_60 + tdur_test;
chans_gnd = {'H1:ISI-GND_STS_HAM2_X_BLRMS_30M_100M.mean,m-trend'};
chans_bad = {'H1:ISI-GND_STS_HAM2_X_BLRMS_100M.mean,m-trend'};

%------------------------------------------------------------------------
% This is expected to pass with no errors
%------------------------------------------------------------------------
mybuff = conn.fetch(tbeg_60,tend_test,chans_gnd);

%------------------------------------------------------------------------
% This is expected to raise an exception, but not crash matlab
%------------------------------------------------------------------------
exception_triggered = 0;
try
    mybuff = conn.fetch(tbeg_60,tend_test,chans_bad)
catch ME
    exception_triggered = 1;
end
unit_test.check( exception_triggered, ...
                 1, ...
                 'Verify that an exception happened.' );

%------------------------------------------------------------------------
% This is explicitly done via garbage collection, but be nice
% and explicitly close the connection
%------------------------------------------------------------------------
conn.close()

unit_test.exit( );
