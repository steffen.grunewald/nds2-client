#include "nds.hh"

#include "daq_config.h"

namespace NDS
{
    inline namespace abi_0
    {
        std::string
        version( )
        {
            std::string ver( PACKAGE_VERSION );
            return ver;
        }
    }
};
