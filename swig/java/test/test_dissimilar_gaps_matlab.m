% -*- mode: octave -*-
%------------------------------------------------------------------------
%------------------------------------------------------------------------

START = 1112749800;
STOP = 1112762400;
CHANNELS = { 'H1:ASC-INP2_Y_INMON.mean,m-trend',
	     'L1:ASC-INP2_Y_INMON.mean,m-trend',
	     'H1:ASC-INP2_Y_INMON.n,m-trend',
	     'L1:ASC-INP2_Y_INMON.n,m-trend'
	    };

unit_test = unittest( );

hostname = unit_test.hostname( );
port = unit_test.port( );
protocol = nds2.connection.PROTOCOL_TWO;

%------------------------------------------------------------------------
% Establish the connection
%------------------------------------------------------------------------
	   
conn = nds2.connection( hostname, port, protocol );

%------------------------------------------------------------------------
% Gap Handler setup
%------------------------------------------------------------------------
conn.setParameter( 'GAP_HANDLER', 'STATIC_HANDLER_ZERO' );

bufs = conn.fetch( START, STOP, CHANNELS );
unit_test.check( bufs(1).dataType( ), ...
		 nds2.channel.DATA_TYPE_FLOAT64, ...
		 'Validating data type' );
unit_test.check( bufs(1).samples( ), ...
		 ( STOP - START ) / 60, ...
		 'Validating sample rate' );

%------------------------------------------------------------------------
% look for a zero filled gap at [1112752800-1112756400) on the
%   first channel
%------------------------------------------------------------------------
gap_start = 1112752800;
gap_stop = 1112756400;
start_index = (gap_start - START) / 60;
stop_index = (gap_stop - START) / 60;
data = double( bufs(1).getData( ) );

for ( i = start_index:stop_index )
    unit_test.check( data( i + 1 ), ...
		     0.0, ...
		     'Validating gap data' );
end
%------------------------------------------------------------------------
% look for  ~0.110272226 at time 1112752740 (sample prior to gap)
%------------------------------------------------------------------------
index = (1112752740 - START) / 60;
delta = abs( data( index + 1 ) - 0.110272226 );

unit_test.msgInfo( 'delta: %g', delta );
unit_test.check( delta <= 0.0000000001, ...
		 true, ...
		 'Validating delta prior to gap' );
%------------------------------------------------------------------------
% Finish
%------------------------------------------------------------------------
conn.close( );
exit( unit_test.exit_code( ) );
