#ifndef NDS_OCTAVE_MACROS_I
#define NDS_OCTAVE_MACROS_I

%begin %{
#include <octave/oct.h>
#include <octave/version.h>

#if ( (OCTAVE_MAJOR_VERSION<<16) + (OCTAVE_MINOR_VERSION<<8) + (OCTAVE_PATCH_VERSION + 0) >= (4<<16) + (2<<8) + (0) )

#define oct_mach_info octave::mach_info
#define nelem numel

#if ( (OCTAVE_MAJOR_VERSION<<16) + (OCTAVE_MINOR_VERSION<<8) + (OCTAVE_PATCH_VERSION + 0) >= (4<<16) + (4<<8) + (0) )
#define is_cell iscell
#define is_numeric_type isnumeric
  // #define octave_call_stack octave::call_stack
  // using octave::feval
#endif /* SWIG_OCTAVE_PREREQ(4,4,0) */

#endif /* SWIG_OCTAVE_PREREQ(4,2,0) */
%}

//-----------------------------------------------------------------------
// Macros
//-----------------------------------------------------------------------

%typemap( in, noblock=1 ) connection::channel_names_type ( Array<std::string> cellstr ) {

  if ( ! $input.is_cellstr( ) )
  {
    SWIG_exception(SWIG_TypeError, "channel_names must be a cell array of strings");
  }

  cellstr = $input.cellstr_value( );

  for ( octave_idx_type i = 0;
        i < cellstr.nelem();
        ++i )
  {
    $1.push_back( cellstr( i ) );
  }
}

%define %nds_vect_typemap(SWIG_VECT_TYPE, SWIG_VECT_T_TYPE)
%typemap(out, noblock=1 ) SWIG_VECT_TYPE ( octave_value_list outlist ) {
  for ( SWIG_VECT_TYPE::iterator
          cur = $1.begin( ),
          last = $1.end( );
        cur != last;
        ++cur )
  {
    outlist.append( SWIG_NewPointerObj( new SWIG_VECT_T_TYPE(*cur),
                                        $descriptor(SWIG_VECT_T_TYPE *),
                                        1 ));
  }
  $result = outlist.cell_value( );
}
%enddef

%define %nds_vect_p_typemap(SWIG_VECT_TYPE, SWIG_VECT_T_TYPE)
%typemap(in, noblock=1 ) const SWIG_VECT_TYPE& ( octave_value_list outlist ) {
  for ( SWIG_VECT_TYPE::iterator
          cur = $1->begin( ),
          last = $1->end( );
        cur != last;
        ++cur )
  {
    outlist.append( SWIG_NewPointerObj( new SWIG_VECT_T_TYPE(*cur),
                                        $descriptor(SWIG_VECT_T_TYPE *),
                                        1 ));
  }
  $result = outlist.cell_value( );
}
%enddef

//-----------------------------------------------------------------------
 // Suplimental code fragments
//-----------------------------------------------------------------------

%begin %{

#include "nds_buffer.hh"

namespace NDS
{

  typedef buffer* BUFFER_type;

}
%}

#endif /* NDS_OCTAVE_MACROS_I */
