#include "nds.hh"

#include <memory>
#include <cstdlib>
#include <iostream>
#include <sstream>
#include <string>

#include "test_macros.hh"
#include "nds_testing.hh"

int
main( int argc, char* argv[] )
{
    NDS::connection::port_type port = 31200;
    if (::getenv( "NDS_TEST_PORT" ) )
    {
        std::istringstream ps(::getenv( "NDS_TEST_PORT" ) );

        ps >> port;
    }

    std::string hostname( "localhost" );
    if (::getenv( "NDS_TEST_HOST" ) )
    {
        hostname = ::getenv( "NDS_TEST_HOST" );
    }

    NDS::parameters params( hostname, port, NDS::connection::PROTOCOL_TWO );
    NDS_ASSERT( params.set( "GAP_HANDLER", "STATIC_HANDLER_NAN" ) );

    pointer< NDS::connection > conn =
        make_unique_ptr< NDS::connection >( params );

    NDS::connection::channel_names_type chans{
        "H1:LSC-SRCL_OUT_DQ.mean,m-trend"
    };
    NDS::buffers_type bufs = conn->fetch( 1218564060, 1218564120, chans );

    NDS_ASSERT( bufs[ 0 ].Type( ) == NDS::channel::CHANNEL_TYPE_MTREND );
    return 0;
}