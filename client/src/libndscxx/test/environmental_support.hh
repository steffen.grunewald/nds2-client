#ifndef NDS2_CLIENT_TEST_ENV_SUPPORT_H
#define NDS2_CLIENT_TEST_ENV_SUPPORT_H

#include <string>

extern int set_env_val( const std::string& key, const std::string& val );

#endif // NDS2_CLIENT_TEST_ENV_SUPPORT_H