#------------------------------------------------------------------------
# -*- mode: cmake -*-
#------------------------------------------------------------------------
include(ax_packaging_deb)
include(ax_packaging_macports)
include(ax_packaging_rpm)

function(ax_packaging)
  set(options)
  set(oneValueArgs)
  set(multiValueArgs)
  cmake_parse_arguments(ARG "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

  if ( ${CPACK_SOURCE_GENERATOR} STREQUAL "TBZ2")
    set( CPACK_SOURCE_PACKAGE_FILE_EXTENSION
      ".tar.bz2")
    set( CPACK_SOURCE_PACKAGE_COMPRESSION_OPTION
      "-j")
  elseif ( ${CPACK_SOURCE_GENERATOR} STREQUAL "TGZ")
    set( CPACK_SOURCE_PACKAGE_FILE_EXTENSION
      ".tar.gz")
    set( CPACK_SOURCE_PACKAGE_COMPRESSION_OPTION
      "-z")
  elseif ( ${CPACK_SOURCE_GENERATOR} STREQUAL "TXZ")
    set( CPACK_SOURCE_PACKAGE_FILE_EXTENSION
      ".tar.xz")
    set( CPACK_SOURCE_PACKAGE_COMPRESSION_OPTION
      "-J")
  else ( ${CPACK_SOURCE_GENERATOR} STREQUAL "TBZ2")
    set( CPACK_SOURCE_EXTENSION ".tar")
    set( CPACK_SOURCE_PACKAGE_COMPRESSION_OPTION
      "")
  endif ( ${CPACK_SOURCE_GENERATOR} STREQUAL "TBZ2")
  if("${CMAKE_GENERATOR}" MATCHES "Make")
    set(STAGING_DIR "${CMAKE_BINARY_DIR}/staging")
    set(TAR_DIR_NAME "${PROJECT_NAME}-${PROJECT_VERSION}")
    set(TAR_NAME "${CPACK_SOURCE_PACKAGE_FILE_NAME}${CPACK_SOURCE_PACKAGE_FILE_EXTENSION}")
    if(NOT PROJECT_SPEC_FILENAME)
      set(PROJECT_SPEC_FILENAME ${CMAKE_BINARY_DIR}/config/${PROJECT_NAME}.spec)
    endif(NOT PROJECT_SPEC_FILENAME)

    # cx_msg_debug_variable( STAGING_DIR )
    # cx_msg_debug_variable( TAR_NAME )
    # cx_msg_debug_variable( PROJECT_SPEC_FILENAME )

    add_custom_target(dist_pre
                      COMMAND cpack -G ${CPACK_SOURCE_GENERATOR} --config CPackSourceConfig.cmake
                      COMMAND ${CMAKE_COMMAND} -E make_directory ${STAGING_DIR}
                      COMMAND tar -x ${CPACK_SOURCE_PACKAGE_COMPRESSION_OPTION} -C ${STAGING_DIR} -f ${CMAKE_BINARY_DIR}/${TAR_NAME} )
    add_custom_target( dist_body_begin
      DEPENDS dist_pre )
    add_custom_target( dist_body
      DEPENDS dist_body_begin )
    add_custom_target( dist_post
      COMMAND tar -c ${CPACK_SOURCE_PACKAGE_COMPRESSION_OPTION} -C ${STAGING_DIR} -f ${TAR_NAME} ${TAR_DIR_NAME}
      COMMAND ${CMAKE_COMMAND} -E remove_directory ${STAGING_DIR}
      WORKING_DIRECTORY ${CMAKE_CURRENT_DIR}
      DEPENDS dist_body
      )
    add_custom_target( dist
      DEPENDS dist_post )
  endif("${CMAKE_GENERATOR}" MATCHES "Make")

  # cx_msg_debug_variable( ARG_UNPARSED_ARGUMENTS )

  ax_packaging_deb(${ARG_UNPARSED_ARGUMENTS})
  ax_packaging_macports(${ARG_UNPARSED_ARGUMENTS})
  ax_packaging_rpm(${ARG_UNPARSED_ARGUMENTS})
endfunction(ax_packaging)
