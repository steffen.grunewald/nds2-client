function value = sampleRate( self ) % -*- mode: octave; -*-
% DATATYPE retrieve the sampleRate of the channel
%
%   Retrieves the sampleRate of the channel
%
%   INPUTS:
%     SELF   : ndsm.channel
%
%   OUTPUTS:
%     VALUE
  value = self.sampleRate( )
end
