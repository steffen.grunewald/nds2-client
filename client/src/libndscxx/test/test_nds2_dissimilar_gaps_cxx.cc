#include "nds.hh"
#include "nds_testing.hh"
#include "test_macros.hh"

#include <algorithm>
#include <cmath>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>

using NDS::connection;

int
main( int argc, char** argv )
{
    std::vector< std::string > args = convert_args( argc, argv );
    bool                       plot = find_opt( std::string( "-plot" ), args );

    connection::port_type port = 31200;
    if (::getenv( "NDS_TEST_PORT" ) )
    {
        std::istringstream ps(::getenv( "NDS_TEST_PORT" ) );
        ps >> port;
    }

    std::string hostname( "localhost" );
    if (::getenv( "NDS_TEST_HOST" ) )
        hostname = ::getenv( "NDS_TEST_HOST" );

    bool fast_fetch = false;
    if (::getenv( "NDS_FAST_FETCH" ) || ( find_opt( "-fast", args ) ) )
        fast_fetch = true;

    // This test only runs against NDS2
    connection::protocol_type proto = connection::PROTOCOL_TWO;

    std::cerr << "Connecting to " << hostname << ":" << port << " ndsv2"
              << std::endl;

    pointer< connection > conn(
        make_unique_ptr< connection >( hostname, port, proto ) );
    NDS_ASSERT( conn->parameters( ).port( ) == port );
    NDS_ASSERT( conn->parameters( ).protocol( ) == 2 );

    connection::channel_names_type cn;
    cn.push_back( "H1:ASC-INP2_Y_INMON.mean,m-trend" );
    cn.push_back( "L1:ASC-INP2_Y_INMON.mean,m-trend" );
    cn.push_back( "H1:ASC-INP2_Y_INMON.n,m-trend" );
    cn.push_back( "L1:ASC-INP2_Y_INMON.n,m-trend" );

    NDS::buffer::gps_second_type gps_start = 1112749800, gps_end = 1112762400;

    conn->parameters( ).set( "GAP_HANDLER", "STATIC_HANDLER_ZERO" );

    NDS::buffers_type bufs = conn->fetch( gps_start, gps_end, cn );

    NDS_ASSERT( bufs.size( ) == cn.size( ) );

    std::cerr << "Data has been retreived" << std::endl;

    conn->close( );

    std::cerr << "testing data type " << bufs[ 0 ].DataType( ) << std::endl;
    NDS_ASSERT( bufs[ 0 ].DataType( ) == NDS::channel::DATA_TYPE_FLOAT64 );
    std::cerr << "testing sample count " << bufs[ 0 ].Samples( ) << std::endl;
    NDS_ASSERT( bufs[ 0 ].Samples( ) == ( 1112762400 - 1112749800 ) / 60 );
    {
        std::cerr << "looking for a zero filled gap at [1112752800-1112756400) "
                     "on the first channel"
                  << std::endl;
        NDS::buffer::gps_second_type gap_start_offset_sec =
            1112752800 - gps_start;
        NDS::buffer::gps_second_type gap_end_offset_sec =
            1112756400 - gps_start;
        NDS::buffer::size_type gap_start_offset_byte = 0,
                               gap_end_offset_byte = 0;
        gap_start_offset_byte = bufs[ 0 ].samples_to_bytes(
            bufs[ 0 ].seconds_to_samples( gap_start_offset_sec ) );
        gap_end_offset_byte = bufs[ 0 ].samples_to_bytes(
            bufs[ 0 ].seconds_to_samples( gap_end_offset_sec ) );
        size_t tmp = 0;
        {
            const unsigned char* base =
                reinterpret_cast< const unsigned char* >(
                    bufs[ 0 ].cbegin< void >( ) );
            for ( NDS::buffer::size_type cur = gap_start_offset_byte;
                  cur < gap_end_offset_byte;
                  ++cur, ++tmp )
            {
                NDS_ASSERT( base[ cur ] == 0 );
            }
        }
        std::cerr << "Found " << tmp << " zero bytes" << std::endl;
        std::cerr << "Looking for 0.110272226 at time 1112752740 (sample prior "
                     "to gap)"
                  << std::endl;
        const double* pre_gap = reinterpret_cast< const double* >(
            reinterpret_cast< const unsigned char* >(
                bufs[ 0 ].cbegin< void >( ) ) +
            gap_start_offset_byte - bufs[ 0 ].DataTypeSize( ) );
        double delta = std::abs( *pre_gap - 0.110272226 );
        std::cerr << *pre_gap << " delta = " << delta << std::endl;
        NDS_ASSERT( delta <= 0.0000000001 );
    }

    if ( plot )
    {
        for ( NDS::buffers_type::const_iterator cur = bufs.begin( );
              cur != bufs.end( );
              ++cur )
        {
            const NDS::buffer& cur_buf = *cur;
            if ( cur_buf.Name( ).find( ".n,m-trend" ) != std::string::npos )
                continue;
            if ( cur_buf.DataType( ) != NDS::channel::DATA_TYPE_FLOAT64 )
                continue;
            std::string   fname( cur_buf.Name( ) + ".dat" );
            std::ofstream out( fname.c_str( ) );
            if ( !out )
                continue;
            out << "#time value" << std::endl;
            auto start = cur_buf.cbegin< double >( );
            for ( int i = 0; i < cur_buf.Samples( ); i++, start++ )
            {
                out << cur_buf.samples_to_seconds( i ) + cur_buf.Start( )
                    << "\t" << *start << std::endl;
            }
        }
    }

    return 0;
}
