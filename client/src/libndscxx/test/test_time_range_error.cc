#include <iostream>
#include <iomanip>

#include <nds.hh>
#include <nds_testing.hh>

#include "test_macros.hh"

using NDS::connection;

void
test_end_before_start( connection& conn )
{
}

int
main( int argc, char* argv[] )
{
    std::vector< std::string > args = convert_args( argc, argv );

    //---------------------------------------------------------------------
    // Obtain the port of the server
    //---------------------------------------------------------------------
    connection::port_type port = 31200;
    if ( std::getenv( "NDS_TEST_PORT" ) )
    {
        std::istringstream ps(::getenv( "NDS_TEST_PORT" ) );

        ps >> port;
    }
    std::string hostname( "localhost" );
    if ( std::getenv( "NDS_TEST_HOST" ) )
    {
        hostname = std::getenv( "NDS_TEST_HOST" );
    }

    connection::protocol_type proto = connection::PROTOCOL_TRY;
    if ( find_opt( "-proto-1", args ) )
    {
        proto = connection::PROTOCOL_ONE;
    }
    else if ( find_opt( "-proto-2", args ) )
    {
        proto = connection::PROTOCOL_TWO;
    }
    else if ( std::getenv( "NDS_TEST_PROTO" ) )
    {
        std::string ps( std::getenv( "NDS_TEST_PROTO" ) );
        std::cerr << "NDS_TEST_PROTO = " << ps << std::endl;
        if ( ps.compare( "1" ) == 0 )
            proto = connection::PROTOCOL_ONE;
        else if ( ps.compare( "2" ) == 0 )
            proto = connection::PROTOCOL_TWO;
    }

    std::cerr << "Connecting to " << hostname << ":" << port << " " << proto
              << std::endl;
    pointer< connection > conn(
        make_unique_ptr< connection >( hostname, port, proto ) );

    NDS::connection::channel_names_type chans;
    chans.push_back( "X1:PEM-1" );
    chans.push_back( "X1:PEM-2" );

    auto gps_start = static_cast< NDS::buffer::gps_second_type >( 1770000000 );
    auto gps_stop = static_cast< NDS::buffer::gps_second_type >( 1770000020 );
    auto gps_cur = gps_start;

    bool argument_error = false;
    try
    {
        auto stream = conn->iterate(
            NDS::request_period( gps_stop, gps_start, 10 ), chans );
        NDS_ASSERT( false );
    }
    catch ( std::invalid_argument& err )
    {
        argument_error = true;
    }
    NDS_ASSERT( argument_error );

    argument_error = false;
    try
    {
        auto stream = conn->fetch( gps_stop, gps_start, chans );
        NDS_ASSERT( false );
    }
    catch ( std::invalid_argument& err )
    {
        argument_error = true;
    }
    NDS_ASSERT( argument_error );

    argument_error = false;
    try
    {
        auto stream = conn->check( gps_stop, gps_start, chans );
        NDS_ASSERT( false );
    }
    catch ( std::invalid_argument& err )
    {
        argument_error = true;
    }
    NDS_ASSERT( argument_error );

    auto stream =
        conn->iterate( NDS::request_period( gps_start, gps_stop, 10 ), chans );
    for ( auto bufs : stream )
    {
        NDS_ASSERT( bufs->at( 0 ).Start( ) == gps_cur );
        gps_cur = bufs->at( 0 ).Stop( );
    }
    try
    {
        NDS::epoch test_epoch( "", gps_stop, gps_start );

        conn->find_channels( NDS::channel_predicate( test_epoch, "X1:PEM-1" ) );
        conn->find_channels( NDS::channel_predicate( test_epoch, "X1:PEM-2" ) );
    }
    catch ( ... )
    {
    }

    // make sure we can do other operations
    NDS::epoch test_epoch( "", gps_start, gps_stop );
    conn->find_channels( NDS::channel_predicate( test_epoch, "X1:PEM-1" ) );
    conn->find_channels( NDS::channel_predicate( test_epoch, "X1:PEM-2" ) );
    /* test_end_before_start( ); */
}
