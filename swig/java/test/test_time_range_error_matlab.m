% -*- mode: octave -*-
%------------------------------------------------------------------------
%------------------------------------------------------------------------
global unit_test;

unit_test = unittest( argv );

hostname = unit_test.hostname( );
port = unit_test.port( );
protocol = 'unknown';
use_gap_handler = 'true';

%------------------------------------------------------------------------
% Need to adjust according to parameter
%------------------------------------------------------------------------
if ( unit_test.hasOption( '-proto-1' ) )
  protocol = nds2.connection.PROTOCOL_ONE;
end
if ( unit_test.hasOption( '-proto-2' ) )
  protocol = nds2.connection.PROTOCOL_TWO;
end
if ( unit_test.hasOption( '-no-gap' ) )
  use_gap_handler = 'false';
end

%------------------------------------------------------------------------
% Establish the connection
%------------------------------------------------------------------------

conn = nds2.connection(hostname, port, protocol );
conn.setParameter( 'ITERATE_USE_GAP_HANDLERS', use_gap_handler );

%------------------------------------------------------------------------
% Run the test
%------------------------------------------------------------------------
START = 1770000000;
STOP = START + 20;
STRIDE = 10;
CHANNELS = { 'X1:PEM-1',
	     'X1:PEM-2'
	    };

state = false;
try
  iter = conn.iterate(STOP, START, STRIDE, CHANNELS);
  while( iter.hasNext( ) )
    bufs = iter.next( );
  end
catch e
  if( isa(e, 'matlab.exception.JavaException' ) )
    ex = e.ExceptionObject;
    fprintf( 'Debug: java exception class: %s\n', class( ex ) );
    assert(isjava(ex));
    if( isa( ex, 'java.lang.IllegalArgumentException' ) )
      state = true;
    end
  end
end
unit_test.check( true, state, 'Catch IllegalArgumentExecption in iterator' );

iter = conn.iterate(START, STOP, STRIDE, CHANNELS);
while( iter.hasNext( ) )
  bufs = iter.next( );
end

%------------------------------------------------------------------------
% Make sure we can do other operations
%------------------------------------------------------------------------

state = false;
try
  conn.setEpoch( STOP, START );
  conn.findChannels( 'X1:PEM-1' );
  conn.findChannels( 'X1:PEM-2' );
catch e
  if( isa(e, 'matlab.exception.JavaException' ) )
    ex = e.ExceptionObject;
    fprintf( 'Debug: java exception class: %s\n', class( ex ) );
    assert(isjava(ex));
    if( isa( ex, 'java.lang.IllegalArgumentException' ) )
      state = true;
    end
  end
end
unit_test.check( true, state, 'Catch IllegalArgumentExecption in setEpoch' );

conn.setEpoch( START, STOP );
conn.findChannels( 'X1:PEM-1' );
conn.findChannels( 'X1:PEM-2' );

%------------------------------------------------------------------------
% Finish
%------------------------------------------------------------------------
conn.close( );
unit_test.exit( );

