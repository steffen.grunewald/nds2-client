% -*- mode: octave -*-
%------------------------------------------------------------------------
%------------------------------------------------------------------------

nds2;
UnitTest;

global START;
global STOP;
global STRIDE;
global GPS_START;
global GPS_STOP;
global CHANNELS;

%------------------------------------------------------------------------
% Utility functions
%------------------------------------------------------------------------

function RETVAL = isgapsecond( GPS )
  RETVAL = false;
end

function RETVAL = checkdata( Buf, Value )

  samples_per_sec = Buf.sample_rate( );
  CHANNEL_TYPE = Buf.data_type( );

  unit_test_msginfo( "samples_per_sec: %d", ...
		     int32(samples_per_sec) );
  offset = 1;
  %======================================================================
  % MATLAB loops are inclusive so need to trim
  % the tail by 1 sec
  %======================================================================
  rs = Buf.start( );
  re = Buf.stop( ) - 1;
  for cur_gps = rs:re
    expected = Value;
    if ( isgapsecond( cur_gps ) )
      expected = 0.0;
    end
    d = Buf.data( );
    for i = 1:samples_per_sec
      if ( d(offset) ~= expected )
	RETVAL = false;
	break
      end
      offset = offset + 1;
    end
  end
  RETVAL = true;
end

try
  START = 0;
  STOP = START + 20;
  STRIDE = 10;
  GPS_START = 1770000000;
  GPS_STOP = GPS_START + 20;

  MULTIPLIER = 10;
  EXPECTED_I = 2;
  TIP_POINT = 0;      % when does the data do a switch
		      % this is just part of the test

  hostname = unit_test_hostname( );
  port = unit_test_port( );
  protocol = "unknown";
  use_gap_handler = "true";
  NO_GAPS=true;

  %----------------------------------------------------------------------
  % Need to adjust according to parameter
  %----------------------------------------------------------------------
  if ( unit_test_hasoption( "-proto-1" ) )
    protocol = nds2.connection.PROTOCOL_ONE;
    MULTIPLIER = 1;
    EXPECTED_I = 20;
    TIP_POINT = 10;
    CHANNELS = { "X1:PEM-1", ...
		"X1:PEM-2", ...
		};
  end
  if ( unit_test_hasoption( "-proto-2" ) )
    protocol = nds2.connection.PROTOCOL_TWO;
    MULTIPLIER = 10;
    EXPECTED_I = 2;
    TIP_POINT = 0;
    CHANNELS = { "X1:PEM-1,online", ...
		"X1:PEM-2,online", ...
		};
  end
  if ( unit_test_hasoption( "-no-gap" ) )
    use_gap_handler = "false";
    NO_GAPS = false;
  end

  %----------------------------------------------------------------------
  % Establish the connection
  %----------------------------------------------------------------------
	   
  conn = nds2.connection(hostname, port, protocol );
  conn.set_parameter( "ITERATE_USE_GAP_HANDLERS", use_gap_handler );
  conn.set_parameter( "GAP_HANDLER", "STATIC_HANDLER_ZERO" );

  %----------------------------------------------------------------------
  % Run the test
  %----------------------------------------------------------------------

  expected = { 1.5, 2.75 };

  i = 0;
  iter = conn.iterate(START, STOP, STRIDE, CHANNELS);
  try
    while( true )
      bufs = iter.next( );
      %------------------------------------------------------------------
      unit_test_check( bufs{1}.samples( ), ...
		       256 * MULTIPLIER, ...
		       "Verifying samples of buffer 1" );
      %unit_test_check( [ checkdata( bufs{1}, expected{1} ) ], ...
      %		       true, ...
      %		       "Verifying data of buffer 1" );
      %------------------------------------------------------------------
      unit_test_check( bufs{2}.samples( ), ...
		       512 * MULTIPLIER, ...
		       "Verifying samples of buffer 0" );
      unit_test_check( [ checkdata( bufs{2}, expected{2} ) ], ...
		       true, ...
		      "Verifying data of buffer 1" );
      %------------------------------------------------------------------
      i = i + 1;
      if (i == TIP_POINT)
	expected = { 3.5, 4.75 };
      end
    end
  catch err
    if ( ! strcmp( "No Next (SWIG_IndexError)", err.message ) )
      rethrow( lasterror );
    end
  end
  unit_test_check( i, ...
		   EXPECTED_I, ...
		   "Verifying number of iterations: " );

  %----------------------------------------------------------------------
  % Make sure we can do other operations
  %----------------------------------------------------------------------

  conn.set_epoch( GPS_START, GPS_STOP );
  conn.find_channels( "X1:PEM-1,online" );
  conn.find_channels( "X1:PEM-2,online" );

  expected = { 1.5, 2.75 };

  i = 0;
  iter = conn.iterate(0, 2, CHANNELS);
  try
    while( true )
      bufs = iter.next( );
      unit_test_check( bufs{1}.samples( ), ...
		       256, ...
		       "Verifying samples of buffer 1 after setepoch" );
      unit_test_check( true, ...
		       [ checkdata( bufs{1}, 1.5 ) ], ...
		       "Verifying data of buffer 1 after setepoch" );
      unit_test_check( bufs{2}.samples( ), ...
		       512, ...
		       "Verifying samples of buffer 2 after setepoch" );
      unit_test_check( true, ...
		       [ checkdata( bufs{2}, 2.75 ) ], ...
		       "Verifying data of buffer 2 after setepoch" );
      i = i + 1;
    end
  catch err
    if ( ! strcmp( "No Next (SWIG_IndexError)", err.message ) )
      rethrow( lasterror );
    end
  end
  unit_test_check( i, ...
		   2, ...
		   "Verifying second number of iterations: " );

  %----------------------------------------------------------------------
  % Finish
  %----------------------------------------------------------------------

  conn.close( );
catch err
  s = unit_test_dump_stack( err.stack );
  unit_test_msginfo( s );
  unit_test_check( false, ...
		   true, ...
		   sprintf( "Caught unexpected excption: %s\n%s", ...
			   err.message, ...
			   s ) );
end
unit_test_exit( );
 
