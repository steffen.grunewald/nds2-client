#
# Unit tests against mock-up NDS 1 server
#
# Copyright (C) 2014  Leo Singer <leo.singer@ligo.org>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#

from __future__ import print_function

import inspect

import os
try:
    a = nds2.connection.PROTOCOL_ONE
except:
    # It failed so there was no nds2 library loaded
    # Use the original C based interface
    import nds2
    print("Using nds2 library")
from binascii import unhexlify

raw_bytepatterns = [
    unhexlify('B000'),
    unhexlify('DEADBEEF'),
    unhexlify('00000000FEEDFACE'),
    unhexlify('FEEDFACE')]

min_bytepatterns = [
    unhexlify('0101'),
    unhexlify('02020202'),
    unhexlify('0303030303030303')]

max_bytepatterns = [
    unhexlify('1111'),
    unhexlify('12121212'),
    unhexlify('1313131313131313')]

mean_bytepatterns = [
    unhexlify('1A1A1A1A1A1A1A1A'),
    unhexlify('2A2A2A2A2A2A2A2A'),
    unhexlify('3A3A3A3A3A3A3A3A')]

rms_bytepatterns = [
    unhexlify('1B1B1B1B1B1B1B1B'),
    unhexlify('2B2B2B2B2B2B2B2B'),
    unhexlify('3B3B3B3B3B3B3B3B')]

n_bytepatterns = [
    unhexlify('1C1C1C1C'),
    unhexlify('2C2C2C2C'),
    unhexlify('3C3C3C3C')]

# a = nds2.channels_type( )

#print "nds2.connection:"
#print inspect.getmembers(nds2.connection)
#print "nds2.channel:"
#print inspect.getmembers(nds2.channel)
#print "nds2.channels_type: a:"
#print inspect.getmembers(a)
#
#print help( a )
#
# print "a.__len__: " + str( a.__len__( ) )
# print "length of a: " + str( len( a ) )

# exit( )


port = int(os.environ['NDS_TEST_PORT'])

conn = nds2.connection('localhost', port, nds2.connection.PROTOCOL_ONE)
assert conn.get_port() == port
assert conn.get_protocol() == 1

# test some enum -> string conversions
channel_type_tests = {
    nds2.channel.CHANNEL_TYPE_ONLINE : "online",
    nds2.channel.CHANNEL_TYPE_RAW : "raw",
    nds2.channel.CHANNEL_TYPE_RDS : "reduced",
    nds2.channel.CHANNEL_TYPE_STREND : "s-trend",
    nds2.channel.CHANNEL_TYPE_MTREND : "m-trend",
    nds2.channel.CHANNEL_TYPE_TEST_POINT : "test-pt",
    nds2.channel.CHANNEL_TYPE_STATIC : "static",
}
for channel_type in channel_type_tests:
    print(channel_type_tests[channel_type])
    assert nds2.channel_channel_type_to_string( channel_type ) == channel_type_tests[channel_type]
    assert nds2.channel.channel_type_to_string( channel_type ) == channel_type_tests[channel_type]

data_type_tests = {
    nds2.channel.DATA_TYPE_INT16 : "int_2",
    nds2.channel.DATA_TYPE_INT32 : "int_4",
    nds2.channel.DATA_TYPE_INT64 : "int_8",
    nds2.channel.DATA_TYPE_FLOAT32 : "real_4",
    nds2.channel.DATA_TYPE_FLOAT64 : "real_8",
    nds2.channel.DATA_TYPE_COMPLEX32 : "complex_8",
    nds2.channel.DATA_TYPE_UINT32 : "uint_4",
}
for data_type in data_type_tests:
    print(data_type_tests[data_type])
    print(nds2.channel.data_type_to_string( data_type ))
    assert nds2.channel_data_type_to_string( data_type ) == data_type_tests[data_type]
    assert nds2.channel.data_type_to_string( data_type ) == data_type_tests[data_type]

channels = conn.find_channels('*')
# print "type info for channels: " + type(channels).__name__
# print inspect.getmembers(channels)

# update this check when the channel count is
# changed on the mock server
base_channel_count = 9
assert len(channels) == base_channel_count * (5*2 + 1)

channels = conn.find_channels('X*:{A,B,C,G,H,I}')
assert len(channels) == 6
print('DEBUG: channels[0]: %s' % ( str(channels[0])) )
assert str(channels[0]) == '<X1:A (16Hz, ONLINE, INT16)>'
assert str(channels[1]) == '<X1:B (16Hz, ONLINE, INT32)>'
assert str(channels[2]) == '<X1:C (16Hz, ONLINE, INT64)>'
assert str(channels[3]) == '<X1:G (16Hz, ONLINE, UINT32)>'
assert str(channels[4]) == '<X1:H (65536Hz, ONLINE, FLOAT32)>'
assert str(channels[5]) == '<X1:I (16384Hz, ONLINE, UINT32)>'
print('DEBUG: finished asserts')

channels = conn.find_channels('X1:G*')
expected_names = set(('<X1:G (16Hz, ONLINE, UINT32)>', '<X1:G.max,m-trend (0.0166667Hz, MTREND, UINT32)>', 
    '<X1:G.max,s-trend (1Hz, STREND, UINT32)>', '<X1:G.mean,m-trend (0.0166667Hz, MTREND, FLOAT64)>', 
    '<X1:G.mean,s-trend (1Hz, STREND, FLOAT64)>', '<X1:G.min,m-trend (0.0166667Hz, MTREND, UINT32)>', 
    '<X1:G.min,s-trend (1Hz, STREND, UINT32)>', '<X1:G.rms,m-trend (0.0166667Hz, MTREND, FLOAT64)>', 
    '<X1:G.rms,s-trend (1Hz, STREND, FLOAT64)>', '<X1:G.n,m-trend (0.0166667Hz, MTREND, INT32)>',
    '<X1:G.n,s-trend (1Hz, STREND, INT32)>'))
actual_names = set([str(channel) for channel in channels])
assert expected_names == actual_names
print('DEBUG: finshed find_channels("X1:G*)')

#channels = conn.find_channels('*')
#raise AssertionError(str(channels))

#import sys
#sys.stdin.readline()
print('DEBUG: Issuing fetch')
bufs = conn.fetch(1000000000, 1000000004, ['X1:A', 'X1:B', 'X1:C', 'X1:G'])
print("bufs.__len__: " + str( bufs.__len__( ) ))
print(bufs)
assert len(bufs) == 4
for buf, bytepattern in zip(bufs, raw_bytepatterns):
    print("len: " + str( len(buf.data) ) + " expected: " + str( 4 * buf.channel.sample_rate ))
    assert buf.gps_seconds == 1000000000
    assert buf.gps_nanoseconds == 0
    assert len(buf.data) == 4 * buf.channel.sample_rate
    assert bytes(buf.data.data) == bytepattern * len(buf.data)

for trend_type in ['s-trend', 'm-trend']:
    if trend_type == 's-trend':
        start = 1000000000
        end = 1000000020
        mult = 1
        samples = 20
    else:
        start = 1000000020
        end = 1000000140
        mult = 60
        samples = 2
    
    channel_list = ['X1:A.min,%s' % trend_type, 'X1:B.min,%s' % trend_type, 'X1:C.min,%s' % trend_type]
    channel_types = [ conn.find_channels(ch)[0].channel_type for ch in channel_list ]
    bufs = conn.fetch(start, end, channel_list)
    assert len(bufs) == 3
    for buf, bytepattern, expected_type in zip(bufs, min_bytepatterns, channel_types):
        assert expected_type == buf.channel.channel_type
        try:
            assert expected_type == buf.channel_type
        except AttributeError:
            # Not available in nds2 library, but available in nds library
            pass
        assert buf.gps_seconds == start
        assert buf.gps_nanoseconds == 0
        ##assert len(buf.data) == samples * (buf.channel.sample_rate * mult)
        ##assert bytes(buf.data.data) == bytepattern * len(buf.data)

    channel_list = ['X1:A.max,%s' % trend_type, 'X1:B.max,%s' % trend_type, 'X1:C.max,%s' % trend_type]
    channel_types = [ conn.find_channels(ch)[0].channel_type for ch in channel_list ]
    bufs = conn.fetch(start, end, channel_list)
    assert len(bufs) == 3
    for buf, bytepattern, expected_type  in zip(bufs, max_bytepatterns, channel_types):
        assert expected_type == buf.channel.channel_type
        try:
            assert expected_type == buf.channel_type
        except AttributeError:
            # Not available in nds2 library, but available in nds library
            pass
        assert buf.gps_seconds == start
        assert buf.gps_nanoseconds == 0
        ##assert len(buf.data) == samples * (buf.channel.sample_rate * mult)
        ##assert bytes(buf.data.data) == bytepattern * len(buf.data)

    channel_list = ['X1:A.rms,%s' % trend_type, 'X1:B.rms,%s' % trend_type, 'X1:C.rms,%s' % trend_type]
    channel_types = [ conn.find_channels(ch)[0].data_type for ch in channel_list ]
    bufs = conn.fetch(start, end, channel_list)
    assert len(bufs) == 3
    for buf, bytepattern, expected_type in zip(bufs, rms_bytepatterns, channel_types):
        assert expected_type == buf.channel.data_type
        assert buf.channel.data_type == buf.channel.DATA_TYPE_FLOAT64
        assert buf.gps_seconds == start
        assert buf.gps_nanoseconds == 0
        try:
            assert expected_type == buf.data_type
            assert buf.data_type == buf.DATA_TYPE_FLOAT64
        except AttributeError:
            # Not available in nds2 library, but available in nds library
            pass
        ##assert len(buf.data) == samples * (buf.channel.sample_rate * mult)
        ##assert bytes(buf.data.data) == bytepattern * len(buf.data)

    channel_list = ['X1:A.mean,%s' % trend_type, 'X1:B.mean,%s' % trend_type, 'X1:C.mean,%s' % trend_type]
    channel_types = [ conn.find_channels(ch)[0].data_type for ch in channel_list ]
    bufs = conn.fetch(start, end, channel_list)
    assert len(bufs) == 3
    for buf, bytepattern, expected_type in zip(bufs, mean_bytepatterns, channel_types):
        assert expected_type == buf.channel.data_type
        assert buf.channel.data_type == buf.channel.DATA_TYPE_FLOAT64
        try:
            assert expected_type == buf.data_type
            assert buf.data_type == buf.DATA_TYPE_FLOAT64
        except AttributeError:
            # Not available in nds2 library, but available in nds library
            pass
        assert buf.gps_seconds == start
        assert buf.gps_nanoseconds == 0
        ##assert len(buf.data) == samples * (buf.channel.sample_rate * mult)
        ##assert bytes(buf.data.data) == bytepattern * len(buf.data)

    channel_list = ['X1:A.n,%s' % trend_type, 'X1:B.n,%s' % trend_type, 'X1:C.n,%s' % trend_type]
    channel_types = [ conn.find_channels(ch)[0].data_type for ch in channel_list ]
    bufs = conn.fetch(start, end, channel_list)
    assert len(bufs) == 3
    for buf, bytepattern, expected_type in zip(bufs, n_bytepatterns, channel_types):
        assert expected_type == buf.channel.data_type
        assert buf.channel.data_type == buf.channel.DATA_TYPE_INT32
        try:
            assert expected_type == buf.data_type
            assert buf.data_type == buf.DATA_TYPE_INT32
        except AttributeError:
            # Not available in nds2 library, but available in nds library
            pass
        assert buf.gps_seconds == start
        assert buf.gps_nanoseconds == 0
        ##assert len(buf.data) == samples * (buf.channel.sample_rate * mult)
        ##assert bytes(buf.data.data) == bytepattern * len(buf.data)


    print("Offline iterate")
    print(dir(conn))
    for bufs in conn.iterate(start, end, ['X1:A.mean,%s' % trend_type, 'X1:B.mean,%s' % trend_type, 'X1:C.mean,%s' % trend_type]):
        break

    assert len(bufs) == 3
    for buf, bytepattern in zip(bufs, mean_bytepatterns):
        assert buf.gps_seconds == start
        assert buf.gps_nanoseconds == 0
        # a limit of the current mock nds server, trends only send one sample
        ##assert len(buf.data) == samples * (buf.channel.sample_rate * mult)
        ##assert bytes(buf.data.data) == bytepattern * len(buf.data)


for bufs in conn.iterate(['X1:A', 'X1:B', 'X1:C']):
    break
assert len(bufs) == 3
for buf, bytepattern in zip(bufs, raw_bytepatterns):
    assert buf.gps_seconds == 1000000000
    assert buf.gps_nanoseconds == 0
    ##assert len(buf.data) == buf.channel.sample_rate
    ##assert bytes(buf.data.data) == bytepattern * len(buf.data)

# connection automatically closed when garbage collected,
# but test explicit close anyway
conn.close()
