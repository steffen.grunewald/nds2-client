from __future__ import print_function

import nds2
import os
import sys

def let_original_data_go_out_of_scope(conn):
    buf = conn.fetch(1163880064, 1163880064+30,
["H1:DAQ-BCST0_FAST_DATA_CRC",])
    print(buf[0].data)
    print("checking assertions on original data")
    assert (buf[0].data[0] == buf[0].data[1])
    assert (buf[0].data[1] == buf[0].data[2])
    return buf[0].data

host = os.getenv('NDS_TEST_HOST', 'localhost')
port = int(os.getenv('NDS_TEST_PORT', 31200))

conn = nds2.connection(host, port)
data = let_original_data_go_out_of_scope(conn)

print("\n\n")
print(data)
print("checking assertions on returned data")
assert( data[0] == data[1] )
assert( data[1] == data[2] )

conn.close( )
