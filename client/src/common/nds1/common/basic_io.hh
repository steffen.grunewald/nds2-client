//
// Created by jonathan.hanks on 5/4/18.
//

#ifndef NDS2_CLIENT_BASIC_IO_HH
#define NDS2_CLIENT_BASIC_IO_HH

#include <cstdint>

#include "common/status_codes.hh"

namespace nds_impl
{
    namespace nds1
    {
        namespace common
        {

            /**
             * A Basic_IO object provides common IO primitives
             * for a NDS1 connection.
             * @tparam BufferedReader The reader interface it must be
             * compatibile with nds_impl::Socket::BufferedReader
             */
            template < typename BufferedReader >
            class Basic_IO
            {
            public:
                Basic_IO( BufferedReader& r ) : r_( r )
                {
                }

                /**
                 * @return A daq status code
                 */
                nds_impl::common::status_code
                read_status_code( )
                {
                    nds_impl::common::status_code result;
                    r_.read_exactly( result.data( ),
                                     result.data( ) + result.size( ) );
                    return result;
                }

                /**
                 * Read a 32bit unsigned integer encoded as a 8 byte ASCII hex
                 * string.
                 * @return the 32bit unsigned int.
                 */
                std::uint32_t
                read_uint32_hex( )
                {
                    uint32_t val = 0;
                    std::array< char, 8 > buf;
                    r_.read_exactly( buf.data( ), buf.data( ) + buf.size( ) );
                    for ( const auto byte : buf )
                    {
                        val = ( val << 4 ) | from_hex_nibble( byte );
                    }
                    return val;
                }

                /**
                 * Read a 32bit unsigned integer encoded as a 8 byte ASCII hex
                 * string.
                 * @return the 32bit unsigned int.
                 */
                float
                read_float32_hex( )
                {
                    uint32_t tmp = read_uint32_hex( );
                    return *reinterpret_cast< float* >( &tmp );
                }

            private:
                BufferedReader& r_;

                int
                from_hex_nibble( int nibble )
                {
                    if ( nibble >= '0' && nibble <= '9' )
                    {
                        return nibble - '0';
                    }
                    if ( nibble >= 'a' && nibble <= 'f' )
                    {
                        return ( nibble - 'a' ) + 10;
                    }
                    if ( nibble >= 'A' && nibble <= 'F' )
                    {
                        return ( nibble - 'A' ) + 10;
                    }
                    throw std::invalid_argument(
                        "Unknown input found while converting a hex value" );
                }
            };
        }
    }
}

//////////////////////////////////////////
// Tests only after this point.
/////////

#ifdef _NDS_IMPL_ENABLE_CATCH_TESTS_

#include "common/status_codes.hh"
#include "socket/buffered_reader.hh"
#include "tests/dummy_socket.hh"
#include "catch.hpp"

TEST_CASE( "You should be able to create a basic_io object",
           "[nds2,basic_io,create]" )
{
    using namespace nds_testing;

    typedef nds_impl::Socket::BufferedReader< DummySocket > sock_type;

    DummySocket                                   s( "" );
    sock_type                                     buf_s( s );
    nds_impl::nds1::common::Basic_IO< sock_type > b( buf_s );
}

TEST_CASE( "You should be able to read a nds1 status code", "[nds1]" )
{
    using namespace nds_testing;
    using namespace nds_impl::common;

    typedef nds_impl::Socket::BufferedReader< DummySocket > sock_type;

    DummySocket                                   s1( "0000000d" );
    sock_type                                     buf_s1( s1 );
    nds_impl::nds1::common::Basic_IO< sock_type > b( buf_s1 );

    REQUIRE( b.read_status_code( ) == STATUS_DAQD_OK );
    REQUIRE( b.read_status_code( ) == STATUS_DAQD_NOT_FOUND );
}

TEST_CASE( "You should be able to read a 32bit ascii/hex value",
           "[nds1,basic_io,hex]" )
{
    using namespace nds_testing;
    typedef nds_impl::Socket::BufferedReader< DummySocket > sock_type;

    DummySocket s1( "0000034000000000f7F8f9aB" );
    sock_type   buf_s1( s1 );
    nds_impl::nds1::common::Basic_IO< sock_type > b( buf_s1 );

    REQUIRE( b.read_uint32_hex( ) == 0x340 );
    REQUIRE( b.read_uint32_hex( ) == 0x0 );
    REQUIRE( b.read_uint32_hex( ) == 0xf7f8f9ab );
}

TEST_CASE( "Exceptions are thrown when reading a bad 32bit ascii/hex value",
           "[nds1,basic_io,hex]" )
{
    using namespace nds_testing;
    typedef nds_impl::Socket::BufferedReader< DummySocket > sock_type;

    // two tests, one with an invalid char '&'
    // another with a short input
    DummySocket                                   s1( "0000&3400000" );
    sock_type                                     buf_s1( s1 );
    nds_impl::nds1::common::Basic_IO< sock_type > b( buf_s1 );

    REQUIRE_THROWS_AS( b.read_uint32_hex( ), std::invalid_argument );
    REQUIRE_THROWS( b.read_uint32_hex( ) );
}

TEST_CASE( "You should be able to read a 32bit float ascii/hex value",
           "[nds1,basic_io,hex]" )
{
    using namespace nds_testing;
    typedef nds_impl::Socket::BufferedReader< DummySocket > sock_type;

    DummySocket                                   s1( "3f800000" );
    sock_type                                     buf_s1( s1 );
    nds_impl::nds1::common::Basic_IO< sock_type > b( buf_s1 );

    REQUIRE( b.read_float32_hex( ) == 1.0f );
}

TEST_CASE(
    "Exceptions are thrown when reading a bad 32bit float ascii/hex value",
    "[nds1,basic_io,hex]" )
{
    using namespace nds_testing;
    typedef nds_impl::Socket::BufferedReader< DummySocket > sock_type;

    // two tests, one with an invalid char '&'
    // another with a short input
    DummySocket                                   s1( "3f8&0000000" );
    sock_type                                     buf_s1( s1 );
    nds_impl::nds1::common::Basic_IO< sock_type > b( buf_s1 );

    REQUIRE_THROWS_AS( b.read_float32_hex( ), std::invalid_argument );
    REQUIRE_THROWS( b.read_float32_hex( ) );
}

#endif // _NDS_IMPL_ENABLE_CATCH_TESTS_

#endif // NDS2_CLIENT_BASIC_IO_HH
