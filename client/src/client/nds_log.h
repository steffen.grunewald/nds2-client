/* -*- tab-width:8 c-basic-offset:4  indent-tabs-mode:nil -*- */
/* set vi: ts=8:softtabstop=4,shiftwidth=4,expandtab */

#ifndef NDS_LOG_H
#define NDS_LOG_H

#ifndef DLL_EXPORT
#if defined _WIN32 || defined _WIN64
#define DLL_EXPORT __declspec( dllexport )
#else
#define DLL_EXPORT
#endif /* WIN32 || WIN64 */
#endif /* DLL_EXPORT */

DLL_EXPORT void nds_flush( );

DLL_EXPORT void nds_log( const char* Message );

#endif /* NDS_LOG_H */
