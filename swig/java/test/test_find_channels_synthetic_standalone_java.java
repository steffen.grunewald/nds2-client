/* ------------------------------------------------------------------- *
 * ------------------------------------------------------------------- */

import nds2.connection;
import nds2.channel;
import nds2.channel_predicate;
import nds2.nds2;
import nds2.parameters;

public final class test_find_channels_synthetic_standalone_java {

    public static void main( String[] args )
	throws java.io.UnsupportedEncodingException
    {
	unittest unit_test = new unittest( args );

	String hostname = unit_test.hostname( );
	int port = unit_test.port( );

	/* ------------------------------------------------------------- *
	   Establish the connection
	 * ------------------------------------------------------------- */

	parameters params = new parameters( hostname, port );

	boolean caught_error = false;

	/* ------------------------------------------------------------- *
	   Run test
	 * ------------------------------------------------------------- */

	channel_predicate pred = new channel_predicate();
	pred.setGlob("*");
	pred.setTimespan(1200000000, 1210000000);

	channel[] lst = nds2.findChannels(params, pred);
	unit_test.check( lst.length,
			 5,
			 "Validating number of channels for pattern '*'" );

	String[] chans = new String[]{ "<X1:PEM-1 (256Hz, RAW, FLOAT32)>",
				       "<X1:PEM-1 (1Hz, STREND, FLOAT32)>",
				       "<X1:PEM-2 (256Hz, RAW, FLOAT32)>",
				       "<X1:PEM-2 (1Hz, STREND, FLOAT32)>",
				       "<X1:PEM-3 (256Hz, RAW, FLOAT32)>"
	};

	for(  int i = 0; i < chans.length; ++i )
	{
	    unit_test.check( chans[i],
			     lst[ i ].toString( ),
			     "Validating all channel names for pattern '*'" );
	}

	unit_test.exit( );
    }
}