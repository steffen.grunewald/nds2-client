#include <iostream>
#include <sstream>
#include <stdexcept>
#include <string>
#include <vector>

#include <stdlib.h>

#include "daqc.h"

class cleanup_daq
{
public:
    cleanup_daq( daq_t& daq ) : daq_( daq )
    {
    }
    ~cleanup_daq( )
    {
        daq_disconnect( &daq_ );
    }

private:
    daq_t& daq_;
};

void
check( const char* msg, int val )
{
    if ( val )
    {
        std::ostringstream os;
        os << "Error found in " << msg << ": " << val;
        throw std::runtime_error( os.str( ) );
    }
}

int
main( int argc, char* argv[] )
{
    std::string hostname( "localhost" );
    if (::getenv( "NDS_TEST_HOST" ) )
    {
        hostname = ::getenv( "NDS_TEST_HOST" );
    }
    int port = 31200;
    if (::getenv( "NDS_TEST_PORT" ) )
    {
        std::istringstream ps(::getenv( "NDS_TEST_PORT" ) );
        ps >> port;
    }
    daq_t daq;

    check( "startup", daq_startup( ) );

    check( "connect", daq_connect( &daq, hostname.c_str( ), port, nds_try ) );
    cleanup_daq d_( daq );

    daq_channel_t chan;
    daq_init_channel( &chan, "H1:CHANNEL_1,raw", cUnknown, 0.0, _undefined );
    daq_request_channel_from_chanlist( &daq, &chan );

    check( "epoch", daq_set_epoch( &daq, "1266253322:60000" ) );

    std::vector< char > buffer( 262144, ' ' );
    long                used = 0;
    check( "source data",
           daq_recv_source_data(
               &daq, buffer.data( ), buffer.size( ), 0, &used ) );
    {
        std::string out( buffer.data( ), buffer.data( ) + used );
        std::cout << out << "\n";
        std::cout << used << std::endl;
    }

    used = 0;
    check( "source list",
           daq_recv_source_list(
               &daq, buffer.data( ), buffer.size( ), 0, &used ) );

    {
        std::string out( buffer.data( ), buffer.data( ) + used );
        std::cout << out << "\n";
        std::cout << used << std::endl;
    }
    return 0;
}
