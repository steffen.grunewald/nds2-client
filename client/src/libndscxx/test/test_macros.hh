#ifndef TEST_MACROS_HH
#define TEST_MACROS_HH

#include <algorithm>
#include <vector>
#include <fstream>

#include "environmental_support.hh"

std::ostream&
null_stream( )
{
    static std::ofstream nstream( "/dev/null" );

    return nstream;
}

inline std::vector< std::string >
convert_args( int argc, char** argv )
{
    std::vector< std::string > retval( argc );

    for ( int i = 1; i < argc; i++ )
    {
        retval.push_back( argv[ i ] );
    }
    return retval;
}

inline bool
find_opt( const std::string& Opt, const std::vector< std::string >& Args )
{
    return ( std::find( Args.begin( ), Args.end( ), Opt ) != Args.end( ) );
}

inline bool
find_opt( const char* Opt, const std::vector< std::string >& Args )
{
    return ( find_opt( std::string( Opt ), Args ) );
}

inline std::ostream&
debug_message( const std::string& Header )
{
#if 0
  std::ostream& retstream = std::cerr;
#else
    std::ostream& retstream = null_stream( );
#endif

    retstream << "DEBUG: CLIENT: " << Header << ":";
    return retstream;
}

#endif /* TEST_MACROS_HH */
