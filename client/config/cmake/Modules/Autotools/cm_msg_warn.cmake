#------------------------------------------------------------------------
# -*- mode: cmake -*-
#------------------------------------------------------------------------
# cm_msg_warn( )
#------------------------------------------------------------------------
include( CMakeParseArguments )

include ( Autotools/cm_msg_notice )

function( cm_msg_warn txt )
  set( prefix "WARNING" )

  set(options
    )
  set(oneValueArgs
    )
  set(multiValueArgs
    )

  cmake_parse_arguments( ARG "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

  cm_msg_notice( "${prefix}: ${txt}" )

endfunction( )
