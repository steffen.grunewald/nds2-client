#include "nds.hh"

#include <memory>
#include <cstdlib>
#include <iostream>
#include <sstream>
#include <string>

#include "test_macros.hh"
#include "nds_testing.hh"

int
main( int argc, char* argv[] )
{
    using NDS::connection;

    std::vector< std::string > args = convert_args( argc, argv );

    //---------------------------------------------------------------------
    // Obtain the port of the server
    //---------------------------------------------------------------------
    connection::port_type port = 31200;
    if (::getenv( "NDS_TEST_PORT" ) )
    {
        std::istringstream ps(::getenv( "NDS_TEST_PORT" ) );

        ps >> port;
    }

    std::string hostname( "localhost" );
    if (::getenv( "NDS_TEST_HOST" ) )
    {
        hostname = ::getenv( "NDS_TEST_HOST" );
    }

    connection::protocol_type proto = connection::PROTOCOL_ONE;

    pointer< connection > conn =
        make_unique_ptr< connection >( hostname, port, proto );

    NDS::buffer::gps_second_type start = 1108835634;
    NDS::buffer::gps_second_type end = start + 4;

    NDS::channels_type lst =
        conn->find_channels( NDS::channel_predicate( "H1:SUS-BS*" ) );

    NDS::connection::channel_names_type channels{
        "H1:SUS-BS_BIO_ENCODE_DIO_0_OUT",
        "H1:SUS-BS_BIO_M1_MSDELAYON",
        "H1:SUS-BS_COMMISH_STATUS",
        "H1:SUS-BS_DACKILL_BYPASS_TIMEMON",
        "H1:SUS-BS_DCU_ID",
        "H1:SUS-BS_DITHERP2EUL_1_1",
        "H1:SUS-BS_DITHERP2EUL_2_1",
        "H1:SUS-BS_DITHERY2EUL_1_1",
        "H1:SUS-BS_DITHERY2EUL_2_1",
        "H1:SUS-BS_DITHER_P_IPCERR"
    };

    auto buffers = conn->fetch( start, end, channels );

    for ( auto& buf : buffers )
    {
        NDS::channel& ch1 = buf;
        NDS::channel  ch2 =
            conn->find_channels( NDS::channel_predicate( ch1.Name( ) ) )[ 0 ];

        std::cout << "ch1 = " << ch1 << " ch2 = " << ch2 << std::endl;

        NDS_ASSERT( ch1.Name( ) == ch2.Name( ) );
        NDS_ASSERT( ch1.DataType( ) == ch2.DataType( ) );
        NDS_ASSERT( ch1.Type( ) == ch2.Type( ) );
        NDS_ASSERT( ch1.SampleRate( ) == ch2.SampleRate( ) );
        NDS_ASSERT( ch1.Gain( ) == ch2.Gain( ) );
        NDS_ASSERT( ch1.Slope( ) == ch2.Slope( ) );
        NDS_ASSERT( ch1.Units( ) == ch2.Units( ) );
    }
    conn->close( );
    return 0;
}
