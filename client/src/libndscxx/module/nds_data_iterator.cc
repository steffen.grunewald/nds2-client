//
// Created by jonathan.hanks on 5/25/18.
//

#include "nds_data_iterator.hh"
#include "nds_iterate_handler.hh"

namespace NDS
{
    inline namespace abi_0
    {

        data_stream_iterator::data_stream_iterator( )
            : p_( nullptr ), cache_( nullptr )
        {
        }

        data_stream_iterator::data_stream_iterator(
            const data_stream_iterator& other ) = default;

        data_stream_iterator::data_stream_iterator(
            data_stream_iterator&& other ) noexcept = default;

        data_stream_iterator::data_stream_iterator(
            std::shared_ptr< detail::iterate_handler > p, value_type c )
            : p_( std::move( p ) ), cache_( std::move( c ) )
        {
        }

        data_stream_iterator::~data_stream_iterator( ) = default;

        data_stream_iterator& data_stream_iterator::
        operator=( const data_stream_iterator& other ) = default;

        data_stream_iterator& data_stream_iterator::
        operator=( data_stream_iterator&& other ) noexcept = default;

        bool
        data_stream_iterator::
        operator==( const NDS::data_stream_iterator& other ) const
        {
            return ( ( p_ == other.p_ ) && ( cache_ == other.cache_ ) );
        }

        bool
        data_stream_iterator::
        operator!=( const NDS::data_stream_iterator& other ) const
        {
            return !operator==( other );
        }

        data_stream_iterator::reference data_stream_iterator::operator*( )
        {
            return cache_;
        }

        data_stream_iterator& data_stream_iterator::operator++( )
        {
            p_->next( );
            cache_ = p_->cache_;
            return *this;
        }

        data_stream_iterator data_stream_iterator::operator++( int )
        {
            data_stream_iterator tmp{ *this };
            ++( *this );
            return tmp;
        }

        data_iterable::data_iterable(
            std::shared_ptr< NDS::detail::iterate_handler > handler )
            : p_{ std::move( handler ) }
        {
        }

        data_iterable::data_iterable( const data_iterable& other ) = default;

        data_iterable::data_iterable( data_iterable&& other ) noexcept =
            default;

        data_iterable::~data_iterable( ) = default;

        data_iterable& data_iterable::
        operator=( const data_iterable& other ) = default;

        data_iterable& data_iterable::
        operator=( data_iterable&& other ) noexcept = default;

        data_iterable::iterator_type
        data_iterable::begin( )
        {
            return data_stream_iterator( p_, p_->cache_ );
        }

        data_iterable::iterator_type
        data_iterable::end( )
        {
            return data_stream_iterator( p_, nullptr );
        }

        void
        data_iterable::abort( )
        {
            p_->abort( );
        }
    }
}
