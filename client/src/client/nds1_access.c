/* -*- tab-width:8 c-basic-offset:4  indent-tabs-mode:nil -*- */
/* set vi: ts=8:softtabstop=4,shiftwidth=4,expandtab */

#include "daq_bsd_string.h"

#if HAVE_CONFIG_H
#include "daq_config.h"
#endif /* HAVE_CONFIG_H */

#if HAVE_WIN_H
#include <win.h>
#endif /* HAVE_WIN_H */
#if HAVE_STDARG_H
#include <stdarg.h>
#endif

#include <ctype.h>
#include <errno.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#if HAVE_UNISTD_H
#include <unistd.h>
#else
#include <io.h>
#endif /* HAVE_UNISTD_H */

/*--------------------------------------------------------------------
 *
 *    Select includes depend on posix versions
 *--------------------------------------------------------------------*/
#if _POSIX_C_SOURCE < 200100L
#if HAVE_SYIME_H
#include <sys/time.h>
#endif /* HAVE_SYS_TIME_H */
#include <sys/types.h>
#else
#include <sys/select.h>
#endif

#include "daqc.h"
#include "daqc_private.h"
#include "daqc_response.h"
#include "daqc_internal.h"
#include "daqc_net.h"
#include "nds1.h"
#include "nds_logging.h"
#include "nds_os.h"

#if _WIN32
#define NFDS( x ) 0
#else
#define NFDS( x ) ( x + 1 )
#endif

/*
 * Pulled from the swig client
 */
static int
nds_str_endswith( const char* str, const char* end )
{
    const size_t str_len = strlen( str );
    const size_t end_len = strlen( end );

    return end_len <= str_len &&
        memcmp( str + ( str_len - end_len ), end, end_len ) == 0;
}

static ssize_t
nds_str_lastindex( const char* str, int ch )
{
    const char* cur = str;
    char        _ch = (char)ch;
    ssize_t     index = 0;
    if ( !str || ch == '\0' )
    {
        return -1;
    }
    while ( *cur )
    {
        cur++;
        index++;
    }
    while ( index >= 0 && *cur != _ch )
    {
        cur--;
        index--;
    }
    return index;
}

static int
_nds1_retrieve_version( daq_t* daq, int* v, int* rv )
{
    int resp = 0;
    int exp_rv = 0;

    /* Do protocol version check */
    if ( nds_logging_check( NDS_LOG_GROUP_STATUS_UPDATE, 0 ) )
    {
        nds_logging_printf( " GET NDS version\n" );
    }
    if ( ( resp = daq_send( daq, "version;" ) ) )
    {
        daq_private_srvr_close( daq->conceal );
        return resp;
    }

    /* Read server response */
    *v = (int)read_server_response( daq->conceal->sockfd );
    if ( nds_logging_check( NDS_LOG_GROUP_STATUS_UPDATE, 0 ) )
    {
        nds_logging_printf( "NDS version of server is %d\n", v );
    }
    if ( ( *v > MAX_NDS1_PROTOCOL_VERSION ) ||
         ( *v < MIN_NDS1_PROTOCOL_VERSION ) )
    {
        if ( nds_logging_check( NDS_LOG_GROUP_STATUS_UPDATE, 0 ) )
        {
            nds_logging_printf(
                "unsupported communication protocol version: " );
            nds_logging_printf( "received %d, only support %d to %d\n",
                                *v,
                                MIN_NDS1_PROTOCOL_VERSION,
                                MAX_NDS1_PROTOCOL_VERSION );
            nds_logging_printf( " version %d not between %d to %d\n",
                                *v,
                                MIN_NDS1_PROTOCOL_VERSION,
                                MAX_NDS1_PROTOCOL_VERSION );
        }
        daq_private_srvr_close( daq->conceal );
        return DAQD_VERSION_MISMATCH;
    }

    /* Set things based on protocol version of server */
    if ( *v >= 12 )
    {
        exp_rv = 0;
    }
    else
    {
        exp_rv = 4;
    }
    /* Do protocol revision check */
    resp = daq_send( daq, "revision;" );
    if ( resp )
    {
        daq_private_srvr_close( daq->conceal );
        return resp;
    }

    /* Read server response */
    *rv = (int)read_server_response( daq->conceal->sockfd );
    if ( *rv != exp_rv )
    {
        if ( nds_logging_check( NDS_LOG_GROUP_STATUS_UPDATE, 0 ) )
        {
            nds_logging_printf(
                "Warning: communication protocol revision mismatch: " );
            nds_logging_printf(
                "expected %d.%d, received %d.%d\n", *v, exp_rv, *v, *rv );
        }
    }
    return 0;
}

int
nds1_reconnect( daq_t* daq )
{
    socket_addr_t srv_addr_tmp;
    size_t        max_nds1_commands = 0;
    int           resp = 0;
    int           connect_rc = 0;
    int           v = 0, rv = 0;

    memcpy( &srv_addr_tmp, daq->conceal->srvr_addr, sizeof( srv_addr_tmp ) );
    max_nds1_commands = daq->conceal->max_command_count;
    daq_private_delete( &( daq->conceal ) );
    daq_private_create( &( daq->conceal ) );

    /*  Get a socket.
     */
    resp = daq_private_srvr_open( daq->conceal );
    if ( resp != DAQD_OK )
    {
        return resp;
    }

    memcpy( daq->conceal->srvr_addr, &srv_addr_tmp, sizeof( srv_addr_tmp ) );
    daq->conceal->max_command_count = max_nds1_commands;

    /*-----------------------------------  Connect to the server      */
    daq_private_srvr_nonblocking( daq->conceal, 1 );
    if ( nds_logging_check( NDS_LOG_GROUP_STATUS_UPDATE, 0 ) )
    {
        nds_logging_printf( "Connecting NDS1 .." );
        nds_logging_flush( );
    }
    connect_rc = daq_private_srvr_connect( daq->conceal );
    if ( connect_rc == NDS_SOCKET_TRANSIENT_FAILURE )
    {
        struct timeval timeout = { 4, 0 };
        fd_set         writefds;
        int            ret;
        FD_ZERO( &writefds );
        FD_SET( daq->conceal->sockfd, &writefds );
        ret = select(
            NFDS( daq->conceal->sockfd ), NULL, &writefds, NULL, &timeout );
        if ( ret == 1 )
            connect_rc = NDS_SOCKET_OK;
    }
    if ( connect_rc != NDS_SOCKET_OK )
    {
        if ( nds_logging_check( NDS_LOG_GROUP_STATUS_UPDATE, 0 ) )
        {
            char pmsg[ 256 ];

            nds_logging_printf( " failed\n" );
            strerror_r( errno, pmsg, sizeof( pmsg ) );
            nds_logging_printf( "connect( ): errno: %d - %s\n", errno, pmsg );
        }
        daq_private_srvr_close( daq->conceal );
        return DAQD_CONNECT;
    }
    if ( nds_logging_check( NDS_LOG_GROUP_STATUS_UPDATE, 0 ) )
    {
        nds_logging_printf( " done\n" );
    }
    daq_private_srvr_nonblocking( daq->conceal, 0 );

    daq->conceal->datafd = daq->conceal->sockfd;

    resp = _nds1_retrieve_version( daq, &v, &rv );

    if ( resp != DAQD_OK )
    {
        return resp;
    }

    daq->nds1_ver = v;
    daq->nds1_rev = rv;

    return 0;
}

/*
 *  Connect to the DAQD server on the host identified by `ip' address.
 *  Returns zero if OK or the error code if failed.
 */
int
nds1_connect( daq_t* daq, const char* host, int port )
{
    int resp;
    int v, rv;
    int connect_rc = 0;

    /*  Get a socket.
     */
    resp = daq_private_srvr_open( daq->conceal );
    if ( resp != DAQD_OK )
    {
        return resp;
    }

    /*----------------------------------  Get the server address.     */
    resp = daq_set_server( daq, host, port );
    if ( resp )
    {
        daq_private_srvr_close( daq->conceal );
        return resp;
    }

    /*-----------------------------------  Connect to the server      */
    daq_private_srvr_nonblocking( daq->conceal, 1 );
    if ( nds_logging_check( NDS_LOG_GROUP_STATUS_UPDATE, 0 ) )
    {
        nds_logging_printf( "Connecting NDS1 .." );
        nds_logging_flush( );
    }
    connect_rc = daq_private_srvr_connect( daq->conceal );
    if ( connect_rc == NDS_SOCKET_TRANSIENT_FAILURE )
    {
        struct timeval timeout = { 4, 0 };
        fd_set         writefds;
        int            ret;
        FD_ZERO( &writefds );
        FD_SET( daq->conceal->sockfd, &writefds );
        ret = select(
            NFDS( daq->conceal->sockfd ), NULL, &writefds, NULL, &timeout );
        if ( ret == 1 )
            connect_rc = NDS_SOCKET_OK;
    }
    if ( connect_rc != NDS_SOCKET_OK )
    {
        if ( nds_logging_check( NDS_LOG_GROUP_STATUS_UPDATE, 0 ) )
        {
            char pmsg[ 256 ];

            nds_logging_printf( " failed\n" );
            strerror_r( errno, pmsg, sizeof( pmsg ) );
            nds_logging_printf( "connect( ): errno: %d - %s\n", errno, pmsg );
        }
        daq_private_srvr_close( daq->conceal );
        return DAQD_CONNECT;
    }
    if ( nds_logging_check( NDS_LOG_GROUP_STATUS_UPDATE, 0 ) )
    {
        nds_logging_printf( " done\n" );
    }
    daq_private_srvr_nonblocking( daq->conceal, 0 );

    daq->conceal->datafd = daq->conceal->sockfd;

    resp = _nds1_retrieve_version( daq, &v, &rv );
    if ( resp != DAQD_OK )
    {
        return resp;
    }

    daq->nds1_ver = v;
    daq->nds1_rev = rv;

#ifdef VXWORKS
    {
        int blen = 4 * 4096;
        if ( setsockopt( daq->conceal->sockfd,
                         SOL_SOCKET,
                         SO_SNDBUF,
                         &blen,
                         sizeof( blen ) ) < 0 )
        {
            if ( nds_logging_check( NDS_LOG_GROUP_STATUS_UPDATE, 0 ) )
            {
                nds_logging_printf( "setsockopt SO_SNDBUF failed\n" );
            }
            daq_private_srvr_close( daq->conceal );
            return DAQD_SETSOCKOPT;
        }
    }
#endif
    return 0;
}

/*
 *  Disconnect from the server and close the socket file descriptor
 */
int
nds1_disconnect( daq_t* daq )
{
#if _WIN32
    typedef int write_len_t;
#else /* _WIN32 */
    typedef size_t write_len_t;
#endif /* _WIN32 */

    static const char* METHOD = "nds1_disconnect";
    char*              command = "quit;\n";
    size_t             lcmd = strlen( command );
    if ( nds_logging_check( NDS_LOG_GROUP_TRACE_ENTRY_EXIT, 10 ) )
    {
        nds_logging_printf( "INFO: %s: ENTRY\n", METHOD );
    }
    if ( write( daq->conceal->sockfd, command, ( write_len_t )( lcmd ) ) !=
         lcmd )
    {
        if ( nds_logging_check( NDS_LOG_GROUP_VERBOSE_ERRORS, 0 ) )
        {
            nds_logging_printf( "ERROR: %s: write errno=%d\n", METHOD, errno );
        }
        if ( nds_logging_check( NDS_LOG_GROUP_TRACE_ENTRY_EXIT, 10 ) )
        {
            nds_logging_printf( "INFO: %s: EXIT - %d\n", METHOD, DAQD_WRITE );
        }
        return DAQD_WRITE;
    }
    usleep( 100000 );
    if ( nds_logging_check( NDS_LOG_GROUP_TRACE_ENTRY_EXIT, 10 ) )
    {
        nds_logging_printf( "INFO: %s: EXIT - 0\n", METHOD );
    }
    return 0;
}

/*
 *  Get the most recent error message recorded by the server.
 *  Returns zero if OK or the error code if failed.
 */
int
nds1_get_last_message( daq_t* daq, char* buf, size_t max_len, int* str_len )
{
    int len = strerror_r( daq->err_num, buf, max_len );
    if ( str_len )
        *str_len = len;
    return len ? DAQD_OK : DAQD_ERROR;
}

static void
cvthex( const char** s, char* buf, void* x )
{
    memcpy( buf, *s, (size_t)8 );
    buf[ 8 ] = 0;
    ( *s ) += 8;
    *(int*)x = (int)( dca_strtol( buf ) );
}

/*
 * Some trend types have differing data types than the base channel type.  (Ex:
 * ".mean", ".rms"
 * return double precision values for integer channels)
 *
 * @param trend_name - The trend name (with the m-trend or s-trend removed) as a
 * NULL terminated string
 * @param base_channel_type - The type of the base data channel
 * @return - The data type to use for the trend
 */
static daq_data_t
nds1_get_trend_data_type( const char* trend_name, daq_data_t base_channel_type )
{
    daq_data_t result = base_channel_type;
    if ( trend_name )
    {
        if ( nds_str_endswith( trend_name, ".mean" ) ||
             nds_str_endswith( trend_name, ".rms" ) )
        {
            result = _64bit_double;
        }
        else if ( nds_str_endswith( trend_name, ".n" ) )
        {
            result = _32bit_integer;
        }
    }
    return result;
}

static void
_nl_to_null( char* s )
{
    char ch = 0;

    if ( !s )
        return;
    for ( ch = *s; ch != '\0' && ch != '\n'; ++ch )
    {
    }
    if ( ch == '\n' )
        *s = '\0';
}

static int
_nds1_read_sc3_string( nds_socket_type sock, size_t max_len, char* buf )
{
    if ( !buf || _daq_read_cstring( sock, max_len, buf ) < 0 )
    {
        return DAQD_ERROR;
    }
    _nl_to_null( buf );
    return DAQD_OK;
}

static int
_nds1_read_sc3_number( nds_socket_type sock,
                       size_t          max_len,
                       char*           buf,
                       long*           out )
{
    char* endp = 0;
    long  tmp = 0;

    if ( _nds1_read_sc3_string( sock, max_len, buf ) != DAQD_OK )
    {
        return DAQD_ERROR;
    }
    tmp = strtol( buf, &endp, 10 );
    if ( *endp != '\0' )
    {
        nds_logging_print_errno( "Error channel meta-data" );
        return DAQD_ERROR;
    }
    *out = tmp;
    return DAQD_OK;
}

static int
_nds1_read_sc3_double( nds_socket_type sock,
                       size_t          max_len,
                       char*           buf,
                       double*         out )
{
    char*  endp = 0;
    double tmp = 0.0;

    if ( _nds1_read_sc3_string( sock, max_len, buf ) != DAQD_OK )
    {
        return DAQD_ERROR;
    }
    tmp = strtod( buf, &endp );
    if ( *endp != '\0' )
    {
        nds_logging_print_errno( "Error channel meta-data: %s\n" );
        return DAQD_ERROR;
    }
    *out = tmp;
    return DAQD_OK;
}

static int
nds1_recv_channels_sc3( daq_t*         daq,
                        daq_channel_t* channel,
                        int            num_channels,
                        int*           num_channels_received,
                        const char*    name )
{
#if _WIN32
    typedef int write_len_t;
#else /* _WIN32 */
    typedef size_t write_len_t;
#endif /* _WIN32 */

    static const char* METHOD = "nds1_recv_channels_sc3";
#if _WIN32
#define BUF_SIZE ( MAX_LONG_CHANNEL_NAME_LENGTH + 2 )
#else /* _WIN32 */
    const int      BUF_SIZE = MAX_LONG_CHANNEL_NAME_LENGTH + 2;
#endif /* _WIN32 */
    char chan_req[ sizeof( "status channels 3 {\"\"};\n" ) +
                   MAX_LONG_CHANNEL_NAME_LENGTH ];
    char        buf[ BUF_SIZE ];
    const char* tmp = 0;
    long        numeric_val = 0;
    double      double_val = 0.0;
    const int   signal_unit_size = 40;
    int         result = DAQD_ERROR;

    /* for right now just done named entries on 12.1+ */
    if ( !name || ( ( daq->nds1_ver == 12 && daq->nds1_rev == 0 ) ||
                    daq->nds1_ver < 12 ) )
        return DAQD_NOT_SUPPORTED;

    if ( strlen( name ) > MAX_LONG_CHANNEL_NAME_LENGTH )
        return DAQD_INVALID_CHANNEL_NAME;

#if HAVE_SPRINTF_S
    sprintf_s
#else /* HAVE_SPRINTF_S */
    snprintf
#endif /* HAVE_SPRINTF_S */
        ( chan_req, sizeof( chan_req ), "status channels 3 {\"%s\"};\n", name );

    if ( nds_logging_check( NDS_LOG_GROUP_VERBOSE_ERRORS, 10 ) )
    {
        nds_logging_printf( "INFO: %s: nds1_recv_channels_sc3: entry\n",
                            METHOD );
    }

    /** Cannot use daq_send as status channels 3 { ... } does not send
     * a response code on success, only error.
     */
    if ( write( daq->conceal->sockfd,
                chan_req,
                (write_len_t)strlen( chan_req ) ) != strlen( chan_req ) )
    {
        nds_logging_print_errno( "Error in write(): %s" );
        return DAQD_WRITE;
    }
    /* On success the output from the server is the channel name, not 0000.
     * So read in the first byte, if it is a '0' we have an error, else
     * read the rest of the channel name
     */
    if ( read_bytes( daq->conceal->sockfd, buf, (size_t)1 ) != 1 )
    {
        nds_logging_print_errno( "Error in read_bytes()" );
        return DAQD_ERROR;
    }
    /* Error condition */
    if ( buf[ 0 ] == '0' )
    {
        if ( read_bytes( daq->conceal->sockfd, buf + 1, (size_t)3 ) != 3 )
        {
            nds_logging_print_errno( "Error in read_bytes()" );
            return DAQD_ERROR;
        }
        buf[ 4 ] = '\0';
        return dca_strtol( buf );
    }
    /* At this point buf has the first character of the name, read in the rest
     */
    if ( _nds1_read_sc3_string( daq->conceal->sockfd,
                                (size_t)MAX_LONG_CHANNEL_NAME_LENGTH + 2 - 1,
                                buf + 1 ) != DAQD_OK )
        goto clean_up;
    tmp = buf;
    _daq_get_string( &tmp,
                     buf + MAX_LONG_CHANNEL_NAME_LENGTH + 2,
                     channel->name,
                     sizeof( channel->name ) );
    /* rate */
    if ( _nds1_read_sc3_number( daq->conceal->sockfd,
                                (size_t)MAX_LONG_CHANNEL_NAME_LENGTH,
                                buf,
                                &numeric_val ) != DAQD_OK )
        goto clean_up;

    channel->rate = (double)numeric_val;
    /* data type */
    if ( _nds1_read_sc3_number( daq->conceal->sockfd,
                                (size_t)MAX_LONG_CHANNEL_NAME_LENGTH,
                                buf,
                                &numeric_val ) != DAQD_OK )
        goto clean_up;

    channel->data_type = (daq_data_t)numeric_val;
    channel->bps = (int)data_type_size( channel->data_type );
    /* TP num */
    if ( _nds1_read_sc3_number( daq->conceal->sockfd,
                                (size_t)MAX_LONG_CHANNEL_NAME_LENGTH,
                                buf,
                                &numeric_val ) != DAQD_OK )
        goto clean_up;

    channel->tpnum = (daq_data_t)numeric_val;
    /* group */
    if ( _nds1_read_sc3_number( daq->conceal->sockfd,
                                (size_t)MAX_LONG_CHANNEL_NAME_LENGTH,
                                buf,
                                &numeric_val ) != DAQD_OK )
        goto clean_up;

    channel->type = cvt_group_chantype( (int)numeric_val );
    /* Fast channels have non-zero group numbers, cvt_group_chantype maps them
     * to cUnknown.  Map them to cOnline.
     */
    if ( channel->type == cUnknown )
        channel->type = cOnline;
    /* units */
    if ( _nds1_read_sc3_string(
             daq->conceal->sockfd, (size_t)signal_unit_size, buf ) != DAQD_OK )
        goto clean_up;

    tmp = buf;
    _daq_get_string( &tmp,
                     buf + signal_unit_size + 2,
                     channel->s.signal_units,
                     sizeof( channel->s.signal_units ) );
    /* gain */
    if ( _nds1_read_sc3_double( daq->conceal->sockfd,
                                (size_t)MAX_LONG_CHANNEL_NAME_LENGTH,
                                buf,
                                &double_val ) != DAQD_OK )
        goto clean_up;

    channel->s.signal_gain = (float)double_val;
    /* slope */
    if ( _nds1_read_sc3_double( daq->conceal->sockfd,
                                (size_t)MAX_LONG_CHANNEL_NAME_LENGTH,
                                buf,
                                &double_val ) != DAQD_OK )
        goto clean_up;

    channel->s.signal_slope = (float)double_val;
    /* offset */
    if ( _nds1_read_sc3_double( daq->conceal->sockfd,
                                (size_t)MAX_LONG_CHANNEL_NAME_LENGTH,
                                buf,
                                &double_val ) != DAQD_OK )
        goto clean_up;

    channel->s.signal_offset = (float)double_val;
    result = DAQD_OK;

    *num_channels_received = 1;
clean_up:
    if ( result != DAQD_OK )
    {
        nds_logging_print_errno( "Unable to read channel meta-datan" );
    }
    return result;
#if defined( BUF_SIZE )
#undef BUF_SIZE
#endif /* DEFINED(BUF_SIZE) */
}

static int
nds1_recv_channels_impl( daq_t*         daq,
                         daq_channel_t* channel,
                         int            num_channels,
                         int*           num_channels_received,
                         const char*    name )
{
    static const char* METHOD = "nds1_recv_channels_impl";
    int                i;
    int                resp;
    size_t             lChanWd = 8;
    int                channels = -1;
    int                spurious_bytes =
        0; /* NDS1 < v12.1 with a named channel adds a spurious \t */
#if _WIN32
    nds_socket_type f;
#else
    FILE* f;
#endif /* 0 */

/* FIXME: additional "1" is for hack for stray tab in "status channels
 * 2" output for named channels from broken servers */
#define CHAN_BUF_SIZE_MAX 149 + 1

    char chan_req[ sizeof( "status channels 2 {\"\"};\n" ) +
                   MAX_LONG_CHANNEL_NAME_LENGTH ] = "status channels 2;\n";

    int  chan_buf_size;
    int  channel_name_size;
    int  signal_unit_size;
    char buf[ CHAN_BUF_SIZE_MAX ];

    if ( name )
    {
        if ( strlen( name ) > MAX_LONG_CHANNEL_NAME_LENGTH )
            return DAQD_INVALID_CHANNEL_NAME;

#if HAVE_SPRINTF_S
        sprintf_s
#else /* HAVE_SPRINTF_S */
        snprintf
#endif /* HAVE_SPRINTF_S */
            ( chan_req,
              sizeof( chan_req ),
              "status channels 2 {\"%s\"};\n",
              name );
        /* When dealing w/ status channels 2 { name(s) };
         * NDS1 < v12.1 have an extra tab between the name and the meta data
         * NDS1 >= v12.1 get the byte ordering wrong on some fields
         */
        if ( ( daq->nds1_ver == 12 && daq->nds1_rev >= 1 ) ||
             daq->nds1_ver > 12 )
        {
            /* status channels 2 { "named channel" }; is completely broken on
             * 12.1.  Padding is incorrectly
             * applied to 3 of the fields, so a sample rate of 16Hz and 16kHz
             * are encoded as 10000000 amd
             * 40000000 respectively.  The same thing applies to the test point
             * num, type, and group.
             * So we must use status channels 3 { "named channel "}; when we are
             * on 12.1.
             * Might as well use it 12.1+ as well.
             */
            return nds1_recv_channels_sc3(
                daq, channel, num_channels, num_channels_received, name );
        }
        if ( ( daq->nds1_ver == 12 && daq->nds1_rev == 0 ) ||
             daq->nds1_ver < 12 )
        {
            spurious_bytes = 1;
        }
    }

    /* set channel name, signal unit size based on protocol - K. Thorne*/
    if ( daq->nds1_ver >= 12 )
    {
        channel_name_size = 60;
        signal_unit_size = 40;
    }
    else
    {
        channel_name_size = 40;
        signal_unit_size = 40;
    }

    chan_buf_size = channel_name_size + signal_unit_size + 49;

    /* FIXME: hack for stray tab in "status channels 2" output
     * for named channels from broken servers */
    chan_buf_size += spurious_bytes;

    if ( nds_logging_check( NDS_LOG_GROUP_VERBOSE_ERRORS, 10 ) )
    {
        nds_logging_printf( "INFO: %s: nds1_recv_channels: entry\n", METHOD );
    }
    resp = daq_send( daq, chan_req );
    if ( nds_logging_check( NDS_LOG_GROUP_VERBOSE_ERRORS, 10 ) )
    {
        nds_logging_printf( "INFO: %s: nds1_recv_channels: resp: %s\n",
                            METHOD,
                            ( resp ) ? "yes" : "no" );
    }
    if ( resp )
    {
        return resp;
    }

    resp = read_bytes( daq->conceal->sockfd, buf, lChanWd );
    if ( resp == lChanWd )
    {
        buf[ lChanWd ] = 0;
        channels = (int)( dca_strtol( buf ) );
    }

    if ( channels <= 0 )
    {
        if ( nds_logging_check( NDS_LOG_GROUP_STATUS_UPDATE, 0 ) )
        {
            nds_logging_printf(
                "Couldn't determine the number of data channels\n" );
        }
        return DAQD_ERROR;
    }

    *num_channels_received = channels;

#if _WIN32
    f = daq->conceal->sockfd;
#else
    /*  Open the socket as a file
     */
    f = fdopen( dup( daq->conceal->sockfd ), "r" );
#endif /* ! _WIN32 */

    for ( i = 0; i < channels; i++ )
    {
#if _WIN32
        if ( _daq_read_cstring( f, chan_buf_size, buf ) <= 0 )
        {
            nds_logging_print_errno( "string read failed" );
            return DAQD_ERROR;
        }
        if ( nds_logging_check( NDS_LOG_GROUP_VERBOSE_ERRORS, 10 ) )
        {
            nds_logging_printf(
                "INFO: %s: read[%d/%d]: %s\n", METHOD, i, channels, buf );
        }

#else /* _WIN32 */
        if ( fgets( buf, (int)( chan_buf_size ), f ) <= 0 )
        {
            nds_logging_print_errno( "fgets failed" );
            fclose( f );
            return DAQD_ERROR;
        }
#endif /* _WIN32 */
        /* nds_logging_printf("chan string: %s", buf); */

        if ( i < num_channels )
        {
            int         intval;
            const char* s = buf;

            /* Channel name                                 */
            _daq_get_string( &s,
                             buf + channel_name_size,
                             channel[ i ].name,
                             sizeof( channel->name ) );
            s = buf + channel_name_size;
            /* FIXME: hack for stray tab in "status channels 2" output
             * for named channels from broken servers */
            s += spurious_bytes;

            /* Read rate                                    */
            cvthex( &s, buf, &intval );
            channel[ i ].rate = intval;

            /* Read testpoint number                        */
            cvthex( &s, buf, &channel[ i ].tpnum );

            /* Group number and data type are 16-bit fields */
            cvthex( &s, buf, &intval );
            channel[ i ].type = cvt_group_chantype( intval >> 16 );
            /* Fast channels have non-zero group numbers, cvt_group_chantype
             * maps them
             * to cUnknown.  Map them to cOnline.
             */
            if ( channel[ i ].type == cUnknown )
                channel[ i ].type = cOnline;
            channel[ i ].data_type = ( intval & 0xffff );
            channel[ i ].bps = (int)data_type_size( channel[ i ].data_type );

            /* Read gain,slope and offset                   */
            cvthex( &s, buf, &channel[ i ].s.signal_gain );
            cvthex( &s, buf, &channel[ i ].s.signal_slope );
            cvthex( &s, buf, &channel[ i ].s.signal_offset );

            /* Read units                                   */
            _daq_get_string( &s,
                             s + signal_unit_size,
                             channel[ i ].s.signal_units,
                             sizeof( channel->s.signal_units ) );

            if ( nds_logging_check( NDS_LOG_GROUP_VERBOSE_ERRORS, 10 ) )
            {
                nds_logging_printf(
                    "INFO: %s: channel: %s  rate: %f  dtype: %i ctype: %i\n",
                    METHOD,
                    channel[ i ].name,
                    channel[ i ].rate,
                    channel[ i ].data_type,
                    intval );
            }
        }
    }
#if !_WIN32
    fclose( f );
#endif /* ! _WIN32 */
    return 0;
}

/*
 *  Receive channel data using the NDS1 protocol
 */
int
nds1_recv_channels( daq_t*         daq,
                    daq_channel_t* channel,
                    int            num_channels,
                    int*           num_channels_received )
{
    return nds1_recv_channels_impl(
        daq, channel, num_channels, num_channels_received, NULL );
}

/*
 *  Receive channel data using the NDS1 protocol for a single channel, by name.
 *
 *  Note: NDS1 does not understand trend channel names
 * (H1:SOME_CHANNEL.min,s-trend).
 *  This function does the neccesary work to handle trends with NDS1 servers.
 *
 *  FIXME: The NDS1 protocol's "status channels" command accepts any number of
 *  channel names as an optional argument, but the metadata is only valid for
 *  the zeroth channel. For the first, second, ..., Nth channels, the metadata
 *  (sample rate, type, units, etc.) are blank. If this bug in the NDS1 servers
 *  is ever fixed, then we should implement a multi-channel version of this
 *  query.
 */
int
nds1_recv_channel( daq_t* daq, daq_channel_t* channel, const char* name )
{
    int         received;
    int         result = 0;
    const char* channel_name = name;
    chantype_t  channel_type = cRaw;
    double      sample_rate;
    int         is_trend = 0;
    char        trend_name[ MAX_LONG_CHANNEL_NAME_LENGTH + 1 ];
    char        base_name[ MAX_LONG_CHANNEL_NAME_LENGTH + 1 ];

    if ( !name || !channel || !daq )
    {
        return DAQD_ERROR;
    }
    if ( nds_str_endswith( name, ",m-trend" ) )
    {
        channel_type = cMTrend;
        sample_rate = 1.0 / 60;
        is_trend = 1;
    }
    else if ( nds_str_endswith( name, ",s-trend" ) )
    {
        channel_type = cSTrend;
        sample_rate = 1.0;
        is_trend = 1;
    }

    if ( is_trend )
    {
        ssize_t index;

        memset( trend_name, 0, ( size_t )( MAX_LONG_CHANNEL_NAME_LENGTH + 1 ) );
        memset( base_name, 0, ( size_t )( MAX_LONG_CHANNEL_NAME_LENGTH + 1 ) );

        strncpy( trend_name, name, strlen( name ) - strlen( ",m-trend" ) );
        index = nds_str_lastindex( trend_name, '.' );
        if ( index >= 0 )
        {
            strncpy( base_name, trend_name, (size_t)index );
        }
        else
        {
            strncpy( base_name,
                     trend_name,
                     ( size_t )( MAX_LONG_CHANNEL_NAME_LENGTH + 1 ) );
        }
        is_trend = 1;
        channel_name = base_name;
    }

    result =
        nds1_recv_channels_impl( daq, channel, 1, &received, channel_name );
    if ( result == 0 && is_trend )
    {
        /* We need to use the trend name to request data.  Patch in name,
         * the sample rate, and type information gathered above.
         */
        strncpy( channel->name,
                 trend_name,
                 ( size_t )( MAX_LONG_CHANNEL_NAME_LENGTH + 1 ) );
        /* for trends the data type of the trend may differ from the base
         * channel type
         * ex, an int channel will have a double type as the mean trend
         */
        channel->data_type =
            nds1_get_trend_data_type( trend_name, channel->data_type );
        channel->type = channel_type;
        channel->rate = sample_rate;
    }
    return result;
}

/*  nds1_request_check(daq_t*, time_t, time_t)
 *
 *  Check requested channel data status for the specified interval.
 */
int
nds1_request_check( daq_t* daq, time_t start, time_t end )
{
    return DAQD_OK;
}

/*  nds1_request_data(daq_t*, time_t, time_t)
 *
 *  Get requested channel data for the specified interval.
 */
int
nds1_request_data( daq_t* daq, time_t start, time_t dt )
{
    static const char* METHOD = "nds1_request_data";
    enum chantype      reqty = cUnknown;
    enum chantype      chanty = cUnknown;
    size_t             namel = 0;
    size_t             txt_len = 0;
    uint4_type         i;
    char *             cmd_txt, *cmd;
    int                rc = DAQD_OK;
    uint4_type         N = 0;

    /*----------------------------------  Build the command
     */
    /*  Check the time and dt
     */
    if ( dt > 2000000000 || start >= 2000000000 )
    {
        if ( nds_logging_check( NDS_LOG_GROUP_VERBOSE_ERRORS, 0 ) )
        {
            nds_logging_printf(
                "ERROR: %s: start time or time stride is too high\n", METHOD );
        }
        return DAQD_ERROR;
    }

    /*  Get the request type and sum channel name length
     */
    N = daq->num_chan_request;
    if ( N == 0 )
    {
        if ( nds_logging_check( NDS_LOG_GROUP_VERBOSE_ERRORS, 0 ) )
        {
            nds_logging_printf( "ERROR: %s: No channels requested\n", METHOD );
        }
        return DAQD_ERROR;
    }

    for ( i = 0; i < N; ++i )
    {
        chanty = daq->chan_req_list[ i ].type;
        if ( chanty == cTestPoint )
        {
            if ( start != 0 )
            {
                if ( nds_logging_check( NDS_LOG_GROUP_VERBOSE_ERRORS, 0 ) )
                {
                    nds_logging_printf( "ERROR: %s: start time is not 0.\n",
                                        METHOD );
                }
                return DAQD_ERROR;
            }
            chanty = cOnline;
        }
        if ( reqty == cUnknown )
        {
            reqty = chanty;
        }
        else if ( reqty != chanty )
        {
            if ( nds_logging_check( NDS_LOG_GROUP_VERBOSE_ERRORS, 0 ) )
            {
                nds_logging_printf( "ERROR: %s: Incompatible channel types\n",
                                    METHOD );
            }
            return DAQD_ERROR;
        }
        /*  length of quoted name plus leading space
         *  overcounts by 1, since first name lacks leading space
         */
        namel += strlen( daq->chan_req_list[ i ].name ) + 3;
    }

    /*----------------------------------  Resolve  Unknown channel types
     */
    if ( reqty == cUnknown )
    {
        if ( !start )
            reqty = cOnline;
        else
            reqty = cRaw;
    }

    /*----------------------------------  Build up a command
     */
    cmd = NULL;
    switch ( reqty )
    {

    /*----------------------------------  Online channel type.
     */
    case cOnline:
    case cRaw:
    case cTestPoint:
        /* when stride is set to -1, request low-latency data */
        if ( dt == -1 )
            cmd = "start fast-writer";
        else
            cmd = "start net-writer";
        break;

    /*----------------------------------  Second trend channel type.
     */
    case cSTrend:
        cmd = "start trend net-writer";
        break;

    /*----------------------------------  Minute trend channel type.
     */
    case cMTrend:
        cmd = "start trend 60 net-writer";
        break;

    /*----------------------------------  Channel type not supported by NDS1
     */
    default:
        if ( nds_logging_check( NDS_LOG_GROUP_VERBOSE_ERRORS, 0 ) )
        {
            nds_logging_printf( "ERROR: %s: Channel type %s not supported\n",
                                METHOD,
                                cvt_chantype_str( reqty ) );
        }
        return DAQD_ERROR;
    }

    /*----------------------------------  Get the text length, allocat a buffer
     */
    txt_len = strlen( cmd ) /* length of commmand text */
        + snprintf( NULL, 0, "%llu", (unsigned long long)start ) +
        snprintf( NULL, 0, "%llu", (unsigned long long)dt ) +
        strlen( "   {};" ) /* punctuation, times */
        + namel /* length of quoted channel names, blanks and NULL*/
        + 1; /* count the null */
    cmd_txt = (char*)malloc( txt_len );

    /*----------------------------------  Build the command
     */
    _daq_strlcpy( cmd_txt, cmd, txt_len );

    //----------------------------------  Include time, duration if specified.
    if ( start )
    {
        _daq_slprintf( cmd_txt,
                       txt_len,
                       " %llu %llu",
                       (unsigned long long)start,
                       (unsigned long long)dt );
    }

    _daq_strlcat( cmd_txt, " {", txt_len );

    for ( i = 0; i < N; i++ )
    {
        _daq_slprintf( cmd_txt,
                       txt_len,
                       "%s\"%s\"",
                       ( i ? " " : "" ),
                       daq->chan_req_list[ i ].name );
        daq->chan_req_list[ i ].status = 0;
    }

    /* checking here for truncation will catch any truncation in cmd_txt */
    if ( _daq_strlcat( cmd_txt, "};", txt_len ) >= txt_len )
    {
        if ( nds_logging_check( NDS_LOG_GROUP_VERBOSE_ERRORS, 0 ) )
        {
            nds_logging_printf(
                "ERROR: %s: Internal error, command string truncated\n",
                METHOD );
        }
        rc = DAQD_ERROR;
        goto cleanup;
    }

    /*----------------------------------  Send the command.
     */
    rc = daq_send( daq, cmd_txt );
    if ( rc )
    {
        if ( nds_logging_check( NDS_LOG_GROUP_STATUS_UPDATE, 0 ) )
        {
            nds_logging_printf( "Error in daq_send: %i\n", rc );
        }
    }
    else
    {
        int wid = (int)( daq_recv_id( daq ) );
        if ( wid < 0 )
        {
            if ( nds_logging_check( NDS_LOG_GROUP_STATUS_UPDATE, 0 ) )
            {
                nds_logging_printf( "Error in daq_recv_id: %i\n", wid );
            }
        }
        else
        {
            int bl = daq_recv_block_num( daq );
            if ( ( bl < 0 ) &&
                 nds_logging_check( NDS_LOG_GROUP_STATUS_UPDATE, 0 ) )
            {
                nds_logging_printf( "Error in daq_recv_block_num = %i\n", bl );
            }
        }
    }

/*----------------------------------  Free the command.
 */
cleanup:
    free( cmd_txt );
    cmd_txt = 0;
    return rc;
}

/*  nds1_receive_reconfigure(daq_t* daq, long block_len)
 *
 *  Receive a reconfigure block. receive_reconfigure is received after the
 *  block header has been read in. The block length does not include the
 *  header length.
 */
int
nds1_receive_reconfigure( daq_t* daq, size_t block_len )
{
    size_t          nchannels, alloc_size, i;
    nds_socket_type fd;
    signal_conv_t1* ps;

    nchannels = block_len / sizeof( signal_conv_t1 );
    if ( nchannels * sizeof( signal_conv_t1 ) != block_len )
    {
        if ( nds_logging_check( NDS_LOG_GROUP_STATUS_UPDATE, 0 ) )
        {
            nds_logging_printf(
                "warning:channel reconfigure block length has bad length" );
        }
    }
    if ( nchannels == 0 )
        return -2;
    alloc_size = sizeof( signal_conv_t1 ) * nchannels;

    if ( !daq->s )
    {
        daq->s = (signal_conv_t1*)malloc( alloc_size );
        if ( daq->s )
        {
            daq->s_size = (int)nchannels;
        }
        else
        {
            if ( nds_logging_check( NDS_LOG_GROUP_STATUS_UPDATE, 0 ) )
            {
                nds_logging_printf( "malloc(%" PRISIZE_T ") failed; errno=%d\n",
                                    alloc_size,
                                    errno );
            }
            daq->s_size = 0;
            return -1;
        }
    }
    else if ( daq->s_size != nchannels )
    {
        daq->s = (signal_conv_t1*)realloc( (void*)daq->s, alloc_size );
        if ( daq->s )
        {
            daq->s_size = (int)nchannels;
        }
        else
        {
            if ( nds_logging_check( NDS_LOG_GROUP_STATUS_UPDATE, 0 ) )
            {
                nds_logging_printf( "realloc(%" PRISIZE_T
                                    ") failed; errno=%d\n",
                                    alloc_size,
                                    errno );
            }
            daq->s_size = 0;
            return -1;
        }
    }

    fd = daq->conceal->datafd;
    ps = daq->s;
    for ( i = 0; i < nchannels; i++ )
    {
        if ( read_float( fd, &ps[ i ].signal_offset ) )
            return DAQD_ERROR;

        if ( read_float( fd, &ps[ i ].signal_slope ) )
            return DAQD_ERROR;
        read_uint4( fd, &ps[ i ].signal_status );
    }
    return -2;
}

/*
 *  Initialize the nds1 protocol.
 */
int
nds1_startup( void )
{
    return 0;
}
