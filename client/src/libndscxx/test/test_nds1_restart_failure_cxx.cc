#include "nds.hh"
#include "nds_helper.hh"

#include <memory>
#include <cstdlib>
#include <iostream>
#include <sstream>
#include <string>

#include "nds_testing.hh"

int
main( int argc, char* argv[] )
{
    using NDS::connection;

    //---------------------------------------------------------------------
    // Obtain the port of the server
    //---------------------------------------------------------------------
    connection::port_type port = 31200;
    if (::getenv( "NDS_TEST_PORT" ) )
    {
        std::istringstream ps(::getenv( "NDS_TEST_PORT" ) );

        ps >> port;
    }

    std::string hostname( "localhost" );
    if (::getenv( "NDS_TEST_HOST" ) )
    {
        hostname = ::getenv( "NDS_TEST_HOST" );
    }

    std::cerr << "Connecting to " << hostname << ":" << port << std::endl;
    pointer< connection > conn( make_unique_ptr< connection >(
        hostname, port, connection::PROTOCOL_ONE ) );

    connection::channel_names_type ch;
    ch.push_back( "X1:PEM-1" );
    auto stream = conn->iterate( NDS::request_period( 0, 1 ), ch );
    for ( auto bufs : stream )
    {
    }

    return 0;
}
