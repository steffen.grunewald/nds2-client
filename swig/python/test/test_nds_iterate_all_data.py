import nds2
import os
import sys


def is_gap_second(input_gps):
    # no gaps in this test
    return False


def check_data(buf, val):
    samples_per_sec = int(buf.sample_rate)

    offset = 0
    for cur_gps in range(buf.Start(), buf.Stop()):
        expected = val
        if is_gap_second(cur_gps):
            expected = 0.0
        for i in range(samples_per_sec):
            assert( buf.data[offset] == expected )
            offset += 1

host = 'localhost'
if 'NDS_TEST_HOST' in os.environ:
    host = os.environ['NDS_TEST_HOST']
port = 31200
if 'NDS_TEST_PORT' in os.environ:
    port = int(os.environ['NDS_TEST_PORT'])

proto = nds2.connection.PROTOCOL_TWO
if '-proto-1' in sys.argv:
    proto = nds2.connection.PROTOCOL_ONE
elif '-proto-2' in sys.argv:
    proto = nds2.connection.PROTOCOL_TWO

conn = nds2.connection(host, port, proto)

if "-no-gap" in sys.argv:
    conn.set_parameter("ITERATE_USE_GAP_HANDLERS", "false")
else:
    conn.set_parameter("ITERATE_USE_GAP_HANDLERS", "true")

chans = ["X1:PEM-1", "X1:PEM-2"]

i = 0
cur_gps = 1770000000
for bufs in conn.iterate(1770000000, 1770000020, 10, chans):
    expected = [1.5, 2.75]
    if i > 0:
        expected = [3.5, 4.75]

    assert( bufs[0].gps_seconds == cur_gps )
    assert( bufs[0].Samples() == 256*10 )
    check_data( bufs[0], expected[0] )
    assert( bufs[1].Samples() == 512*10 )
    check_data( bufs[1], expected[1] )
    cur_gps = bufs[0].Stop()
    i += 1

# make sure we can do other operations
conn.set_epoch(1770000000, 1770000000+20)
conn.find_channels("X1:PEM-1")
conn.find_channels("X1:PEM-2")