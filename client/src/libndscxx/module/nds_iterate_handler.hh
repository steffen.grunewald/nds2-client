#ifndef __NDS_ITERATE_HANDLER_HH__
#define __NDS_ITERATE_HANDLER_HH__

#include <memory>
#include "nds_buffer.hh"

namespace NDS
{
    namespace detail
    {
        struct conn_p_type;

        /**
         * @class iterate_handler
         * @note Generic interface to objects that handle the streaming
         * data/iterate
         * interface
         */
        class iterate_handler
            : public std::enable_shared_from_this< iterate_handler >
        {
        public:
            explicit iterate_handler(
                std::shared_ptr< NDS::detail::conn_p_type >&& conn_p )
                : conn_p_{ conn_p }
            {
            }
            virtual bool has_next( ) = 0;

            virtual void next( buffers_type& ) = 0;

            void next( );

            virtual ~iterate_handler( );

            virtual bool
            done( )
            {
                return !has_next( );
            }

            void abort( );

            std::shared_ptr< buffers_type > cache_;

        protected:
            NDS::detail::conn_p_type*
            conn( )
            {
                return conn_p_.get( );
            }

        private:
            std::shared_ptr< NDS::detail::conn_p_type > conn_p_;

            void invalidate( );
        };
    }
}

#endif // __NDS_ITERATE_HANDLER_HH__
