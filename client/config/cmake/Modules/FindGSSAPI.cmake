#
# TODO: This still needs:
#    1) OPTION()
#    2) Recording in cache
# 
INCLUDE( CheckIncludeFiles )
include(FindPackageHandleStandardArgs)

IF ( DEFINED WITHOUT_GSSAPI OR "${WITH_GSSAPI}" MATCHES "^[Nn][Oo]$" )
  SET( WITH_GSSAPI_NO 1 )
  SET( WITH_GSSAPI_LENGTH 0 )
ELSEIF ( "${WITH_GSSAPI}" MATCHES "^[Yy][Ee][Ss]$")
  SET( WITH_GSSAPI_LENGTH 0 )
ELSE ( DEFINED WITHOUT_GSSAPI OR "${WITH_GSSAPI}" MATCHES "^[Nn][Oo]$" )
  STRING( TOLOWER "${WITH_GSSAPI}" WITH_GSSAPI_LOWER)
  STRING( LENGTH "${WITH_GSSAPI}" WITH_GSSAPI_LENGTH )
ENDIF ( DEFINED WITHOUT_GSSAPI OR "${WITH_GSSAPI}" MATCHES "^[Nn][Oo]$" )

IF( ${WITH_GSSAPI_NO} )
  #======================================================================
  # Simply do nothing about detecting GSSAPI
  #======================================================================
ELSEIF( "${WITH_GSSAPI}" MATCHES "^." )
  #======================================================================
  # User is supplying hints of where the GSSAPI library is located.
  # Give them the first chance at the libraries and header files
  #======================================================================
  FIND_PATH(GSSAPI_INCLUDE_DIR gssapi/gssapi.h
            HINTS "${WITH_GSSAPI}/inc/krb5"
                  "${WITH_GSSAPI}/include"
            PATHS "${WITH_GSSAPI}/inc/krb5" )
  #SET(GSSAPI_INCLUDE_DIR "${WITH_GSSAPI}/inc/krb5")
  #FIND_LIBRARY(GSSAPI_LIBRARY NAMES krb5_32 krb524
  #             HINTS "${WITH_GSSAPI}/lib" "${WITH_GSSAPI}/bin"
  #             PATHS /usr/lib )
  IF (WIN32)
    IF (CMAKE_CL_64)
      SET(GSSAPI_LIBRARY "${WITH_GSSAPI}/lib/gssapi64.lib")
    ELSE (CMAKE_CL_64)
      SET(GSSAPI_LIBRARY "${WITH_GSSAPI}/lib/gssapi32.lib")
    ENDIF (CMAKE_CL_64)
  ELSE (WIN32)
    FIND_LIBRARY(GSSAPI_LIBRARY NAMES krb5_32 krb524 gssapi_krb5
               PATHS "/usr/lib" "${WITH_GSSAPI}/lib" )
  ENDIF (WIN32)
ELSE( ${WITH_GSSAPI_NO} )

  #======================================================================
  # Search only the system supplied places
  #======================================================================
  FIND_PATH(GSSAPI_INCLUDE_DIR gssapi/gssapi.h
          HINTS "C:/Program Files/MIT/Kerberos/include"
                "C:/Program Files/MIT/Kerberos/inc/krb5"
          PATHS /usr/include )

  IF (WIN32)

   IF (CMAKE_CL_64)
     FIND_LIBRARY(GSSAPI_LIBRARY NAMES gssapi64
         PATHS "C:/Program Files/MIT/Kerberos/lib/amd64")
   ELSE (CMAKE_CL_64)
     FIND_LIBRARY(GSSAPI_LIBRARY NAMES gssapi32
         PATHS "C:/Program Files/MIT/Kerberos/lib/i386")
   ENDIF (CMAKE_CL_64)
  ELSE (WIN32)
    FIND_LIBRARY(GSSAPI_LIBRARY NAMES krb5_32 krb524 gssapi_krb5
                 PATHS /usr/lib )
  ENDIF (WIN32)
ENDIF( ${WITH_GSSAPI_NO} )
#========================================================================
# Handle the QUIETLY and REQUIRED arguments and set GSSAPI_FOUND to TRUE
# if all listed variables are TRUE
#========================================================================
find_package_handle_standard_args(GSSAPI
				  REQUIRED_VARS
				    GSSAPI_LIBRARY
				    GSSAPI_INCLUDE_DIR )
mark_as_advanced(GSSAPI_LIBRARY GSSAPI_INCLUDE_DIR)
set(GSSAPI_FOUND ${GSSAPI_FOUND} CACHE INTERNAL "" FORCE)
if ( GSSAPI_FOUND )
  set(HAVE_GSSAPI 1)
endif ( GSSAPI_FOUND )
