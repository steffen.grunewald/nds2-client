#include "nds_buffer.hh"
#include "nds_connection.hh"
#include "nds_connection_ptype.hh"
#include "nds_request_fragment.hh"
#include "nds_availability_helper.hh"

namespace NDS
{
    namespace detail
    {

        bool
        request_fragment::is_compatible( const time_span_type& avail ) const
        {
            if ( names.size( ) == 0 )
            {
                return true;
            }
            return detail::gaps_equivalent( avail, time_spans );
        }

        bool
        request_fragment::push_back_if( const std::string& name,
                                        time_span_type     avail,
                                        buffer*            dest_buffer )
        {
            if ( !is_compatible( avail ) )
            {
                return false;
            }
            if ( names.size( ) == 0 )
            {
                time_spans = avail;
            }
            names.push_back( name );
            buffers.push_back( dest_buffer );
            return true;
        }

        void
        request_fragment::bulk_set(
            const buffer::channel_names_type& channel_names,
            const working_buffers&            dest_buffers,
            buffer::gps_second_type           gps_start,
            buffer::gps_second_type           gps_stop )
        {
            names.clear( );
            buffers.clear( );
            time_spans.clear( );

            if ( channel_names.size( ) != dest_buffers.size( ) )
            {
                // FIXME: Throw somethign
                return;
            }

            for ( size_t i = 0; i < channel_names.size( ); i++ )
            {
                names.push_back( channel_names[ i ] );
                buffers.push_back( dest_buffers[ i ] );
            }
            simple_segment s( gps_start, gps_stop );
            time_spans.push_back( s );
        }
    }
}
