#include "nds.hh"
#include "nds_testing.hh"
#include "test_macros.hh"

#include <algorithm>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <sstream>
#include <vector>

using NDS::channel;
using NDS::channels_type;
using NDS::buffers_type;

int
main( int argc, char** argv )
{
    std::vector< std::string > args = convert_args( argc, argv );

    bool multi =
        std::find( args.begin( ), args.end( ), std::string( "-multi" ) ) !=
        args.end( );
    bool end_gap =
        std::find( args.begin( ), args.end( ), std::string( "-endgap" ) ) !=
        args.end( );
    bool plot =
        std::find( args.begin( ), args.end( ), std::string( "-plot" ) ) !=
        args.end( );
    bool validate =
        std::find( args.begin( ), args.end( ), std::string( "-validate" ) ) !=
        args.end( );

    std::cerr << "starting with multi = " << multi << " endgap = " << end_gap;
    std::cerr << " plot = " << plot << " validate = " << validate << std::endl;
    using NDS::connection;

    connection::port_type port = 31200;
    if (::getenv( "NDS_TEST_PORT" ) )
    {
        std::istringstream ps(::getenv( "NDS_TEST_PORT" ) );
        ps >> port;
    }

    std::cerr << "about to connect to port " << port << std::endl;

    std::string hostname( "nds.ligo-wa.caltech.edu" );
    if (::getenv( "NDS_TEST_HOST" ) )
    {
        hostname = ::getenv( "NDS_TEST_HOST" );
    }
    connection::protocol_type proto = connection::PROTOCOL_TRY;
    if (::getenv( "NDS_TEST_PROTO" ) )
    {
        if ( std::strcmp(::getenv( "NDS_TEST_PROTO" ), "1" ) == 0 )
            proto = connection::PROTOCOL_ONE;
        else if ( std::strcmp(::getenv( "NDS_TEST_PROTO" ), "2" ) == 0 )
            proto = connection::PROTOCOL_TWO;
    }
    std::cerr << "connecting to " << hostname << ":" << port << " v" << proto
              << std::endl;
    pointer< connection > conn(
        make_unique< connection >( hostname, port, proto ) );
    NDS_ASSERT( conn->get_port( ) == port );
    // NDS_ASSERT( conn->get_protocol( ) == 2 );

    std::cerr << "connected " << hostname << std::endl;

    connection::channel_names_type cn;
    NDS::buffer::gps_second_type   gps_start = 1112572800,
                                 gps_stop = gps_start + 345600;
    if ( multi )
    {
        gps_start = 1109370240, gps_stop = gps_start + 864060;
        cn.push_back( "H0:PEM-CS_WIND_ROOF_WEATHER_MPH.mean,m-trend" );
        cn.push_back( "H0:PEM-EX_WIND_ROOF_WEATHER_MPH.mean,m-trend" );
        cn.push_back( "H0:PEM-EY_WIND_ROOF_WEATHER_MPH.mean,m-trend" );
        cn.push_back( "H1:ALS-X_REFL_CTRL_OUT_DQ.rms,m-trend" );
        cn.push_back( "H1:ALS-Y_REFL_CTRL_OUT_DQ.rms,m-trend" );
    }
    else
    {
        cn.push_back( "H1:PSL-PWR_HPL_DC_OUT_DQ.mean,m-trend" );
        if ( end_gap )
        {
            // the gap ends in 60 more seconds, so stop here
            gps_stop = 1112756340;
        }
    }

    std::cerr << "channel list:" << std::endl;
    for ( connection::channel_names_type::const_iterator cur = cn.begin( );
          cur != cn.end( );
          ++cur )
    {
        std::cerr << " " << *cur << std::endl;
    }
    std::cerr << "Transfer for " << gps_start << " - " << gps_stop << std::endl;
    buffers_type bufs = conn->fetch( gps_start, gps_stop, cn );
    std::cerr << "Have data (" << bufs.size( ) << ")" << std::endl;

    for ( buffers_type::const_iterator cur = bufs.begin( ); cur != bufs.end( );
          cur++ )
    {
        std::cerr << cur->NameLong( ) << " - " << cur->Samples( ) << " samples"
                  << std::endl;
    }

    if ( !plot && validate )
    {
        std::cerr << std::endl << "Validating sample set" << std::endl;
        for ( buffers_type::const_iterator cur = bufs.begin( );
              cur != bufs.end( );
              cur++ )
        {
            std::cerr << "Samples:     " << cur->Samples( ) << std::endl;
            std::cerr << "Sample Size: " << cur->DataTypeSize( ) << std::endl;
            std::cerr << "Sample Rate: " << cur->SampleRate( ) << std::endl;
            NDS::buffer::size_type sampleSize = cur->DataTypeSize( );
            NDS::buffer::size_type max = cur->Samples( ) * sampleSize;

            std::cerr << "searching [0," << max << ":"
                      << ( size_t )( max / cur->DataTypeSize( ) ) << ")"
                      << std::endl;
            unsigned char prevb = 0xff;
            for ( NDS::buffer::size_type i = 0; i < max; i += sampleSize )
            {
                unsigned char curb = ( *cur )[ i ];
                if ( curb != prevb )
                {
                    std::cerr << "Found a ";
                    std::cerr
                        << ( curb == 0
                                 ? "uninit "
                                 : ( curb == 1 ? "gap    "
                                               : ( curb == 2 ? "data   "
                                                             : "unknown" ) ) );
                    std::cerr << " segment at "
                              << cur->samples_to_seconds(
                                     cur->bytes_to_samples( i ) ) +
                            gps_start;
                    std::cerr << " (" << i << ":" << cur->bytes_to_samples( i );
                    std::cerr << ":"
                              << cur->samples_to_seconds(
                                     cur->bytes_to_samples( i ) ) +
                            gps_start;
                    std::cerr << ")" << std::endl;
                }
                prevb = curb;
                for ( NDS::buffer::size_type j = 1; j < sampleSize; j++ )
                {
                    if ( ( *cur )[ i + j ] != prevb )
                    {
                        std::cerr << "Inter sample data change at byte "
                                  << i + j << std::endl;
                    }
                }
            }
            break;
        }
    }
    else
    {
        for ( buffers_type::const_iterator cur = bufs.begin( );
              cur != bufs.end( );
              cur++ )
        {
            const NDS::buffer& cur_buffer = *cur;
            std::cout << "#time value" << std::endl;
            if ( cur->DataType( ) != NDS::channel::DATA_TYPE_FLOAT64 )
                continue;
            const double* start =
                reinterpret_cast< const double* >( &( cur_buffer[ 0 ] ) );
            for ( int i = 0; i < cur->Samples( ); i++, start++ )
            {
                std::cout << cur->samples_to_seconds( i ) + cur->Start( )
                          << "\t" << *start << std::endl;
            }
            break;
        }
    }
    conn->shutdown( );
    conn->close( );
    return 0;
}
