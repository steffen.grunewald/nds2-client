/* -*- mode: C++ ; c-basic-offset: 2; indent-tabs-mode: nil -*- */

#ifndef SWIG__COMMON__NDS_BASH_PATTERN_HH
#define SWIG__COMMON__NDS_BASH_PATTERN_HH

#include <string>
#include "bash_pattern.h"

namespace NDS
{
    namespace detail
    {
        class bash_pattern
        {
        public:
            explicit bash_pattern( const std::string& expression )
                : compiled_expr(::bash_pattern_compile( expression.c_str( ) ) )
            {
            }

            explicit bash_pattern( const char* expression )
                : compiled_expr(::bash_pattern_compile(
                      ( expression ? expression : "" ) ) )
            {
            }

            ~bash_pattern( )
            {
                if ( compiled_expr )
                {
                    ::bash_pattern_free( compiled_expr );
                }
            }

            int
            matches( const char* text )
            {
                return ::bash_pattern_matches( compiled_expr, text );
            }

        private:
            typedef ::bash_pattern bash_pattern_t;

            bash_pattern_t* compiled_expr;
        }; // class bash_pattern
    }
}

#endif /* SWIG__COMMON__NDS_BASH_PATTERN_HH */
