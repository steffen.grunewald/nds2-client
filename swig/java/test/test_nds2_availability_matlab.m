% -*- mode: octave -*-
% -------------------------------------------------------------------
%   Simple test to verify availability function
% -------------------------------------------------------------------

unit_test = unittest( );

hostname = unit_test.hostname( );
port = unit_test.port( );
protocol = nds2.connection.PROTOCOL_TWO;

conn = nds2.connection(hostname, port, protocol );

gps_start = 1116733655;
gps_stop = 1116733675;

cn = { 'H1:SUS-ETMX_M0_MASTER_OUT_F1_DQ',
       'H1:SUS-ETMX_M0_MASTER_OUT_F2_DQ',
       'H1:SUS-ETMX_M0_MASTER_OUT_F3_DQ',
       'H1:SUS-ETMX_M0_MASTER_OUT_LF_DQ',
       'H1:SUS-ETMX_M0_MASTER_OUT_RT_DQ',
       'H1:SUS-ETMX_M0_MASTER_OUT_SD_DQ',
       'H1:SUS-ETMX_L1_MASTER_OUT_UL_DQ',
       'H1:SUS-ETMX_L1_MASTER_OUT_UR_DQ',
       'H1:SUS-ETMX_L1_MASTER_OUT_LL_DQ',
       'H1:SUS-ETMX_L1_MASTER_OUT_LR_DQ',
       'H1:SUS-ETMX_L2_MASTER_OUT_UL_DQ',
       'H1:SUS-ETMX_L2_MASTER_OUT_UR_DQ',
       'H1:SUS-ETMX_L2_MASTER_OUT_LL_DQ',
       'H1:SUS-ETMX_L2_MASTER_OUT_LR_DQ',
       'H1:SUS-ETMX_L3_MASTER_OUT_UL_DQ',
       'H1:SUS-ETMX_L3_MASTER_OUT_UR_DQ'
      };

conn.setEpoch(gps_start, gps_stop);

avail = conn.getAvailability( cn );

%unit_test.msgInfo( avail.toString( ) );
unit_test.check( avail.size( ), length(cn), 'number of epochs: ' );
	    
for i = 0:avail.size( )-1
  entry = avail.get(i);
  lead = sprintf( 'offset: %d ', i );

  unit_test.check( entry.getName( ), ...
		   cn{ i + 1}, ...
		   [ lead  'entry.getName(): ' ] );
  unit_test.check( entry.getData( ).size( ), ...
		   2, ...
		   [ lead 'entry.getData( ).size( ): ' ] );

  unit_test.check( entry.getData( ).get( 0 ).getFrameType( ), ...
		   'H-H1_C', ...
		   [ lead 'frameType(0): ' ] );
  unit_test.check( entry.getData( ).get( 1 ).getFrameType( ), ...
		   'H-H1_R', ...
		   [ lead 'frameType(1): ' ] );
  %----------------------------------------------------------------------
  unit_test.check( entry.getData( ).get( 0 ).getGpsStart( ), ...
		   entry.getData( ).get( 1 ).getGpsStart( ), ...
		   [ lead  'start time equal: offset-0: ' ] );
  unit_test.check( entry.getData( ).get( 0 ).getGpsStart( ), ...
		   gps_start, ...
		   [ lead 'start time correct: ' ] );
  %----------------------------------------------------------------------
  unit_test.check( entry.getData( ).get( 0 ).getGpsStop( ), ...
		   entry.getData( ).get( 1 ).getGpsStop( ), ...
		   [ lead 'stop time equal: offset-0: ' ] );
  unit_test.check( entry.getData( ).get( 0 ).getGpsStop( ), ...
		   gps_stop, ...
		   [ lead 'stop time correct: ' ] );
end

if ( true )
  expected = '( <H1:SUS-ETMX_M0_MASTER_OUT_F1_DQ ( H-H1_C:1116733655-1116733675 H-H1_R:1116733655-1116733675 ) >';
  unit_test.check( avail.toString.substring(0,length(expected)), ...
		   expected, ...
		   'Verify availability list name is correct: ' );
end

if ( true )
  expected = '<H1:SUS-ETMX_M0_MASTER_OUT_F1_DQ ( H-H1_C:1116733655-1116733675 H-H1_R:1116733655-1116733675 ) >';

  unit_test.check( avail.get( 0 ).toString( ).substring( 0, length(expected) ), ...
		   expected, ...
		   'Verify availability list name is correct: ' );
end

if ( true )
  test_avail = nds2.availability_list_type( );
  entry = nds2.availability( );

  entry.setName( 'X1:TEST_1' );
  entry.getData( ).pushBack( nds2.segment( 'X-X1_C',  gps_start, gps_start + 1024) );
  entry.getData( ).pushBack( nds2.segment( 'X-X1_R',  gps_start, gps_start + 100) );
  entry.getData( ).pushBack( nds2.segment( 'X-X1_R',  gps_start + 1024, gps_start + 1024+100) );
  test_avail.pushBack( entry );
	    
  simple_test = test_avail.simpleList( );
    
  unit_test.check( simple_test.toString( ), ...
		   '( ( <1116733655-1116734779> ) )', ...
		   'simple_list range: ' );

  unit_test.check( simple_test.size( ),  1, 'simple_list size: ' );
  unit_test.check( simple_test.get( 0 ).size( ), 1, 'simple_list[ 0 ] size: ' );
  unit_test.check( simple_test.get( 0 ).get( 0 ).getGpsStart( ), ...
		   gps_start, ...
		   'simple_list[ 0 ]  GPS Start: ' );
  unit_test.check( simple_test.get( 0 ).get( 0 ).getGpsStop( ), ...
		   gps_start +1024+100, ...
		   'simple_list[ 0 ]  GPS Stop: ' );
end

%------------------------------------------------------------------------
% test the implicit availability call in fetch
%------------------------------------------------------------------------
bufs = conn.fetch( gps_start, gps_stop, cn );

%unit_test.msgInfo( bufs.toString( ) );
%unit_test.msgInfo( bufs( 1 ).toString( ) );
%
%	{
%	    String expected = '('<H1:SUS-ETMX_M0_MASTER_OUT_F1_DQ (GPS time 1116733655, 10240 samples)>','<H1:SUS-ETMX_M0_MASTER_OUT_F2_DQ (GPS time 1116733655, 10240 samples)>','';
%	    String actual = bufs.toString( ).substring(0, expected.length( ) );
%
%	    System.out.format( '-- INFO: actual: %s expected: %s%n', actual, expected );
%
%	    // assert( actual.equals( expected ) );
%	}
%
unit_test.check( bufs(1).toString( ), ...
		 '<H1:SUS-ETMX_M0_MASTER_OUT_F1_DQ (512Hz, RAW, FLOAT32)>', ...
		'Check name of first data buffer: ' );

%------------------------------------------------------------------------
% Test a larger availability.  This particular one will be > 128k
%------------------------------------------------------------------------
conn.setEpoch(0, 1999999999);
cn = { 'H1:GDS-CALIB_STRAIN,reduced'};
avail = conn.getAvailability( cn );

%------------------------------------------------------------------------
% Close the connection
%------------------------------------------------------------------------
conn.close( );
%------------------------------------------------------------------------
% Report on testing status
%------------------------------------------------------------------------
exit( unit_test.exit_code( ) );
