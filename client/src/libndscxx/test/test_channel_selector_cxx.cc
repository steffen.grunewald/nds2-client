#include <string>
#include <vector>

#include "nds.hh"
#include "nds_connection_ptype.hh"
#include "nds_channel_selection.hh"

#include "nds_testing.hh"
#include "test_macros.hh"

int
main( int argc, char* argv[] )
{
    using namespace NDS;

    std::vector< std::string > args = convert_args( argc, argv );

    //---------------------------------------------------------------------
    // Obtain the port of the server
    //---------------------------------------------------------------------
    connection::port_type port = 31200;
    if ( std::getenv( "NDS_TEST_PORT" ) )
    {
        std::istringstream ps(::getenv( "NDS_TEST_PORT" ) );

        ps >> port;
    }
    std::string hostname( "localhost" );
    if ( std::getenv( "NDS_TEST_HOST" ) )
    {
        hostname = std::getenv( "NDS_TEST_HOST" );
    }

    connection::protocol_type proto = connection::PROTOCOL_TRY;
    if ( find_opt( "-proto-1", args ) )
    {
        proto = connection::PROTOCOL_ONE;
    }
    else if ( find_opt( "-proto-2", args ) )
    {
        proto = connection::PROTOCOL_TWO;
    }
    else if ( std::getenv( "NDS_TEST_PROTO" ) )
    {
        std::string ps( std::getenv( "NDS_TEST_PROTO" ) );
        std::cerr << "NDS_TEST_PROTO = " << ps << std::endl;
        if ( ps.compare( "1" ) == 0 )
            proto = connection::PROTOCOL_ONE;
        else if ( ps.compare( "2" ) == 0 )
            proto = connection::PROTOCOL_TWO;
    }

    parameters                             params( hostname, port, proto );
    std::shared_ptr< detail::conn_p_type > conn_p =
        std::make_shared< detail::conn_p_type >( params );

    detail::channel_selector selector( conn_p );

    channel chan = selector(
        "X1:PEM-1", detail::channel_selector::selection_method::FIRST_CHANNEL );

    try
    {
        chan = selector(
            "X1:PEM-1",
            detail::channel_selector::selection_method::UNIQUE_CHANNEL );
        NDS_ASSERT( false );
    }
    catch ( ... )
    {
    }
    return 0;
}
