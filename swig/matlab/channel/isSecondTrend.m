function value = isSecondTrend( self ) % -*- mode: octave; -*-
% DATATYPE retrieve the isSecondTrend of the channel
%
%   Retrieves the isSecondTrend of the channel
%
%   INPUTS:
%     SELF   : ndsm.channel
%
%   OUTPUTS:
%     VALUE
  value = self.isSecondTrend( )
end
